
"""
  Unique Binary Search Trees II

Given an integer n, return all the structurally unique BST's (binary search trees), which has exactly n nodes of unique values from 1 to n. Return the answer in any order.

https://leetcode.com/explore/learn/card/recursion-i/253/conclusion/2384/
"""

"""
    Idea:   1) Initialize list of BSTs as empty.  
            2) For every number i where i varies from 1 to N, do following
            ......a)  Create a new node with key as 'i', let this node be 'node'
            ......b)  Recursively construct list of all left subtrees.
            ......c)  Recursively construct list of all right subtrees.
            3) Iterate for all left subtrees
               a) For current leftsubtree, iterate for all right subtrees
                    Add current left and right subtrees to 'node' and add
                    'node' to list.
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

class Solution:
    def generateTrees(self, n: int) -> List[Optional[TreeNode]]:

        # function for constructing trees
        def constructTrees(start, end):

            list = []

            """ if start > end then subtree will be empty so returning None in the list """
            if (start > end) :
                list.append(None)
                return list

            """ iterating through all values from for constructing left and right subtree recursively """
            for i in range(start, end + 1):

                """ constructing left subtree """
                leftSubtree = constructTrees(start, i - 1)

                """ constructing right subtree """
                rightSubtree = constructTrees(i + 1, end)

                """ now looping through all left and right subtrees and connecting them to ith root below """
                for j in range(len(leftSubtree)) :
                    left = leftSubtree[j]
                    for k in range(len(rightSubtree)):
                        right = rightSubtree[k]
                        node = TreeNode(i) # making value i as root
                        node.left = left # connect left subtree
                        node.right = right # connect right subtree
                        list.append(node) # add this tree to list
            return list
        return constructTrees(1,n)

