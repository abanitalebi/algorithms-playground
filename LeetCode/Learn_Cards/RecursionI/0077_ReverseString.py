
"""
  Reverse String

Write a function that reverses a string. The input string is given as an array of characters s.

You must do this by modifying the input array in-place with O(1) extra memory.

https://leetcode.com/explore/learn/card/recursion-i/250/principle-of-recursion/1440/
"""

"""
    s[::-1]
"""

class Solution:
    def reverseString(self, s):
        """
        :type s: List[str]
        :rtype: void Do not return anything, modify s in-place instead.
        """
        def helper(start, end, ls):
            if start >= end:
                return

            # swap the first and last element
            ls[start], ls[end] = ls[end], ls[start]

            return helper(start+1, end-1, ls)

        helper(0, len(s)-1, s)
