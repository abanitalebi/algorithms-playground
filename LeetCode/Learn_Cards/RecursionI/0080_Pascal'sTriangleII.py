
"""
  Pascal's Triangle II

Given an integer rowIndex, return the rowIndexth (0-indexed) row of the Pascal's triangle.

In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:

https://leetcode.com/explore/learn/card/recursion-i/251/scenario-i-recurrence-relation/3234/
"""

class Solution:
    def getRow(self, rowIndex: int) -> List[int]:
        n = rowIndex
        if n == 0:
            return [1]
        if n == 1:
            return [1,1]

        out = [1]
        prev = self.getRow(n-1)
        for i in range(len(prev)-1):
            s = prev[i] + prev[i+1]
            out.append(s)
        out.append(1)

        return out
