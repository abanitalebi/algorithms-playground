
"""
  Climbing Stairs

You are climbing a staircase. It takes n steps to reach the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

https://leetcode.com/explore/learn/card/recursion-i/255/recursion-memoization/1662/
"""

"""
    Idea: Fibonachi sequence with memoization.
"""


class Solution:

    def climbStairs(self, n: int) -> int:

        if n <= 3:
            return n

        cache = {1: 1, 2: 2, 3: 3}
        def f(n):
            if n in cache:
                result = cache[n]
            else:
                result = f(n-1) + f(n-2)
                cache[n] = result
            return result

        return f(n)

