
"""
  Search in a Binary Search Tree

You are given the root of a binary search tree (BST) and an integer val.

Find the node in the BST that the node's value equals val and return the subtree rooted with that node. If such a node does not exist, return null.

https://leetcode.com/explore/learn/card/recursion-i/251/scenario-i-recurrence-relation/3233/
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def searchBST(self, root: Optional[TreeNode], val: int) -> Optional[TreeNode]:
        if root is None:
            return None
        if root.left is None and root.right is None:
            if root.val == val:
                return root
            else:
                return None

        q = collections.deque([root])
        while q:
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()
                if cur.val == val:
                    return cur
                if cur.left is not None:
                    q.append(cur.left)
                if cur.right is not None:
                    q.append(cur.right)
        return None
