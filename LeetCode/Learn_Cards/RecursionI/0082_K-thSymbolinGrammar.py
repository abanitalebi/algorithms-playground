
"""
  K-th Symbol in Grammar
We build a table of n rows (1-indexed). We start by writing 0 in the 1st row. Now in every subsequent row, we look at the previous row and replace each occurrence of 0 with 01, and each occurrence of 1 with 10.

For example, for n = 3, the 1st row is 0, the 2nd row is 01, and the 3rd row is 0110.
Given two integer n and k, return the kth (1-indexed) symbol in the nth row of a table of n rows.

https://leetcode.com/explore/learn/card/recursion-i/253/conclusion/1675/
"""

"""
    Idea: Use recursion. Each time the sequence lengths gets doubled. We can look at previous iteration n-1, with k/2.
"""

class Solution:
    def kthGrammar(self, n: int, k: int) -> int:
        def f(n,k):
            # print("n: {}, k: {}".format(n, k))
            if n == 1:
                return 0
            if k % 2 == 0:
                prev = f(n-1, k // 2)
                if prev == 0:
                    return 1
                else:
                    return 0
            else:
                prev = f(n-1, k // 2 + 1)
                if prev == 0:
                    return 0
                else:
                    return 1
        return f(n,k)

