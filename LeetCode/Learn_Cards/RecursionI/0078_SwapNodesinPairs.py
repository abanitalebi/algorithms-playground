
"""
  Swap Nodes in Pairs

Given a linked list, swap every two adjacent nodes and return its head. You must solve the problem without modifying the values in the list's nodes (i.e., only nodes themselves may be changed.)

https://leetcode.com/explore/learn/card/recursion-i/250/principle-of-recursion/1681/
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def swapPairs(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if head is None:
            return None
        if head.next is None:
            return head
        if head.next.next is None:
            temp = head.next
            temp.next = head
            head.next = None
            return temp

        out = head.next
        nex = out.next
        out.next = head
        head.next = nex
        cur = head
        while cur.next is not None and cur.next.next is not None:
            # print("out 1: {}".format(out))
            # print("cur 1: {}".format(cur))
            nex = cur.next
            after_next = cur.next.next

            cur.next = after_next
            # if after_next is None:
            nex.next = None
            # else:
            nex.next = after_next.next
            after_next.next = nex

            cur = nex
            # print("cur 2: {}".format(cur))
            # print("out 2: {}".format(out))
        return out
