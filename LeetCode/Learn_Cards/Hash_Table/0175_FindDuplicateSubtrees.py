
"""
  Find Duplicate Subtrees

Given the root of a binary tree, return all duplicate subtrees.

For each kind of duplicate subtrees, you only need to return the root node of any one of them.

Two trees are duplicate if they have the same structure with the same node values.

https://leetcode.com/explore/learn/card/hash-table/185/hash_table_design_the_key/1127/
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def findDuplicateSubtrees(self, root: Optional[TreeNode]) -> List[Optional[TreeNode]]:
        ans = []
        count = Counter()

        def encode(root: Optional[TreeNode]) -> str:
            if not root:
                return ''

            left = encode(root.left)
            right = encode(root.right)
            encoding = str(root.val) + '#' + left + '#' + right

            if count[encoding] == 1:
                ans.append(root)
            count[encoding] += 1
            return encoding

        encode(root)
        return ans
