
"""
  Minimum Index Sum of Two Lists

Suppose Andy and Doris want to choose a restaurant for dinner, and they both have a list of favorite restaurants represented by strings.

You need to help them find out their common interest with the least list index sum. If there is a choice tie between answers, output all of them with no order requirement. You could assume there always exists an answer.

https://leetcode.com/explore/learn/card/hash-table/184/comparison-with-other-data-structures/1177/
"""


class Solution:
    def findRestaurant(self, list1: List[str], list2: List[str]) -> List[str]:

        dict1 = dict(zip(list1, list(range(len(list1)))))
        dict2 = dict(zip(list2, list(range(len(list2)))))

        commons = []
        for idx, item1 in enumerate(list1):
            if item1 in dict2:
                commons.append([idx + dict2[item1], item1])
        commons = sorted(commons)

        result = []
        min_idx = commons[0][0]
        for tp in commons:
            idx, item = tp
            if idx == min_idx:
                result.append(item)
        return result
