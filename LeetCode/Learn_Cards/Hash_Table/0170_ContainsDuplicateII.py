
"""
  Contains Duplicate II

Given an integer array nums and an integer k, return true if there are two distinct indices i and j in the array such that nums[i] == nums[j] and abs(i - j) <= k.

https://leetcode.com/explore/learn/card/hash-table/184/comparison-with-other-data-structures/1121/
"""

class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        n = len(nums)
        d = {}
        for i in range(n):
            if d.get(nums[i]) is not None:
                d[nums[i]] += 1
            else:
                d[nums[i]] = 1

        for i in range(n):
            if d[nums[i]] <= 1:
                continue
            interval = nums[i:min(i+k+1, n)]
            # print("i: {}, interval: {}".format(i, interval))
            if len(interval) != len(set(interval)):
                return True
        return False
