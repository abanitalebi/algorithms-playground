
"""
  Longest Substring Without Repeating Characters

Given a string s, find the length of the longest substring without repeating characters.

https://leetcode.com/explore/learn/card/hash-table/187/conclusion-hash-table/1135/
"""


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        n = len(s)
        ans = 0
        # mp stores the current index of a character
        mp = {}

        i = 0
        # try to extend the range [i, j]
        for j in range(n):
            if s[j] in mp:
                i = max(mp[s[j]], i)

            ans = max(ans, j - i + 1)
            mp[s[j]] = j + 1

        return ans
