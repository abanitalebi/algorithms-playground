
"""
  Isomorphic Strings

Given two strings s and t, determine if they are isomorphic.

Two strings s and t are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character, but a character may map to itself.

https://leetcode.com/explore/learn/card/hash-table/184/comparison-with-other-data-structures/1117/
"""


class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        # s -> t
        dic = {}
        for i in range(len(s)):
            if dic.get(s[i]) is not None:
                if dic[s[i]] != t[i]:
                    return False
            else:
                dic[s[i]] = t[i]
            # print("dic: {}, s[i]: {}, t[i]: {}".format(dic, s[i], t[i]))

        x = ''
        for i in range(len(s)):
            x += dic[s[i]]
        print("x: {}".format(x))

        if len(set(s)) == len(set(t)):
            return True
        else:
            return False
