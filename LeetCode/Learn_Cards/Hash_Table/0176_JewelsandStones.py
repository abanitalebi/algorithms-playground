
"""
  Jewels and Stones

You're given strings jewels representing the types of stones that are jewels, and stones representing the stones you have. Each character in stones is a type of stone you have. You want to know how many of the stones you have are also jewels.

Letters are case sensitive, so "a" is considered a different type of stone from "A".

https://leetcode.com/explore/learn/card/hash-table/187/conclusion-hash-table/1136/
"""

class Solution:
    def numJewelsInStones(self, jewels: str, stones: str) -> int:

        count = 0
        arr = set([i for i in jewels])
        for i in range(len(stones)):
            if stones[i] in arr:
                count += 1
        return count
