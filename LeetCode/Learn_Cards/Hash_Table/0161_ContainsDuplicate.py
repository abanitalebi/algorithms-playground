
"""
  Contains Duplicate

Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.

https://leetcode.com/explore/learn/card/hash-table/183/combination-with-other-algorithms/1112/
"""

class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        a = len(nums)
        b = len(set(nums))
        return a != b
