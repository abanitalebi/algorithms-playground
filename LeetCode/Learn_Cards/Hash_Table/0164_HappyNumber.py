
"""
  Happy Number

Write an algorithm to determine if a number n is happy.

A happy number is a number defined by the following process:

Starting with any positive integer, replace the number by the sum of the squares of its digits.
Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1.
Those numbers for which this process ends in 1 are happy.
Return true if n is a happy number, and false if not.

https://leetcode.com/explore/learn/card/hash-table/183/combination-with-other-algorithms/1131/
"""

class Solution:
    def isHappy(self, n: int) -> bool:
        x = n
        cases = []
        while ((x**2) > 9) and (not x in cases):
            cases.append(x)
            digits = []
            y = x
            while y > 0:
                digits.append((y % 10)**2)
                y = y // 10
            x = sum(digits)
            # print("digits: {}, x: {}, cases: {}".format(digits, x, cases))
        if x == 1:
            return True
        else:
            return False

