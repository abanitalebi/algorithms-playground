
"""
  First Unique Character in a String

Given a string s, find the first non-repeating character in it and return its index. If it does not exist, return -1.

https://leetcode.com/explore/learn/card/hash-table/184/comparison-with-other-data-structures/1120/
"""

class Solution:
    def firstUniqChar(self, s: str) -> int:
        dic = {}
        for letter in s:
            if dic.get(letter) is not None:
                dic[letter] += 1
            else:
                dic[letter] = 1

        for idx, letter in enumerate(s):
            if dic[letter] == 1:
                return idx

        return -1
