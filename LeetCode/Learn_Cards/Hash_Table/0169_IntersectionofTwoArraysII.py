
"""
  Intersection of Two Arrays II

Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must appear as many times as it shows in both arrays and you may return the result in any order.

https://leetcode.com/explore/learn/card/hash-table/184/comparison-with-other-data-structures/1178/
"""

class Solution:
    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:
        d1 = {}
        for i in range(len(nums1)):
            if d1.get(nums1[i]) is not None:
                d1[nums1[i]] += 1
            else:
                d1[nums1[i]] = 1
        d2 = {}
        for i in range(len(nums2)):
            if d2.get(nums2[i]) is not None:
                d2[nums2[i]] += 1
            else:
                d2[nums2[i]] = 1

        out = []
        union = list(set(nums1 + nums2))
        for i in range(len(union)):
            if (d1.get(union[i]) is not None) and (d2.get(union[i]) is not None):
                out += [union[i]] * min(d1.get(union[i]), d2.get(union[i]))

        return out
