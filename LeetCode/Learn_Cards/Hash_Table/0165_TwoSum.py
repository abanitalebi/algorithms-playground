
"""
  Two Sum

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

https://leetcode.com/explore/learn/card/hash-table/184/comparison-with-other-data-structures/1115/
"""

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:

        dic = dict(zip(nums, list(range(len(nums)))))
        for i in range(len(nums)):
            diff = target - nums[i]
            if (diff in dic) and (i != dic[diff]):
                return [i, dic[diff]]
