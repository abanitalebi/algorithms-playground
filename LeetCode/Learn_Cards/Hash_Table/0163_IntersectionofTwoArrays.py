
"""
  Intersection of Two Arrays

Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must be unique and you may return the result in any order.

https://leetcode.com/explore/learn/card/hash-table/183/combination-with-other-algorithms/1105/
"""

"""
    Idea: Set(list1) vs Set(list2) -> loop over elements: O(n)
"""

class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
        s1 = set(nums1)
        s2 = set(nums2)
        out = []
        for i in s1:
            if i in s2:
                out.append(i)

        return out
