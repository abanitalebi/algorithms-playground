
"""
  Unique Word Abbreviation

The abbreviation of a word is a concatenation of its first letter, the number of characters between the first and last letter, and its last letter. If a word has only two characters, then it is an abbreviation of itself.

For example:

dog --> d1g because there is one letter between the first letter 'd' and the last letter 'g'.
internationalization --> i18n because there are 18 letters between the first letter 'i' and the last letter 'n'.
it --> it because any word with only two characters is an abbreviation of itself.
Implement the ValidWordAbbr class:

ValidWordAbbr(String[] dictionary) Initializes the object with a dictionary of words.
boolean isUnique(string word) Returns true if either of the following conditions are met (otherwise returns false):
There is no word in dictionary whose abbreviation is equal to word's abbreviation.
For any word in dictionary whose abbreviation is equal to word's abbreviation, that word and word are the same.

https://leetcode.com/explore/learn/card/hash-table/187/conclusion-hash-table/1137/
"""

class ValidWordAbbr:

    def __init__(self, dictionary: List[str]):
        self.words = {}
        for i in range(len(dictionary)):
            one_word = dictionary[i]
            if len(one_word) <= 2:
                abbr = one_word
            else:
                abbr = one_word[0] + str((len(one_word)-2)) + one_word[-1]
            if abbr in self.words:
                self.words[abbr].append(one_word)
            else:
                self.words[abbr] = [one_word]
        # print("self.words: {}".format(self.words))


    def isUnique(self, word: str) -> bool:
        if len(word) <= 2:
            abbr = word
        else:
            abbr = word[0] + str((len(word)-2)) + word[-1]

        # print("word: {}, abbr: {}".format(word, abbr))

        if abbr not in self.words:
            return True
        else:
            return (word in self.words[abbr]) and (len(set(self.words[abbr])) == 1)


# Your ValidWordAbbr object will be instantiated and called as such:
# obj = ValidWordAbbr(dictionary)
# param_1 = obj.isUnique(word)
