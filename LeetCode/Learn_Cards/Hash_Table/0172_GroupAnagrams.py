
"""
  Group Anagrams

Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

https://leetcode.com/explore/learn/card/hash-table/185/hash_table_design_the_key/1124/
"""

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        if strs == [""]:
            return [[""]]

        freqs = []
        n = len(strs)
        for i in range(n):
            d = {}
            for j in range(len(strs[i])):
                if d.get(strs[i][j]) is not None:
                    d[strs[i][j]] += 1
                else:
                    d[strs[i][j]] = 1
            freqs.append(d)

        out = []
        visited = [0] * n
        for i in range(n):
            # print("i: {}, strs[i]: {}".format(i, strs[i]))
            if visited[i] == 0:
                visited[i] = 1
                item = [strs[i]]
                for j in range(i+1, n):
                    if freqs[j] == freqs[i]:
                        item += [strs[j]]
                        visited[j] = 1
                out.append(item)
                # print("item: {}".format(item))
        return out
