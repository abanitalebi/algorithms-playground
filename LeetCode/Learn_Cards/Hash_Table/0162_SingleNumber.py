
"""
  Single Number

Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

You must implement a solution with a linear runtime complexity and use only constant extra space.

https://leetcode.com/explore/learn/card/hash-table/183/combination-with-other-algorithms/1176/
"""

"""
    Idea: Use the summation of the input list and its set version.
"""

class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        set_sum = sum(set(nums))
        all_sum = sum(nums)
        return set_sum * 2 - all_sum



class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        set_sum = 0
        for item in list(set(nums)):
            set_sum += item
        all_sum = 0
        for item in nums:
            all_sum += item
        return set_sum * 2 - all_sum


