
"""
  4Sum II

Given four integer arrays nums1, nums2, nums3, and nums4 all of length n, return the number of tuples (i, j, k, l) such that:

0 <= i, j, k, l < n
nums1[i] + nums2[j] + nums3[k] + nums4[l] == 0

https://leetcode.com/explore/learn/card/hash-table/187/conclusion-hash-table/1134/
"""

"""
    Idea: -(a+b) = (c+d). Instead of doing O(n**4), we first construct a dict at O(n2) of the first pair, and similarly
        for the second pair. Then if -(a+b) in sum-dict of the other pair (with O(1)) we add the counts. Overall O(n2).
"""


class Solution:
    def fourSumCount(self, nums1: List[int], nums2: List[int], nums3: List[int], nums4: List[int]) -> int:

        d12 = {}
        for i in range(len(nums1)):
            for j in range(len(nums2)):
                x = nums1[i] + nums2[j]
                if x in d12:
                    d12[x] += 1
                else:
                    d12[x] = 1

        d34 = {}
        for i in range(len(nums3)):
            for j in range(len(nums4)):
                x = nums3[i] + nums4[j]
                if x in d34:
                    d34[x] += 1
                else:
                    d34[x] = 1

        count = 0
        for s12,freq12 in d12.items():
            if -s12 in d34:
                count += freq12 * d34[-s12]

        return count
