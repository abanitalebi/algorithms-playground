
"""
  Last Stone Weight

You are given an array of integers stones where stones[i] is the weight of the ith stone.

We are playing a game with the stones. On each turn, we choose the heaviest two stones and smash them together. Suppose the heaviest two stones have weights x and y with x <= y. The result of this smash is:

If x == y, both stones are destroyed, and
If x != y, the stone of weight x is destroyed, and the stone of weight y has new weight y - x.
At the end of the game, there is at most one stone left.

Return the smallest possible weight of the left stone. If there are no stones left, return 0.

https://leetcode.com/explore/learn/card/heap/646/practices/4084/
"""

"""
    Idea: Because each time we remove 2 items and potentially add a new one, heap can be used.
"""


class Solution:
    def lastStoneWeight(self, stones: List[int]) -> int:
        if len(stones) == 1:
            return stones[0]

        stones = [-s for s in stones]
        heapq.heapify(stones)
        while len(stones) > 1:
            m1 = -heapq.heappop(stones)
            m2 = -heapq.heappop(stones)
            m = -abs(m1 - m2)
            print("m1: {}, m2: {}".format(m1,m2))
            if m1 != m2:
                heapq.heappush(stones, m)
            print("stones: {}, m1==m2: {}".format(stones, m1==m2))
            if (len(stones) == 0) and (m1 == m2):
                return 0

        return -stones[0]
