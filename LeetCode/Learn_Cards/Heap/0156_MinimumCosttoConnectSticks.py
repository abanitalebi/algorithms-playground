
"""
  Minimum Cost to Connect Sticks

You have some number of sticks with positive integer lengths. These lengths are given as an array sticks, where sticks[i] is the length of the ith stick.

You can connect any two sticks of lengths x and y into one stick by paying a cost of x + y. You must connect all the sticks until there is only one stick remaining.

Return the minimum cost of connecting all the given sticks into one stick in this way.

https://leetcode.com/explore/learn/card/heap/646/practices/4090/
"""

class Solution:
    def connectSticks(self, sticks: List[int]) -> int:

        cost = 0
        heapq.heapify(sticks)
        while len(sticks) > 1:
            a = heapq.heappop(sticks)
            b = heapq.heappop(sticks)
            c = a + b
            heapq.heappush(sticks, c)
            cost += c

        return cost
