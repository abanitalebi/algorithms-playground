
"""
  Kth Smallest Element in a Sorted Matrix

Given an n x n matrix where each of the rows and columns is sorted in ascending order, return the kth smallest element in the matrix.

Note that it is the kth smallest element in the sorted order, not the kth distinct element.

You must find a solution with a memory complexity better than O(n2).

https://leetcode.com/explore/learn/card/heap/646/practices/4086/
"""

class Solution:
    def kthSmallest(self, matrix: List[List[int]], k: int) -> int:

        n = len(matrix)
        arr = []
        for i in range(n):
            for j in range(n):
                arr.append(matrix[i][j])
        heapq.heapify(arr)
        out = []
        while len(out) < k:
            val = heapq.heappop(arr)
            out.append(val)

        return out[-1]
