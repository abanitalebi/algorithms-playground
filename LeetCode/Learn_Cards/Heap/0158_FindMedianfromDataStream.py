
"""
  Find Median from Data Stream

The median is the middle value in an ordered integer list. If the size of the list is even, there is no middle value and the median is the mean of the two middle values.

For example, for arr = [2,3,4], the median is 3.
For example, for arr = [2,3], the median is (2 + 3) / 2 = 2.5.
Implement the MedianFinder class:

MedianFinder() initializes the MedianFinder object.
void addNum(int num) adds the integer num from the data stream to the data structure.
double findMedian() returns the median of all elements so far. Answers within 10-5 of the actual answer will be accepted.

https://leetcode.com/explore/learn/card/heap/646/practices/4092/
"""

"""
    Idea: Use 2 heaps: a min-heap for the higher half and a max-heap for the lower half.
    - Add num to max-heap lo. Since lo received a new element, we must do a balancing step for hi. So remove the largest element from lo and offer it to hi.
    - The min-heap hi might end holding more elements than the max-heap lo, after the previous operation. We fix that by removing the smallest element from hi and offering it to lo.
"""



class MedianFinder:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.lo=[]
        self.hi=[]


    def addNum(self, num: int) -> None:
        heapq.heappush(self.lo, -num)
        heapq.heappush(self.hi, -heapq.heappop(self.lo))
        if len(self.lo) < len(self.hi):
            heapq.heappush(self.lo, -heapq.heappop(self.hi))

    def findMedian(self) -> float:
        if len(self.lo) > len(self.hi):
            return -self.lo[0]
        else:
            return float(-self.lo[0] + self.hi[0]) / 2






class MedianFinder:

    def __init__(self):
        self.arr = []

    def addNum(self, num: int) -> None:
        if len(self.arr) == 0:
            self.arr = [num]
            # print("arr: {}".format(self.arr))
            return
        left = 0
        right = len(self.arr)
        while left < right:
            mid = (left + right) // 2
            if self.arr[mid] == num:
                self.arr = self.arr[:mid] + [num] + self.arr[mid:]
                return
            elif self.arr[mid] > num:
                right = mid - 1
            elif self.arr[mid] < num:
                left = mid + 1
        # print("left: {}, right: {}, mid: {}, num: {}".format(left, right, mid, num))
        if self.arr[mid] != num:
            # if left == 0 and right == 0:
            #     m1 = 0
            if right < 0 or left < 0:
                m1 = 0
            elif left > len(self.arr) or right > len(self.arr):
                m1 = len(self.arr)
            elif num < self.arr[mid-1] and mid-1 >= 0:
                m1 = mid - 1
            elif num > self.arr[mid-1] and num < self.arr[mid]:
                m1 = mid
            elif num > self.arr[mid] and mid+1 < len(self.arr) and num < self.arr[mid+1]:
                m1 = mid + 1
            elif mid+2 < len(self.arr) and num > self.arr[mid+1] and num < self.arr[mid+2]:
                m1 = mid + 2
            else:
                m1 = min(left,right)
            self.arr = self.arr[:m1] + [num] + self.arr[m1:]
        # print("arr: {}".format(self.arr))


    def findMedian(self) -> float:
        if len(self.arr) == 0:
            return None
        if len(self.arr) == 1:
            return self.arr[0]
        if len(self.arr) == 2:
            return (self.arr[0] + self.arr[1]) / 2
        mid = len(self.arr) // 2
        if len(self.arr) % 2 == 1:
            return self.arr[mid]
        else:
            return (self.arr[mid-1] + self.arr[mid]) / 2


# Your MedianFinder object will be instantiated and called as such:
# obj = MedianFinder()
# obj.addNum(num)
# param_2 = obj.findMedian()






"""
class MedianFinder {
    priority_queue<int> lo;                              // max heap
    priority_queue<int, vector<int>, greater<int>> hi;   // min heap

public:
    // Adds a number into the data structure.
    void addNum(int num)
    {
        lo.push(num);                                    // Add to max heap

        hi.push(lo.top());                               // balancing step
        lo.pop();

        if (lo.size() < hi.size()) {                     // maintain size property
            lo.push(hi.top());
            hi.pop();
        }
    }

    // Returns the median of current data stream
    double findMedian()
    {
        return lo.size() > hi.size() ? lo.top() : ((double) lo.top() + hi.top()) * 0.5;
    }
};
"""
