
"""
Meeting Rooms II

Given an array of meeting time intervals intervals where intervals[i] = [starti, endi], return the minimum number of conference rooms required.

https://leetcode.com/explore/learn/card/heap/646/practices/4087/
"""

"""
    Idea: Use a heap for all intervals, so you sort the meetings in the order they start.
        Then create another heap for the meeting end times. Loop n times and each time compare a new start time 
        with the lowest end time.
"""

class Solution:
    def minMeetingRooms(self, intervals: List[List[int]]) -> int:
        heapq.heapify(intervals)
        count = 1
        n = len(intervals)
        cur = heapq.heappop(intervals)
        meeting_ends = [cur[1]]
        heapq.heapify(meeting_ends)
        for i in range(n-1):
            lowest_end = meeting_ends[0]
            cur = heapq.heappop(intervals)
            if cur[0] >= lowest_end:
                heapq.heappop(meeting_ends)
                heapq.heappush(meeting_ends, cur[1])
            else:
                heapq.heappush(meeting_ends, cur[1])
                if len(meeting_ends) > count:
                    count = len(meeting_ends)

        return count
