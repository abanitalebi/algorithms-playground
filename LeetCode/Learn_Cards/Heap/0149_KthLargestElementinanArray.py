
"""
  Kth Largest Element in an Array

Given an integer array nums and an integer k, return the kth largest element in the array.

Note that it is the kth largest element in the sorted order, not the kth distinct element.

https://leetcode.com/explore/learn/card/heap/646/practices/4014/
"""

"""
    Idea: This solution is based on using heap, and is O(n + klogn). A better solution is with QuickSelect at O(n).
"""


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        arr = [-i for i in nums]
        heapq.heapify(arr)
        for i in range(k):
            out = heapq.heappop(arr)
        return -out
