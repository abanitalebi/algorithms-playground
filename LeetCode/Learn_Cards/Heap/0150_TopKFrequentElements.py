
"""
  Top K Frequent Elements

Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any order.

https://leetcode.com/explore/learn/card/heap/646/practices/4015/
"""

"""
    Idea: Count the appearances first. Then sort the tuples with sorted(zip([],[])).
"""


class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:

        counts = {}
        for i in range(len(nums)):
            if counts.get(nums[i]) is not None:
                counts[nums[i]] += 1
            else:
                counts[nums[i]] = 1

        freqs = zip(counts.values(), counts.keys())
        freqs = sorted(freqs)

        out = []
        for item in freqs[-k:]:
            out.append(item[1])

        return out
