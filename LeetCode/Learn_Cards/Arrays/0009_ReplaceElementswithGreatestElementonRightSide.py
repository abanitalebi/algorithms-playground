
"""
  Replace Elements with Greatest Element on Right Side

Given an array arr, replace every element in that array with the greatest element among the elements to its right, and replace the last element with -1.

After doing so, return the array.

https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3259/
"""


class Solution:
    def replaceElements(self, arr: List[int]) -> List[int]:

        L = len(arr)
        for i in range(L-1):
            arr[i] = max(arr[i+1:])
        arr[-1] = -1

        return arr
