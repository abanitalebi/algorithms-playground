
"""
Find Numbers with Even Number of Digits

Given an array nums of integers, return how many of them contain an even number of digits.

https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3237/
"""


class Solution:
    def findNumbers(self, nums: List[int]) -> int:

        out = 0
        for x in nums:
            c = 0
            while x > 0:
                x = int(x/10)
                c += 1
            if c % 2 == 0:
                out += 1

        return out
