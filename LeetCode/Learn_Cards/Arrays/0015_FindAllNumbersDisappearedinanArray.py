
"""
  Find All Numbers Disappeared in an Array


Given an array nums of n integers where nums[i] is in the range [1, n], return an array of all the integers in the range [1, n] that do not appear in nums.



Example 1:

Input: nums = [4,3,2,7,8,2,3,1]
Output: [5,6]

https://leetcode.com/explore/learn/card/fun-with-arrays/523/conclusion/3270/
"""

"""Idea:
    Let's take an example:  nums = [4,3,2,7,8,2,3,1].  Now Let's iterate over it,
    Index-1: Value = 4 -> Mark (Value - 1) -> (4-1) index element as negative provided that element is positive.
    Now, nums = [4,3,2,-7,8,2,3,1].  In this do for every index, You will come to this: nums = [-4,-3,-2,-7,8,2,-3,-1]
    The element at index = 4 and index = 5 are positive. So, the answer is [4+1,5+1] = [5,6].
"""

class Solution:
    def findDisappearedNumbers(self, nums: List[int]) -> List[int]:

        out = []
        L = len(nums)
        for i in range(L):
            if nums[abs(nums[i]) - 1] > 0:
                nums[abs(nums[i]) - 1] *= -1
            # print(nums)
        for i in range(L):
            if nums[i] > 0:
                out.append(i + 1)

        return out
