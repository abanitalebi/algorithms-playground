
"""
  Remove Duplicates from Sorted Array

Given an integer array nums sorted in non-decreasing order, remove the duplicates in-place such that each unique element appears only once. The relative order of the elements should be kept the same.

Since it is impossible to change the length of the array in some languages, you must instead have the result be placed in the first part of the array nums. More formally, if there are k elements after removing the duplicates, then the first k elements of nums should hold the final result. It does not matter what you leave beyond the first k elements.

Return k after placing the final result in the first k slots of nums.

Do not allocate extra space for another array. You must do this by modifying the input array in-place with O(1) extra memory.

https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3248/
"""

"Trick: Two pointers both from left-2-right. First one index of where nums[i] >= nums[i+1], the other one finds the next greater number."

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:

        L = len(nums)
        c = 0
        for i in range(len(nums)-1):
            if nums[i] == nums[i+1]:
                c += 1

        i = 0
        j = 0
        while (i < (L-1)) and (j < L):
            if nums[i] >= nums[i+1]:
                current_max = max(nums[i], nums[i+1])
                while j < L-1:
                    j += 1
                    if nums[j] > current_max:
                        nums[i+1] = nums[j]
                        break
            i += 1

        k = L - c

        print("c: {}, nums: {}".format(c, nums))

        return k