
"""
Squares of a Sorted Array

Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.

https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3240/
"""


class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
        out = [0] * len(nums)

        sq = [0] * len(nums)
        flip_idx = -1
        for idx, x in enumerate(nums):
            sq[idx] = x ** 2
            if (idx > 0) and (x >= 0) and (nums[idx-1] < 0):
                flip_idx = idx

        if flip_idx == -1:
            if nums[0] < 0:
                out = sq[::-1]
            else:
                out = sq
            return out

        sq1 = sq[:flip_idx]
        sq2 = sq[flip_idx:]
        sq1_sorted = sq1[::-1]
        c1 = 0
        c2 = 0
        l1 = len(sq1_sorted)
        l2 = len(sq2)
        # print(sq1_sorted, sq2, flip_idx)
        for idx in range(len(nums)):
            if (c1 <= l1) and (c2 <= l2):
                if (c1 < l1) and (c2 < l2) and (sq1_sorted[c1] <= sq2[c2]):
                    out[idx] = sq1_sorted[c1]
                    c1 += 1
                elif c1 == l1:
                    out[idx] = sq2[c2]
                    c2 += 1
                elif c2 == l2:
                    out[idx] = sq1_sorted[c1]
                    c1 += 1
                else:
                    out[idx] = sq2[c2]
                    c2 += 1
            # print("c1: {}, c2: {}, out[idx]: {}".format(c1, c2, out[idx]))

        return out