
"""
  Remove Element

Given an integer array nums and an integer val, remove all occurrences of val in nums in-place. The relative order of the elements may be changed.

Since it is impossible to change the length of the array in some languages, you must instead have the result be placed in the first part of the array nums. More formally, if there are k elements after removing the duplicates, then the first k elements of nums should hold the final result. It does not matter what you leave beyond the first k elements.

Return k after placing the final result in the first k slots of nums.

Do not allocate extra space for another array. You must do this by modifying the input array in-place with O(1) extra memory.

https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3247/
"""

"Trick: Two pointers. One from left to search for val, one from right to search for non-val. Swap in-place & continue."

class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        c = 0
        L = len(nums)
        val_idx = -1
        for i, v in enumerate(nums):
            if v == val:
                c += 1
                val_idx = i

        # edge case
        if L == 1:
            if nums[0] == val:
                return 0
            else:
                return 1
        if L == c:
            return 0


        k = 0                     # counter to reach c
        p1 = 0                    # position of the val occurances
        p2 = len(nums) - 1        # position of other values, to be swapped with val from the left side
        while k < c:

            found_val = False
            while not found_val:
                if nums[p1] == val:
                    found_val = True
                else:
                    p1 += 1

            found_other = False
            while not found_other:
                if nums[p2] != val:
                    found_other = True
                else:
                    p2 -= 1

            print(p1, p2)
            if p1 >= L - c:
                break
            nums[p1] = nums[p2]
            nums[p2] = val
            k += 1
            print(p1, p2, nums)


        return L - c