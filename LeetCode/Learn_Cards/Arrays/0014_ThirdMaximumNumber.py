
"""
  Third Maximum Number

Given an integer array nums, return the third distinct maximum number in this array. If the third maximum does not exist, return the maximum number.



Example 1:

Input: nums = [3,2,1]
Output: 1
Explanation:
The first distinct maximum is 3.
The second distinct maximum is 2.
The third distinct maximum is 1.

https://leetcode.com/explore/learn/card/fun-with-arrays/523/conclusion/3231/
"""

"""Idea
    Use heapq.nlargest or manually heapify and heappop().
    https://docs.python.org/3/library/heapq.html
    https://www.geeksforgeeks.org/heap-queue-or-heapq-in-python/
"""

import heapq

class Solution:
    def thirdMax(self, nums: List[int]) -> int:

        nums = list(set(nums))


        # out = heapq.nlargest(3, nums)
        # print(out)
        # return out[2]


        if len(nums) < 3:
            return max(nums)

        x = nums
        for i, v in enumerate(nums):
            x[i] = -v

        heapq.heapify(x)
        n = 3
        for i in range(n):
            print(x)
            m = heapq.heappop(x)
        out = -m
        print(out)

        return out
