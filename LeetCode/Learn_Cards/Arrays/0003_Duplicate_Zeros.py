
"""
Duplicate Zeros

Given a fixed-length integer array arr, duplicate each occurrence of zero, shifting the remaining elements to the right.

Note that elements beyond the length of the original array are not written. Do the above modifications to the input array in place and do not return anything.

https://leetcode.com/explore/learn/card/fun-with-arrays/525/inserting-items-into-an-array/3245/
"""


class Solution:
    def duplicateZeros(self, arr: List[int]) -> None:
        """
        Do not return anything, modify arr in-place instead.
        """
        idx = 0
        while idx < len(arr):
            if arr[idx] == 0:
                arr.pop()
                arr.insert(idx+1, 0)
                idx += 1
            idx += 1
            # print(arr)
