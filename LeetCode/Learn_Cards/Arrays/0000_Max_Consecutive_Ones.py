
"""
Max Consecutive Ones:

Given a binary array nums, return the maximum number of consecutive 1's in the array.

https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3238/
"""


class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        out = 0
        c = 0
        for i in nums:
            if i == 1:
                c += 1
                if c > out:
                    out = c
            else:
                c = 0
        return out
