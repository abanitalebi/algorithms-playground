"""
  Check If N and Its Double Exist
Given an array arr of integers, check if there exists two integers N and M such that N is the double of M ( i.e. N = 2 * M).

More formally check if there exists two indices i and j such that :

i != j
0 <= i, j < arr.length
arr[i] == 2 * arr[j]

https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3250/
"""

"""Trick: 
    Loop over the array and for each element see if there is any element in the rest of the array 
    equal to double of it, or vice versa."""


class Solution:
    def checkIfExist(self, arr: List[int]) -> bool:

        L = len(arr)
        for i in range(len(arr)-1):
            for j in range(i+1, L):
                if (arr[i] == 2 * arr[j]) or (arr[j] == 2 * arr[i]):
                    return True
        return False


