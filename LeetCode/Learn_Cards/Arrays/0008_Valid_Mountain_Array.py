"""
  Valid Mountain Array

Given an array of integers arr, return true if and only if it is a valid mountain array.

Recall that arr is a mountain array if and only if:

arr.length >= 3
There exists some i with 0 < i < arr.length - 1 such that:
arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
arr[i] > arr[i + 1] > ... > arr[arr.length - 1]

https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3251/
"""

""" Trick:
Loop over from the left, compare each element with the next, and carry a state whether it has increased or is decreasing
"""


class Solution:
    def validMountainArray(self, arr: List[int]) -> bool:

        mountain = False
        has_increased = False
        is_decreasing = False
        L = len(arr)
        for i in range(L-1):
            if arr[i+1] == arr[i]:
                return False

            if arr[i+1] > arr[i]:
                has_increased = True
                if is_decreasing:
                    return False
            else:
                if not has_increased:
                    return False
                else:
                    is_decreasing = True

        if has_increased and is_decreasing:
            return True
        else:
            return False
