
"""
  Sort Array By Parity

Given an integer array nums, move all the even integers at the beginning of the array followed by all the odd integers.

Return any array that satisfies this condition.

Example 1:

Input: nums = [3,1,2,4]
Output: [2,4,3,1]
Explanation: The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.

https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3260/
"""

"""Idea:
    In-place solution: Get two pointers, one for indexing the array, and one for the next even element to substitute
    when/if an odd element found. This should be O(n).
"""

class Solution:
    def sortArrayByParity(self, nums: List[int]) -> List[int]:
        L = len(nums)
        if L <= 1:
            return nums

        e = 0
        o = 0
        for i in range(L):
            if nums[i] % 2 == 0:
                if e <= i:
                    e = i + 1
                continue
            else:
                # e = i
                print("e: {}".format(e))
                # print("nums: {}".format(nums))
                while (e < L) and (nums[e] % 2 != 0) and (e >= i):
                    e += 1
                if (e < L) and (nums[e] % 2 == 0) and (e >= i):
                    temp = nums[i]
                    nums[i] = nums[e]
                    nums[e] = temp

        return nums


