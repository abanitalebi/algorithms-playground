
"""
  Move Zeroes

Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.

Note that you must do this in-place without making a copy of the array.

https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3157/
"""

""" Explanation:
A pointer goes from left to right, and for each element that is zero, finds the next non-zero element and swap."""

class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        L = len(nums)
        for i in range(L-1):
            if nums[i] == 0:
                found_val = False
                for j in range(i+1,L):
                    if nums[j] != 0:
                        found_val = True
                        break
                if found_val:
                    nums[i] = nums[j]
                    nums[j] = 0
