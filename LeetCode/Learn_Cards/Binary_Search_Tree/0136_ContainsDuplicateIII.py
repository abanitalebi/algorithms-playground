
"""
  Contains Duplicate III

Given an integer array nums and two integers k and t, return true if there are two distinct indices i and j in the array such that abs(nums[i] - nums[j]) <= t and abs(i - j) <= k.

https://leetcode.com/explore/learn/card/introduction-to-data-structure-binary-search-tree/142/conclusion/1013/
"""

"""
Here is the entire algorithm in pseudocode:

Initialize an empty BST set
Loop through the array, for each element xx
    Find the smallest element ss in set that is greater than or equal to xx, return true if s - x <= ts−x ≤t
    Find the greatest element gg in set that is smaller than or equal to xx, return true if x - g <= tx−g ≤t
    Put xx in set
    If the size of the set is larger than kk, remove the oldest item.
Return false
"""