
"""
  Insert into a Binary Search Tree

You are given the root node of a binary search tree (BST) and a value to insert into the tree. Return the root node of the BST after the insertion. It is guaranteed that the new value does not exist in the original BST.

Notice that there may exist multiple valid ways for the insertion, as long as the tree remains a BST after insertion. You can return any of them.

https://leetcode.com/explore/learn/card/introduction-to-data-structure-binary-search-tree/141/basic-operations-in-a-bst/1003/
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def insertIntoBST(self, root: Optional[TreeNode], val: int) -> Optional[TreeNode]:

        if root is None:
            return TreeNode(val)

        def search(node, target):

            if node.val > target:
                if node.left is None:
                    new_node = TreeNode(target)
                    node.left = new_node
                    return
                else:
                    search(node.left, target)
            if node.val < target:
                if node.right is None:
                    new_node = TreeNode(target)
                    node.right = new_node
                    return
                else:
                    search(node.right, target)

        search(root, val)
        return root
