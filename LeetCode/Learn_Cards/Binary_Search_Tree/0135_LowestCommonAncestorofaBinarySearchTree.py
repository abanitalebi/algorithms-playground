
"""
  Lowest Common Ancestor of a Binary Search Tree

Given a binary search tree (BST), find the lowest common ancestor (LCA) of two given nodes in the BST.

According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes p and q as the lowest node in T that has both p and q as descendants (where we allow a node to be a descendant of itself).”

https://leetcode.com/explore/learn/card/introduction-to-data-structure-binary-search-tree/142/conclusion/1012/
"""

"""
    Idea: Binary search to find the paths to p & q. Then compare the paths. Overall O(log n)
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':

        if root is None:
            return None

        path_p = []
        path_q = []
        def search(node, target, path_list):
            while True:
                path_list.append(node)
                if node == target:
                    return path_list
                elif node.val < target.val:
                    node = node.right
                elif node.val > target.val:
                    node = node.left

        path_p = search(root, p, path_p)
        path_q = search(root, q, path_q)

        min_l = min(len(path_p), len(path_q))
        for i in range(min_l):
            print("i: {}, path_p[i]: {}, path_q[i]: {}".format(i, path_p[i].val, path_q[i].val))
            if path_p[i] != path_q[i]:
                return path_p[i-1]
        return path_p[i]
