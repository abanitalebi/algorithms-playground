
"""
  Inorder Successor in BST

Given the root of a binary search tree and a node p in it, return the in-order successor of that node in the BST. If the given node has no in-order successor in the tree, return null.

The successor of a node p is the node with the smallest key greater than p.val.

https://leetcode.com/explore/learn/card/introduction-to-data-structure-binary-search-tree/140/introduction-to-a-bst/998/
"""

"""
    Idea: The inorder traversal in BST will be in ascending order. 
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def inorderSuccessor(self, root: 'TreeNode', p: 'TreeNode') -> 'Optional[TreeNode]':

        self.p = None
        def in_order(root_node):
            if root_node is None:
                return []

            return in_order(root_node.left) + [root_node] + in_order(root_node.right)

        traverse = in_order(root)

        for i in range(len(traverse)):
            if traverse[i] == p:
                if i < len(traverse) - 1:
                    return traverse[i+1]
                else:
                    return None