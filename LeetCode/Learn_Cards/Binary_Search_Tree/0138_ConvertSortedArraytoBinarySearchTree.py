
"""
  Convert Sorted Array to Binary Search Tree

Given an integer array nums where the elements are sorted in ascending order, convert it to a height-balanced binary search tree.

A height-balanced binary tree is a binary tree in which the depth of the two subtrees of every node never differs by more than one.

https://leetcode.com/explore/learn/card/introduction-to-data-structure-binary-search-tree/143/appendix-height-balanced-bst/1015/
"""

"""
    Idea: Recursion. TreeNode(mid) , f(nums[:mid]), f([mid+1:]). Then connect node to its left and right.
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> Optional[TreeNode]:

        if len(nums) == 0:
            return None
        if len(nums) == 1:
            return TreeNode(nums[0])
        if len(nums) == 2:
            node1 = TreeNode(nums[0])
            node2 = TreeNode(nums[1])
            node1.right = node2
            return node1

        mid = len(nums) // 2
        node1 = self.sortedArrayToBST(nums[:mid])
        node2 = self.sortedArrayToBST(nums[mid+1:])
        node = TreeNode(nums[mid])
        node.left = node1
        node.right = node2

        return node