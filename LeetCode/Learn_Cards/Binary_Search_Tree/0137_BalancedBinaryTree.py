
"""
  Balanced Binary Tree

Given a binary tree, determine if it is height-balanced.

For this problem, a height-balanced binary tree is defined as:

a binary tree in which the left and right subtrees of every node differ in height by no more than 1.

https://leetcode.com/explore/learn/card/introduction-to-data-structure-binary-search-tree/143/appendix-height-balanced-bst/1027/
"""


"""
    Idea: Recursion to find depth on the left and right sides, and then check if depths are equal.
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isBalanced(self, root: Optional[TreeNode]) -> bool:

        if root is None:
            return True

        self.out = True
        def find_depth(node):
            if self.out == False:
                return 0
            if node is None:
                return 0
            else:
                left_depth = find_depth(node.left)
                right_depth = find_depth(node.right)
                # print("node: {}, left_depth: {}, right_depth: {}".format(node.val, left_depth, right_depth))
                depth = max(left_depth, right_depth) + 1
                if abs(left_depth - right_depth) > 1:
                    self.out = False
                    return 0
            return depth

        find_depth(root)
        return self.out