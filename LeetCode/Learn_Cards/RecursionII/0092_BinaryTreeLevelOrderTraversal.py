
"""
  Binary Tree Level Order Traversal

Given the root of a binary tree, return the level order traversal of its nodes' values. (i.e., from left to right, level by level).

https://leetcode.com/explore/learn/card/recursion-ii/503/recursion-to-iteration/2784/
"""

"""
    Idea: BFS with a queue.
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def levelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:

        if root is None:
            return []

        if root.left is None and root.right is None:
            return [[root.val]]

        out = []
        q = collections.deque([root])
        while q:
            level = []
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()
                level.append(cur.val)
                if cur.left is not None:
                    q.append(cur.left)
                if cur.right is not None:
                    q.append(cur.right)
            out.append(level)
        return out
