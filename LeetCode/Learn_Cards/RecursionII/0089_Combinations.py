
"""
  Combinations

Given two integers n and k, return all possible combinations of k numbers out of the range [1, n].

You may return the answer in any order.

https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/2798/
"""

"""
    Idea: Recursion: The n-th element either appears in the subset (in which case we append it to the subset), or does not.
"""


class Solution:
    def combine(self, n: int, k: int) -> List[List[int]]:

        if k == 0 or n == 0:
            return [[]]
        if k == n:
            return [[i for i in range(1, n+1)]]

        paths1 = self.combine(n-1, k-1)
        for path in paths1:
            path.append(n)
            # print(path)

        paths2 = self.combine(n-1, k)

        return paths1 + paths2




class Solution1:
    def combine(self, n: int, k: int):
        # init first combination
        nums = list(range(1, k + 1)) + [n + 1]

        output, j = [], 0
        while j < k:
            # add current combination
            output.append(nums[:k])
            # increase first nums[j] by one
            # if nums[j] + 1 != nums[j + 1]
            j = 0
            while j < k and nums[j + 1] == nums[j] + 1:
                nums[j] = j + 1
                j += 1
            nums[j] += 1

        return output


class Solution2:
    def combine(self, n: int, k: int):
        def backtrack(first = 1, curr = []):
            if len(curr) == k:
                output.append(curr[:])
            for i in range(first, n + 1):
                # add i into the current combination
                curr.append(i)

                # use next integers to complete the combination
                backtrack(i + 1, curr)

                # backtrack
                curr.pop()

        output = []
        backtrack()
        return output


if __name__ == "__main__":
    ob = Solution()
    ob.combine(n=4, k=2)
