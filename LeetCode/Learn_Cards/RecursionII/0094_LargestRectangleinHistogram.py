
"""
  Largest Rectangle in Histogram

Given an array of integers heights representing the histogram's bar height where the width of each bar is 1, return the area of the largest rectangle in the histogram.

https://leetcode.com/explore/learn/card/recursion-ii/507/beyond-recursion/2901/
"""

class Solution:
    def largestRectangleArea(self, heights: List[int]) -> int:

        if len(heights) == 0:
            return 0
        if len(heights) == 1:
            return heights[0]

        areas = []
        for i in range(1, len(heights)+1):
            for j in range(len(heights)-i+1):
                # Get areas of rectangles of width i
                w = i
                h = min(heights[j:j+i])
                areas.append(w * h)
            # print("areas: {}".format(areas))

        return max(areas)


class Solution:
    def largestRectangleArea(self, heights: List[int]) -> int:
        stack = [-1]
        max_area = 0
        for i in range(len(heights)):
            while stack[-1] != -1 and heights[stack[-1]] >= heights[i]:
                current_height = heights[stack.pop()]
                current_width = i - stack[-1] - 1
                max_area = max(max_area, current_height * current_width)
            stack.append(i)

        while stack[-1] != -1:
            current_height = heights[stack.pop()]
            current_width = len(heights) - stack[-1] - 1
            max_area = max(max_area, current_height * current_width)
        return max_area

 