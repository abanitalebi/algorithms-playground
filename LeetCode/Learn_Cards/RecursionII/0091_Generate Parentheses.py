
"""
  Generate Parentheses

Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

https://leetcode.com/explore/learn/card/recursion-ii/503/recursion-to-iteration/2772/
"""

"""
    Idea: Recursion: add "(" or ")" to f(k-1) list, to build f(k). Validate each case with a stack. 
"""


class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        if n == 1:
            return ["()"]

        def f(k):
            if k == 1:
                return [")", "("]

            p1 = ["(" + i for i in f(k-1)]
            p2 = [")" + i for i in f(k-1)]

            return p1 + p2

        def is_valid(p):
            inp = list(p)
            if len(inp) == 0 or len(inp) == 1:
                return False
            stack = [inp[0]]
            c = 1
            while c < len(inp):
                # print("stack: {}, c: {}".format(stack, c))
                if c < len(inp):
                    stack.append(inp[c])
                    c += 1
                if len(stack) >= 2 and stack[-2] == "(" and stack[-1] == ")":
                    stack.pop()
                    stack.pop()
            if len(stack) == 0:
                return True
            else:
                return False

        candidates = f(2*n)
        # print("Candidates: {}".format(candidates))

        results = []
        for p in candidates:
            # print(is_valid(p))
            if is_valid(p):
                results.append(p)
        # print("results: {}".format(results))
        print(is_valid(results[-1]))
        return results
