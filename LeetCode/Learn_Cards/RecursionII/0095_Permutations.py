
"""
  Permutations

Given an array nums of distinct integers, return all the possible permutations. You can return the answer in any order.

https://leetcode.com/explore/learn/card/recursion-ii/507/beyond-recursion/2903/
"""

"""
    Idea: Recursion: Leave the last element out, call function for n-1 elements, then add the last element in all possible indices.
"""

class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        if len(nums) == 0:
            return []
        if len(nums) == 1:
            return [nums]

        n = len(nums)
        prev = self.permute(nums[:n-1])
        cur = []
        for p in prev:
            for i in range(len(p)+1):
                cur.append(p[0:i] + [nums[n-1]] + p[i:])
        return cur
