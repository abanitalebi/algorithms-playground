
"""
Merge Sort:   Sort an Array

Given an array of integers nums, sort the array in ascending order.

https://leetcode.com/explore/learn/card/recursion-ii/470/divide-and-conquer/2944/
"""

"""
   Idea: Merge sort using recursion (divide and conquer): MS(left-side) & MS(right-side), then merge two lists. 
"""


class Solution:
    def sortArray(self, nums: List[int]) -> List[int]:

        def merge_sort(nums):
            L = len(nums)
            if L == 1:
                return nums
            mid = int(L/2)
            left_arr = nums[:mid]
            right_arr = nums[mid:]

            left_arr = merge_sort(left_arr)
            right_arr = merge_sort(right_arr)

            return merge_lists(left_arr, right_arr)

        def merge_lists(left_arr, right_arr):
            left_L = len(left_arr)
            right_L = len(right_arr)
            left_p = 0
            right_p = 0
            results = []
            while (left_p < left_L) and (right_p < right_L):
                if left_arr[left_p] < right_arr[right_p]:
                    results.append(left_arr[left_p])
                    left_p += 1
                else:
                    results.append(right_arr[right_p])
                    right_p += 1
            results += left_arr[left_p:]
            results += right_arr[right_p:]
            return results

        return merge_sort(nums)
