
"""
  Graph Valid Tree

You have a graph of n nodes labeled from 0 to n - 1. You are given an integer n and a list of edges where edges[i] = [ai, bi] indicates that there is an undirected edge between nodes ai and bi in the graph.

Return true if the edges of the given graph make up a valid tree, and false otherwise.

https://leetcode.com/explore/learn/card/graph/618/disjoint-set/3910/
"""

"""
    Idea: Search for loops. There is a loop, if a node has two parents. Run a BFS, and carry parents in a dict. 
    In each iteration, exclude the parent of the current node, and check if the current neighbors already have a parent.
    If they ask for the path of a loop, can recurse on the nodes over the parents dictionary.
"""



class Solution:
    def validTree(self, n: int, edges: List[List[int]]) -> bool:

        if len(edges) == 0 and n ==1:
            return True

        E = {}
        for  edge in edges:
            if E.get(edge[0]):
                E.get(edge[0]).append(edge[1])
            else:
                E[edge[0]] = [edge[1]]
            if E.get(edge[1]):
                E.get(edge[1]).append(edge[0])
            else:
                E[edge[1]] = [edge[0]]
        print(E)

        if len(E) != n:
            return False

        q = collections.deque([0])
        parents = {0: -1}
        while q:
            q_size = len(q)
            for k in range(q_size):
                # print("q: {}".format(q))
                cur = q.popleft()
                for neighbor in E.get(cur):
                    if neighbor == parents[cur]:    # Exclude the current node's parent.
                        continue
                    if neighbor in parents:
                        return False
                    parents[neighbor] = cur
                    q.append(neighbor)

        return len(parents) == n
