
"""
  Number of Connected Components in an Undirected Graph

You have a graph of n nodes. You are given an integer n and an array edges where edges[i] = [ai, bi] indicates that there is an edge between ai and bi in the graph.

Return the number of connected components in the graph.

https://leetcode.com/explore/learn/card/graph/618/disjoint-set/3911/
"""

"""
    Idea: BFS (1-D) on the nodes: First record the connections in a dictionary, then BFS on every node that has not been visited yet.
        If they ask for the node names of different components, can populate 'visited' array with 1,2,3,...
"""


class Solution:
    def countComponents(self, n: int, edges: List[List[int]]) -> int:

        connections = {}
        for i in edges:
            if connections.get(i[0]) is None:
                connections[i[0]] = [i[1]]
            else:
                connections[i[0]].append(i[1])
            if connections.get(i[1]) is None:
                connections[i[1]] = [i[0]]
            else:
                connections[i[1]].append(i[0])
        print(connections)


        def BFS(dic, visited, k):
            q = collections.deque([k])
            while q:
                q_size = len(q)
                for j in range(q_size):
                    cur = q.popleft()
                    if dic.get(cur) is None:
                        continue
                    for neighbor in dic.get(cur):
                        if (visited[neighbor] == 0):
                            q.append(neighbor)
                            visited[neighbor] = 1


        visited = [0] * n
        count = 0
        for i in range(n):
            if (visited[i] == 0):
                BFS(connections, visited, i)
                count += 1
        return count
