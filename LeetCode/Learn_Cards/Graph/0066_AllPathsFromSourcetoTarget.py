
"""
  All Paths From Source to Target

Given a directed acyclic graph (DAG) of n nodes labeled from 0 to n - 1, find all possible paths from node 0 to node n - 1 and return them in any order.

The graph is given as follows: graph[i] is a list of all nodes you can visit from node i (i.e., there is a directed edge from node i to node graph[i][j]).

https://leetcode.com/explore/learn/card/graph/619/depth-first-search-in-graph/3849/
"""

"""
    Idea: if we are asked to output True/False if a path exists, we can do BFS or DFS. But if we are asked to return 
        all such paths, or one such path, it's best to use recursion (DFS) with a find_path function.
"""


class Solution:
    def allPathsSourceTarget(self, graph: List[List[int]]) -> List[List[int]]:

        if not graph or len(graph) == 0:
            return []

        def find_path(node, N):
            path.append(node)
            if node == N-1:
                paths.append(path.copy())
                return

            for neighbor in graph[node]:
                find_path(neighbor, N)
                path.pop()

        paths = []
        path = []
        N = len(graph)

        find_path(0, N)

        return paths
