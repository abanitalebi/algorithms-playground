
"""
  Minimum Height Trees

A tree is an undirected graph in which any two vertices are connected by exactly one path. In other words, any connected graph without simple cycles is a tree.

Given a tree of n nodes labelled from 0 to n - 1, and an array of n - 1 edges where edges[i] = [ai, bi] indicates that there is an undirected edge between the two nodes ai and bi in the tree, you can choose any node of the tree as the root. When you select a node x as the root, the result tree has height h. Among all possible rooted trees, those with minimum height (i.e. min(h))  are called minimum height trees (MHTs).

Return a list of all MHTs' root labels. You can return the answer in any order.

The height of a rooted tree is the number of edges on the longest downward path between the root and a leaf.

https://leetcode.com/explore/learn/card/graph/623/kahns-algorithm-for-topological-sorting/3953/
"""

"""
    Idea: In starting all leaf node are pushed into the queue, then they are removed from the tree, next new leaf node is pushed in the queue, this procedure keeps on going until we have only 1 or 2 node in our tree, which represent the result.
        Similar to Kahn's algorithm but instead of starting from 0 in-degree nodes, we start from leaf nodes.
"""


class Solution:
    def findMinHeightTrees(self, n: int, edges: List[List[int]]) -> List[int]:
        if n == 0:
            return []
        if n == 1:
            return [0]
        if n == 2:
            return [0,1]

        # Get a dictionary of edges and node degrees
        edge_map = {}
        degrees = [0] * n
        for e in edges:
            if edge_map.get(e[0]) is not None:
                edge_map[e[0]].append(e[1])
            else:
                edge_map[e[0]] = [e[1]]
            if edge_map.get(e[1]) is not None:
                edge_map[e[1]].append(e[0])
            else:
                edge_map[e[1]] = [e[0]]
            degrees[e[0]] += 1
            degrees[e[1]] += 1
        print("edge_map: {}".format(edge_map))
        print("degrees: {}".format(degrees))

        # BFS; add leaf nodes, and iteratively include any node that becomes leaf
        leaves = []
        for i in range(n):
            if degrees[i] == 1:
                leaves.append(i)
        print("leaves: {}".format(leaves))
        q = collections.deque(leaves)
        print("degrees: {}, q: {}".format(degrees, q))
        while q:
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()
                degrees[cur] -= 1
                if edge_map.get(cur) is None:
                    continue
                for node in edge_map.get(cur):
                    degrees[node] -= 1
                    if degrees[node] == 1:
                        q.append(node)
            # print("degrees: {}, q: {}".format(degrees, q))
            if (len(q) < 3) and (sum([1 for j in degrees if j > 0]) < 3):
                return list(q)


