
"""
  Min Cost to Connect All Points

You are given an array points representing integer coordinates of some points on a 2D-plane, where points[i] = [xi, yi].

The cost of connecting two points [xi, yi] and [xj, yj] is the manhattan distance between them: |xi - xj| + |yi - yj|, where |val| denotes the absolute value of val.

Return the minimum cost to make all points connected. All points are connected if there is exactly one simple path between any two points.

https://leetcode.com/explore/learn/card/graph/621/algorithms-to-construct-minimum-spanning-tree/3860/
"""


"""
    Idea: This is a MST problem (minimum spanning tree), that can be solved using the Kruskal's algorithm or the Prim's algorithm.
"""


class Solution:
    def minCostConnectPoints(self, points: List[List[int]]) -> int:

        if not points or len(points) == 0:
            return 0

        class Edge:
            def __init__(self, point1, point2, cost):
                self.point1 = point1
                self.point2 = point2
                self.cost = cost

            def __lt__(self, other):
                return self.cost < other.cost

        N = len(points)
        visited = [0] * N
        pq = []
        point1 = 0
        for i in range(1,N):
            point2 = i
            cost = abs(points[point1][0] - points[point2][0]) + abs(points[point1][1] - points[point2][1])
            edge = Edge(point1, point2, cost)
            pq.append(edge)
        visited[0] = 1

        heapq.heapify(pq)
        count = 0
        result = 0
        while pq and count < N-1:
            edge = heapq.heappop(pq)
            point1 = edge.point1
            point2 = edge.point2
            cost = edge.cost
            if visited[point2] == 0:
                result += cost
                visited[point2] = 1
                count += 1
                for j in range(N):
                    if visited[j] == 0:
                        new_cost = abs(points[point2][0] - points[j][0]) + abs(points[point2][1] - points[j][1])
                        new_edge = Edge(point2, j, new_cost)
                        heapq.heappush(pq, new_edge)

        return result


################################################################################
class Solution:
    def minCostConnectPoints(self, points: List[List[int]]) -> int:
        if not points or len(points) == 0:
            return 0
        size = len(points)
        pq = []
        visited = [False] * size
        result = 0
        count = size - 1
        # Add all edges from points[0] vertexs
        x1, y1 = points[0]
        for j in range(1, size):
            # Calculate the distance between two coordinates.
            x2, y2 = points[j]
            cost = abs(x1 - x2) + abs(y1 - y2)
            edge = Edge(0, j, cost)
            pq.append(edge)

        # Convert pq to a heap.
        heapq.heapify(pq)

        visited[0] = True
        while pq and count > 0:
            edge = heapq.heappop(pq)
            point1 = edge.point1
            point2 = edge.point2
            cost = edge.cost
            if not visited[point2]:
                result += cost
                visited[point2] = True
                for j in range(size):
                    if not visited[j]:
                        distance = abs(points[point2][0] - points[j][0]) + \
                                   abs(points[point2][1] - points[j][1])
                        heapq.heappush(pq, Edge(point2, j, distance))
                count -= 1
        return result

class Edge:
    def __init__(self, point1, point2, cost):
        self.point1 = point1
        self.point2 = point2
        self.cost = cost

    def __lt__(self, other):
        return self.cost < other.cost

if __name__ == "__main__":
    points = [[0,0],[2,2],[3,10],[5,2],[7,0]]
    solution = Solution()
    print(f"points = {points}")
    print(f"Minimum Cost to Connect Points = {solution.minCostConnectPoints(points)}")

