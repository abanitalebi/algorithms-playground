
"""
  Find if Path Exists in Graph

There is a bi-directional graph with n vertices, where each vertex is labeled from 0 to n - 1 (inclusive). The edges in the graph are represented as a 2D integer array edges, where each edges[i] = [ui, vi] denotes a bi-directional edge between vertex ui and vertex vi. Every vertex pair is connected by at most one edge, and no vertex has an edge to itself.

You want to determine if there is a valid path that exists from vertex start to vertex end.

Given edges and the integers n, start, and end, return true if there is a valid path from start to end, or false otherwise.

https://leetcode.com/explore/learn/card/graph/619/depth-first-search-in-graph/3893/
"""

"""
    Idea: Use a DFS/BFS from the start node to the end node. Each iteration add new nodes if they are not visited before.
"""


class Solution:
    def validPath(self, n: int, edges: List[List[int]], start: int, end: int) -> bool:
        if not edges or len(edges) == 0:
            if n == 1:
                return True
            else:
                return False

        dc = {}
        for edge in edges:
            if not dc.get(edge[0]):
                dc[edge[0]] = [edge[1]]
            else:
                dc[edge[0]].append(edge[1])
            if not dc.get(edge[1]):
                dc[edge[1]] = [edge[0]]
            else:
                dc[edge[1]].append(edge[0])
        # print("dc: {}".format(dc))

        stack = [start]
        visited = [0] * n
        while stack:
            s_size = len(stack)
            for i in range(s_size):
                cur = stack.pop()
                visited[cur] = 1
                if cur == end:
                    return True
                if dc.get(cur):
                    for j in dc[cur]:
                        if visited[j] == 0:
                            stack.append(j)
        return False
