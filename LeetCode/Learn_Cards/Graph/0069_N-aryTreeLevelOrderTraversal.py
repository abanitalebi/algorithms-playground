
"""
  N-ary Tree Level Order Traversal

Given an n-ary tree, return the level order traversal of its nodes' values.

Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value (See examples).

https://leetcode.com/explore/learn/card/graph/620/breadth-first-search-in-graph/3897/
"""

"""
# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children
"""

"""
# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children
"""


class Solution:
    def levelOrder(self, root: 'Node') -> List[List[int]]:
        if root is None:
            return []
        if root.children is None:
            return [root.val]

        output = [[root.val]]
        q = collections.deque([root])
        while q:
            q_size = len(q)
            level = []
            for i in range(q_size):
                cur = q.popleft()
                for neighbor in cur.children:
                    q.append(neighbor)
                    level.append(neighbor.val)
            output.append(level)

        return output[:-1]
