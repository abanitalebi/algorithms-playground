
"""
  Network Delay Time

You are given a network of n nodes, labeled from 1 to n. You are also given times, a list of travel times as directed edges times[i] = (ui, vi, wi), where ui is the source node, vi is the target node, and wi is the time it takes for a signal to travel from source to target.

We will send a signal from a given node k. Return the time it takes for all the n nodes to receive the signal. If it is impossible for all the n nodes to receive the signal, return -1.

https://leetcode.com/explore/learn/card/graph/622/single-source-shortest-path-algorithm/3863/
"""

"""
    Idea: Use a queue like BFS. Add src to it. Iterate and pop. Look at the neighbors; compute neighbors costs and 
        see if they are lower than the current value (initialize with sys.maxsize). Update if it's lower, and add to queue if not already there.
        If it asks for path length lower than XX, then track path lengths and add a constraint before updates.
"""


class Solution:
    def networkDelayTime(self, times: List[List[int]], n: int, k: int) -> int:

        if times is None or len(times) == 0:
            return 0

        edges = {}
        for i in range(len(times)):
            if edges.get(times[i][0]) is not None:
                edges[times[i][0]].append((times[i][1],times[i][2]))
            else:
                edges[times[i][0]] = [(times[i][1],times[i][2])]

        cost0 = sys.maxsize
        costs = [cost0] * n
        costs[k-1] = 0
        is_in_q = [0] * n
        is_in_q[k-1] = 1
        q = collections.deque([k])
        while q:
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()
                is_in_q[cur-1] = 0

                if edges.get(cur) is not None:
                    for neighbor in edges[cur]:
                        node = neighbor[0]
                        cost = neighbor[1]
                        if costs[cur-1]+cost < costs[node-1]:
                            costs[node-1] = costs[cur-1]+cost
                            if is_in_q[node-1] == 0:
                                q.append(node)
        if cost0 in costs:
            return -1
        else:
            return max(costs)
