
"""
  All Paths From Source to Target

Given a directed acyclic graph (DAG) of n nodes labeled from 0 to n - 1, find all possible paths from node 0 to node n - 1 and return them in any order.

The graph is given as follows: graph[i] is a list of all nodes you can visit from node i (i.e., there is a directed edge from node i to node graph[i][j]).

https://leetcode.com/explore/learn/card/graph/620/breadth-first-search-in-graph/3853/
"""

"""
    Idea: Use BFS but instead of appending nodes to the queue, append paths.
"""


########## BFS ############################
class Solution:
    def allPathsSourceTarget(self, graph: List[List[int]]) -> List[List[int]]:

        if not graph or len(graph) == 0:
            return []

        paths = []
        N = len(graph)

        q = collections.deque([[0]])
        while q:
            q_size = len(q)
            for i in range(q_size):
                cur_path = q.popleft()
                cur_node = cur_path[-1]

                for neighbor in graph[cur_node]:
                    if neighbor == N-1:
                        paths.append(cur_path + [neighbor])
                    else:
                        q.append(cur_path + [neighbor])

        return paths





########## DFS ############################
class Solution:
    def allPathsSourceTarget(self, graph: List[List[int]]) -> List[List[int]]:

        if not graph or len(graph) == 0:
            return []

        def find_path(node, N):
            path.append(node)
            if node == N-1:
                paths.append(path.copy())
                return

            for neighbor in graph[node]:
                find_path(neighbor, N)
                path.pop()

        paths = []
        path = []
        N = len(graph)

        find_path(0, N)

        return paths

