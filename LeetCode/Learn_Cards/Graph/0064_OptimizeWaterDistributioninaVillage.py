
"""
  Optimize Water Distribution in a Village

There are n houses in a village. We want to supply water for all the houses by building wells and laying pipes.

For each house i, we can either build a well inside it directly with cost wells[i - 1] (note the -1 due to 0-indexing), or pipe in water from another well to it. The costs to lay pipes between houses are given by the array pipes, where each pipes[j] = [house1j, house2j, costj] represents the cost to connect house1j and house2j together using a pipe. Connections are bidirectional.

Return the minimum total cost to supply water to all houses.

https://leetcode.com/explore/learn/card/graph/618/disjoint-set/3916/
"""

"""
    Idea: Add a virtual node, connect it to houses with edges weighted by the costs to build wells in these houses.
    The problem is now reduced to a Minimum Spanning Tree problem. Solve with Prim's algorithm.
"""


class Solution:
    def minCostToSupplyWater(self, n: int, wells: List[int], pipes: List[List[int]]) -> int:

        if wells is None or len(wells) == 0:
            return 0
        if pipes is None or len(pipes) == 0:
            return 0

        class Edge:
            def __init__(self, point1, point2, cost):
                self.point1 = point1
                self.point2 = point2
                self.cost = cost
            def __lt__(self, other):
                return self.cost < other.cost

        edges = pipes
        point1 = 0
        visited = [0] * (n+1)
        pq = []
        for i in range(n):
            cost = wells[i]
            pipe_v = [0, i+1, cost]
            edges.append(pipe_v)

            edge = Edge(0, i+1, cost)
            pq.append(edge)
        visited[0] = 1


        costs = {}
        for e in edges:
            p1, p2, c = e
            costs[(p1,p2)] = c
            costs[(p2,p1)] = c
        # print("costs: {}".format(costs))

        count = 0
        result = 0
        heapq.heapify(pq)
        while pq and count < n:
            # print("Executing loop...")
            edge = heapq.heappop(pq)
            point1 = edge.point1
            point2 = edge.point2
            cost = edge.cost
            if visited[point2] == 0:
                visited[point2] = 1
                result += cost
                count += 1
                for j in range(n+1):
                    if (visited[j] == 0) and (costs.get((point2,j)) is not None):
                        new_cost = costs[(point2,j)]
                        new_edge = Edge(point2, j, new_cost)
                        heapq.heappush(pq, new_edge)
                        # print("Edge added")
            # print("count: {}, visited: {}, point1: {}, point2: {}, cost: {}".format(count, visited, point1, point2, cost))

        return result
