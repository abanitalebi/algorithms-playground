
"""
  Rotting Oranges

You are given an m x n grid where each cell can have one of three values:

0 representing an empty cell,
1 representing a fresh orange, or
2 representing a rotten orange.
Every minute, any fresh orange that is 4-directionally adjacent to a rotten orange becomes rotten.

Return the minimum number of minutes that must elapse until no cell has a fresh orange. If this is impossible, return -1.

https://leetcode.com/explore/learn/card/graph/620/breadth-first-search-in-graph/3898/
"""

"""
    Idea: Use BFS. Initialize the queue with all 2's.
"""


class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:

        if (grid is None) or len(grid) == 0:
            return 0

        q = collections.deque()
        goal = 0
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 2:
                    q.append((i,j))
                elif grid[i][j] == 1:
                    goal += 1

        iters = 0
        count = 0
        h = [-1, 1, 0, 0]
        w = [0, 0, -1, 1]
        while q:
            q_size = len(q)
            for k in range(q_size):
                cur = q.popleft()
                for (row, col) in zip(h,w):
                    if (cur[0]+row >= 0) and (cur[0]+row < len(grid)) and (cur[1]+col >= 0) and (cur[1]+col < len(grid[0])) and (grid[cur[0]+row][cur[1]+col] == 1):
                        grid[cur[0]+row][cur[1]+col] = 2
                        q.append((cur[0]+row, cur[1]+col))
                        count += 1
            if len(q) > 0:
                iters += 1

        # print("iters: {}, goal: {}, count: {}".format(iters, goal, count))
        if count == goal:
            return iters
        else:
            return -1
