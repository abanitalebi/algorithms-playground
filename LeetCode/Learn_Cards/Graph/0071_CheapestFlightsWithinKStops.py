
"""
  Cheapest Flights Within K Stops

There are n cities connected by some number of flights. You are given an array flights where flights[i] = [fromi, toi, pricei] indicates that there is a flight from city fromi to city toi with cost pricei.

You are also given three integers src, dst, and k, return the cheapest price from src to dst with at most k stops. If there is no such route, return -1.

https://leetcode.com/explore/learn/card/graph/622/single-source-shortest-path-algorithm/3866/
"""

"""
    Idea: Use a queue like BFS. Add src to it. Iterate and pop. Look at the neighbors; compute neighbors costs and 
        see if they are lower than the current value (initialize with sys.maxsize). Update if it's lower, and add to queue if not already there.
        If it asks for path length lower than XX, then track path lengths and add a constraint before updates.
"""


class Solution:
    def findCheapestPrice(self, n: int, flights: List[List[int]], src: int, dst: int, k: int) -> int:

        if n == 1:
            return 0
        # if n == 2:
        #     return flights[0][2]

        edges = {}
        for edge in flights:
            if edges.get(edge[0]) is not None:
                edges[edge[0]].append([edge[1], edge[2]])
            else:
                edges[edge[0]] = [[edge[1], edge[2]]]
        print("edges: {}".format(edges))

        costs = [sys.maxsize] * n
        costs[src] = 0
        cost0 = costs[dst]
        q = collections.deque([src])
        is_in_q = [0] * n
        is_in_q[src] = 1
        path_len = [0] * n
        while q:
            print("q: {}".format(q))
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()
                is_in_q[cur] = 0

                if edges.get(cur) is not None:
                    for neighbor in edges[cur]:

                        print("  neighbor: {}, candidate_c: {}, c: {}, path_len[cur]: {}".format(neighbor, costs[cur]+neighbor[1], costs[neighbor[0]], path_len[cur]))

                        if (costs[cur]+neighbor[1] < costs[neighbor[0]]) and (path_len[cur] <= k):
                            costs[neighbor[0]] = costs[cur]+neighbor[1]
                            path_len[neighbor[0]] = path_len[cur] + 1
                            if is_in_q[neighbor[0]] == 0:
                                q.append(neighbor[0])
        print("dst cost: {}".format(costs[dst]))
        if costs[dst] == cost0:
            return -1
        else:
            return costs[dst]
