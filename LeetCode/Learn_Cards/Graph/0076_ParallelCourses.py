
"""
  Parallel Courses

You are given an integer n, which indicates that there are n courses labeled from 1 to n. You are also given an array relations where relations[i] = [prevCoursei, nextCoursei], representing a prerequisite relationship between course prevCoursei and course nextCoursei: course prevCoursei has to be taken before course nextCoursei.

In one semester, you can take any number of courses as long as you have taken all the prerequisites in the previous semester for the courses you are taking.

Return the minimum number of semesters needed to take all courses. If there is no way to take all the courses, return -1.

https://leetcode.com/explore/learn/card/graph/623/kahns-algorithm-for-topological-sorting/3954/
"""

class Solution:
    def minimumSemesters(self, n: int, relations: List[List[int]]) -> int:
        if n == 0:
            return -1
        if n == 1:
            return 1

        edge_map = {}
        in_map = {}
        for e in relations:
            if edge_map.get(e[0]) is not None:
                edge_map[e[0]].append(e[1])
            else:
                edge_map[e[0]] = [e[1]]
            if in_map.get(e[1]) is not None:
                in_map[e[1]].append(e[0])
            else:
                in_map[e[1]] = [e[0]]
        print("edge_map: {}".format(edge_map))
        indeg = [0] * n
        for i in range(n):
            if in_map.get(i+1) is not None:
                indeg[i] = len(in_map[i+1])
        print("indeg: {}".format(indeg))

        z = []
        for i in range(n):
            if indeg[i] == 0:
                z.append(i+1)
        print("zero in degree: {}".format(z))

        q = collections.deque(z)
        visited = [0] * n
        count = 0
        while q:
            # print("q: {}".format(q))
            count += 1
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()
                visited[cur-1] = 1
                if edge_map.get(cur) is None:
                    continue
                for node in edge_map[cur]:
                    indeg[node-1] -= 1
                    if indeg[node-1] == 0:
                        q.append(node)

        print("indeg at the end: {}".format(indeg))
        if sum(indeg) == 0:
            return count
        else:
            return -1
