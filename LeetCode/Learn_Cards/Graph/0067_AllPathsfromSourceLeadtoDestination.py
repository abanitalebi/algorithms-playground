
"""
  All Paths from Source Lead to Destination

Given the edges of a directed graph where edges[i] = [ai, bi] indicates there is an edge between nodes ai and bi, and two nodes source and destination of this graph, determine whether or not all paths starting from source eventually, end at destination, that is:

At least one path exists from the source node to the destination node
If a path exists from the source node to a node with no outgoing edges, then that node is equal to destination.
The number of possible paths from source to destination is a finite number.
Return true if and only if all roads from source lead to destination.

https://leetcode.com/explore/learn/card/graph/619/depth-first-search-in-graph/3951/
"""


class Solution:
    def leadsToDestination(self, n: int, edges: List[List[int]], source: int, destination: int) -> bool:

        if n == 1:
            return True

        edge_map = {}
        for edge in edges:
            if edge_map.get(edge[0]) is not None:
                edge_map[edge[0]].append(edge[1])
            else:
                edge_map[edge[0]] = [edge[1]]
        print("edge_map: {}".format(edge_map))

        q = collections.deque([source])
        parents = {}
        while q:
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()

                # if cur == destination:
                # print("cur: {}, parent[cur]: {}, edge_map[cur]: {}".format(cur, parents[cur], edge_map[cur]))

                if (cur == destination) and (edge_map.get(cur) is not None) and (edge_map[cur][0] == destination):
                    return False
                # if (cur != destination) and (parents.get(cur) is not None) and (parents[cur] == cur):
                #     return False

                if edge_map.get(cur) is None:
                    if cur != destination:
                        return False
                else:
                    for e in edge_map[cur]:
                        q.append(e)
                        if parents.get(e) is not None:
                            if (parents[e] != cur) and (e != destination):
                                return False
                        else:
                            parents[e] = cur


                # print("q: {}".format(q))

        return True
