
"""
  Course Schedule II

There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
Return the ordering of courses you should take to finish all courses. If there are many valid answers, return any of them. If it is impossible to finish all courses, return an empty array.

https://leetcode.com/explore/learn/card/graph/623/kahns-algorithm-for-topological-sorting/3868/
"""

"""
    Idea: Use Kahn's algorithm for topological sorting. Gather dictionaries of in-degrees and edges.
"""

class Solution:
    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        if numCourses == 0:
            return []
        if numCourses == 1:
            return [0]

        # Build edge mapping dictionaries
        edges = {}
        in_edges = {}
        for edge in prerequisites:
            if edges.get(edge[1]) is not None:
                edges[edge[1]].append(edge[0])
            else:
                edges[edge[1]] = [edge[0]]
            if in_edges.get(edge[0]) is not None:
                in_edges[edge[0]].append(edge[1])
            else:
                in_edges[edge[0]] = [edge[1]]

        # Compute in-degrees of all nodes & find the ones with 0 in-degree
        z = []
        indeg = [0] * numCourses
        for i in range(numCourses):
            if in_edges.get(i) is None:
                z.append(i)
                indeg[i] = 0
            else:
                indeg[i] = len(in_edges.get(i))

        # Kahn's algorithm for topological sorting
        q = collections.deque(z)
        out = []
        visited = [0] * numCourses
        while q:
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()
                out.append(cur)

                if edges.get(cur) is None:
                    continue
                for node in edges.get(cur):
                    indeg[node] -= 1
                    if (indeg[node] == 0) and (visited[node] == 0):
                        q.append(node)
        if len(out) < numCourses:
            return []
        else:
            return out
