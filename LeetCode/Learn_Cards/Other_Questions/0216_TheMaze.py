
"""
    The Maze

There is a ball in a maze with empty spaces (represented as 0) and walls (represented as 1). The ball can go through the empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.

Given the m x n maze, the ball's start position and the destination, where start = [startrow, startcol] and destination = [destinationrow, destinationcol], return true if the ball can stop at the destination, otherwise return false.

You may assume that the borders of the maze are all walls (see examples).

https://leetcode.com/problems/the-maze/
"""

"""
    Idea: Use a BFS. Start from the starting point and look at the four neighbors. Add them to queue if eligible.
"""


"""
public class Solution {
    public boolean hasPath(int[][] maze, int[] start, int[] destination) {
        boolean[][] visited = new boolean[maze.length][maze[0].length];
        int[][] dirs={{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
        Queue < int[] > queue = new LinkedList < > ();
        queue.add(start);
        visited[start[0]][start[1]] = true;
        while (!queue.isEmpty()) {
            int[] s = queue.remove();
            if (s[0] == destination[0] && s[1] == destination[1])
                return true;
            for (int[] dir: dirs) {
                int x = s[0] + dir[0];
                int y = s[1] + dir[1];
                while (x >= 0 && y >= 0 && x < maze.length && y < maze[0].length && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                }
                if (!visited[x - dir[0]][y - dir[1]]) {
                    queue.add(new int[] {x - dir[0], y - dir[1]});
                    visited[x - dir[0]][y - dir[1]] = true;
                }
            }
        }
        return false;
    }
}
"""


class Solution:
    def hasPath(self, maze: List[List[int]], start: List[int], destination: List[int]) -> bool:
        visited = []
        m = len(maze)
        n = len(maze[0])

        dhh = [-1,1,0,0]
        dww = [0,0,-1,1]

        q = collections.deque([start])
        while q:
            q_len = len(q)
            for i in range(q_len):
                cur = q.popleft()
                visited.append(cur)

                for (dh,dw) in zip(dhh,dww):
                    h,w = cur
                    while (0 <= h+dh < m) and (0 <= w+dw <= n):
                        h = h + dh
                        w = w + dw
                    if [h,w] not in visited:
                        q.append([h,w])
                    if h == destination[0] and w == destination[1]:
                        return True
        return False
