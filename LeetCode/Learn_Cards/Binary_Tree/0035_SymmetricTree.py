
"""
  Symmetric Tree

Given the root of a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).

https://leetcode.com/explore/learn/card/data-structure-tree/17/solve-problems-recursively/536/
"""

"""
    Idea: Recursion:
    For two trees to be mirror images,
        the following three conditions must be true
        1 - Their root node's key must be same
        2 - left subtree of left tree and right subtree
          of the right tree have to be mirror images
        3 - right subtree of left tree and left subtree
           of right tree have to be mirror images
        https://www.geeksforgeeks.org/symmetric-tree-tree-which-is-mirror-image-of-itself/
"""







# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:

    def isMirror(self, root1, root2):

        if (root1 is None) and (root2 is None):
            return True
        if (root1 is not None) and (root2 is None):
            return False
        if (root1 is None) and (root2 is not None):
            return False

        if root1.val != root2.val:
            return False
        else:
            return self.isMirror(root1.left, root2.right) and self.isMirror(root1.right, root2.left)



    def isSymmetric(self, root: Optional[TreeNode]) -> bool:

        return self.isMirror(root.left, root.right)

