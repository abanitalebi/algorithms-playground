
"""
  Lowest Common Ancestor of a Binary Tree

Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.

According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes p and q as the lowest node in T that has both p and q as descendants (where we allow a node to be a descendant of itself).”

https://leetcode.com/explore/learn/card/data-structure-tree/133/conclusion/932/
"""

"""
    Idea: find paths ending at the given nodes. Then compare them from left (i.e. root) and find the first mismatch index.
        Give path as input to the recursive function and modify it within the function (instead of being an output)
"""



# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


def pr(nodes):
    values = []
    for i in nodes:
        values.append(i.val)
    return values

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':

        if root is None:
            return None

        def find_path(cur, path, node):

            if cur is None:
                return False

            path.append(cur)

            if cur is node:
                return True
            if cur.left is not None:
                if find_path(cur.left, path, node):
                    return True
            if cur.right is not None:
                if find_path(cur.right, path, node):
                    return True

            path.pop()
            return False

        path1 = []
        find_path(root, path1, p)
        path2 = []
        find_path(root, path2, q)

        # print("path1: {}".format(pr(path1)))
        # print("path2: {}".format(pr(path2)))

        for i in range(min(len(path1), len(path2))):
            if path1[i].val != path2[i].val:
                return path1[i-1]
        if len(path1) <= len(path2):
            if path1 == path2[:len(path1)]:
                return path1[-1]
        if len(path2) <= len(path1):
            if path2 == path1[:len(path2)]:
                return path2[-1]

