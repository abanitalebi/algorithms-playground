
"""
  Binary Tree Inorder Traversal

Given the root of a binary tree, return the inorder traversal of its nodes' values.

https://leetcode.com/explore/learn/card/data-structure-tree/134/traverse-a-tree/929/
"""

"""
    Idea: Use recursion.
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def inorderTraversal(self, root: Optional[TreeNode]) -> List[int]:

        if root is None:
            return root

        if root.left is not None:
            left = self.inorderTraversal(root.left)
        else:
            left = []

        if root.right is not None:
            right = self.inorderTraversal(root.right)
        else:
            right = []

        if (root.left is None) and (root.right is None):
            return [root.val]

        return left + [root.val] + right


