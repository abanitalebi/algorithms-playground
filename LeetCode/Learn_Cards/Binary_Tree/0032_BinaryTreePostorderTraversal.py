
"""
  Binary Tree Postorder Traversal

Given the root of a binary tree, return the postorder traversal of its nodes' values.

https://leetcode.com/explore/learn/card/data-structure-tree/134/traverse-a-tree/930/
"""

"""
    Idea: Use recursion.
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def postorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        if root is None:
            return root

        if root.left is not None:
            left = self.postorderTraversal(root.left)
        else:
            left = []

        if root.right is not None:
            right = self.postorderTraversal(root.right)
        else:
            right = []

        if (root.left is None) and (root.right is None):
            return [root.val]

        return left + right + [root.val]




