"""
  Populating Next Right Pointers in Each Node II

Given a binary tree

struct Node {
  int val;
  Node *left;
  Node *right;
  Node *next;
}
Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.

Initially, all next pointers are set to NULL.

https://leetcode.com/explore/learn/card/data-structure-tree/133/conclusion/1016/
"""

"""
    Idea: Use a queue for BFS. But  a for loop for each level within a while q.
"""




"""
# Definition for a Node.
class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next
"""

def pr(q):
    qq = []
    for i in q:
        qq.append(i.val)
    return qq


class Solution:
    def connect(self, root: 'Node') -> 'Node':

        if root is None:
            return None
        if (root.left is None) and (root.right is None):
            return root

        q = [root]
        while q:
            q_size = len(q)
            for i in range(q_size):
                removed = q.pop(0)
                if removed.left is not None:
                    q.append(removed.left)
                if removed.right is not None:
                    q.append(removed.right)

                if i < q_size-1:
                    removed.next = q[0]

        return root

