
"""
  Serialize and Deserialize Binary Tree

Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.

Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.

Clarification: The input/output format is the same as how LeetCode serializes a binary tree. You do not necessarily need to follow this format, so please be creative and come up with different approaches yourself.

https://leetcode.com/explore/learn/card/data-structure-tree/133/conclusion/995/
"""


"""
    Idea: Perform a BFS, convert to a list of strings and then one long string. Reverse, then use 2 pointers to build the tree.
"""



# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Codec:

    def serialize(self, root):
        """Encodes a tree to a single string.

        :type root: TreeNode
        :rtype: str
        """

        if root is None:
            return "None"

        q = collections.deque([root])
        out = []
        count = 0
        while q:
            q_size = len(q)
            for i in range(q_size):
                removed = q.popleft()
                out.append(str(removed.val) if type(removed) != str else removed)
                if (removed != "None") and (removed.left is not None):
                    q.append(removed.left)
                elif removed != "None":
                    q.append("None")
                if (removed != "None") and (removed.right is not None):
                    q.append(removed.right)
                elif removed != "None":
                    q.append("None")

        while out[-1] == "None":
            out.pop()
        # print(out)
        out = ','.join([o for o in out])
        # print(out)
        return out


    def deserialize(self, data):
        """Decodes your encoded data to tree.

        :type data: str
        :rtype: TreeNode
        """

        if data is None or data == "None":
            return None

        values = data.split(',')
        if len(values) == 1:
            return TreeNode(values[0])

        # print("decode")
        # print("Values: {}".format(values))
        nodes = []
        for i in range(len(values)):
            if values[i] != "None":
                nodes.append(TreeNode(values[i]))
            else:
                nodes.append(None)

        # print("Nodes before: {}".format(nodes))

        p1 = 0
        p2 = 1
        L = len(values)
        while p1 < L:
            # print("p1: {},  p2: {}".format(p1, p2))
            if nodes[p1] is not None:
                if p2 < L:
                    nodes[p1].left = nodes[p2]
                if p2+1 < L:
                    nodes[p1].right = nodes[p2+1]
                p2 += 2

            p1 += 1

        return nodes[0]


# Your Codec object will be instantiated and called as such:
# ser = Codec()
# deser = Codec()
# ans = deser.deserialize(ser.serialize(root))
