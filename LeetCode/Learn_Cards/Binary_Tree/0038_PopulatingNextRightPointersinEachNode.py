
"""
  Populating Next Right Pointers in Each Node

You are given a perfect binary tree where all leaves are on the same level, and every parent has two children. The binary tree has the following definition:

struct Node {
  int val;
  Node *left;
  Node *right;
  Node *next;
}
Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.

Initially, all next pointers are set to NULL.

https://leetcode.com/explore/learn/card/data-structure-tree/133/conclusion/994/
"""

"""
    Perform a LevelOrder traversal. Count the number of pops, 
        and if it's a power of 2 minus 1, set next to None, otherwise to next node.
"""




"""
# Definition for a Node.
class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next
"""

class Solution:
    def connect(self, root: 'Node') -> 'Node':

        if root is None:
            return None

        if root.left is None:
            return root

        cur = root
        q = [cur, cur.left, cur.right]
        cur.left.next = cur.right
        count = 0
        level = 0
        while len(q) > 1:
            removed = q.pop(0)
            cur = q[0]
            count += 1
            if cur.left is not None:
                q.append(cur.left)
                q.append(cur.right)

            if count == 2 ** (level+1) - 1:
                level += 1
                removed.next = None
            else:
                removed.next = cur

        return root

