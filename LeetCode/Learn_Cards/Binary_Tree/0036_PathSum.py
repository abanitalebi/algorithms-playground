
"""
  Path Sum

Given the root of a binary tree and an integer targetSum, return true if the tree has a root-to-leaf path such that adding up all the values along the path equals targetSum.

A leaf is a node with no children.

https://leetcode.com/explore/learn/card/data-structure-tree/17/solve-problems-recursively/537/
"""

"""
    Idea: Recursion. Define a new function that takes a node and a current reduced sum. Each time subtract the current
        Node's value from the remaining sum, and call the function again until it reaches a leaf node.
        
        Point: A recursively called function, that takes a node & a value as input.
"""



# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:

    def check_sum(self, cur, s):
        if (cur.left is None) and (cur.right is None):
            if s == cur.val:
                return True
        else:
            if cur.left is not None:
                left = self.check_sum(cur.left, s - cur.val)
                if left:
                    return True
            if cur.right is not None:
                right = self.check_sum(cur.right, s - cur.val)
                if right:
                    return True


    def hasPathSum(self, root: Optional[TreeNode], targetSum: int) -> bool:
        if root is None:
            return False

        out = self.check_sum(root, targetSum)
        if out:
            return True
        else:
            return False
