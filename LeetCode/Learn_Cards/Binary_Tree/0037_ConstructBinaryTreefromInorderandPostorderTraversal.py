
"""
  Construct Binary Tree from Inorder and Postorder Traversal

Given two integer arrays inorder and postorder where inorder is the inorder traversal of a binary tree and postorder is the postorder traversal of the same tree, construct and return the binary tree.
inorder and postorder consist of unique values.

https://leetcode.com/explore/learn/card/data-structure-tree/133/conclusion/942/
"""

"https://www.geeksforgeeks.org/construct-a-binary-tree-from-postorder-and-inorder/"

"""
1) We first find the last node in post[]. The last node is “1”, we know this value is root as the root always appears at the end of postorder traversal.
2) We search “1” in in[] to find the left and right subtrees of the root. Everything on the left of “1” in in[] is in the left subtree and everything on right is in the right subtree. 

3) We recur the above process for following two. 
….b) Recur for in[] = {6, 3, 7} and post[] = {6, 7, 3} 
…….Make the created tree as right child of root. 
….a) Recur for in[] = {4, 8, 2, 5} and post[] = {8, 4, 5, 2}. 
…….Make the created tree as left child of root.
Below is the implementation of the above idea. One important observation is, we recursively call for the right subtree before the left subtree as we decrease the index of the postorder index whenever we create a new node. 
"""


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:

    def buildUtil(self, In, post, inStrt, inEnd, pIndex):

        if (inStrt > inEnd):
            return None

        node = TreeNode(post[pIndex[0]])
        pIndex[0] -= 1

        if (inStrt == inEnd):
            return node

        iIndex = self.search(In, inStrt, inEnd, node.val)

        node.right = self.buildUtil(In, post, iIndex + 1, inEnd, pIndex)
        node.left = self.buildUtil(In, post, inStrt, iIndex - 1, pIndex)

        return node


    def search(self, arr, strt, end, value):
        i = 0
        for i in range(strt, end + 1):
            if (arr[i] == value):
                break
        return i


    def buildTree(self, inorder: List[int], postorder: List[int]) -> Optional[TreeNode]:
        n = len(inorder)
        pIndex = [n - 1]
        return self.buildUtil(inorder, postorder, 0, n - 1, pIndex)


