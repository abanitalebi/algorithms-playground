
"""
  Maximum Length of Repeated Subarray

Given two integer arrays nums1 and nums2, return the maximum length of a subarray that appears in both arrays.

https://leetcode.com/explore/learn/card/dynamic-programming/647/more-practice-problems/4078/
"""

class Solution(object):
    def findLength(self, A, B):
        memo = [[0] * (len(B) + 1) for _ in range(len(A) + 1)]
        for i in range(len(A) - 1, -1, -1):
            for j in range(len(B) - 1, -1, -1):
                if A[i] == B[j]:
                    memo[i][j] = memo[i + 1][j + 1] + 1
        return max(max(row) for row in memo)





class Solution:
    def findLength(self, nums1: List[int], nums2: List[int]) -> int:

        n = len(nums1)
        m = len(nums2)
        idx = {}
        for i in range(len(nums2)):
            if nums2[i] not in idx:
                idx[nums2[i]] = [i]
            else:
                idx[nums2[i]].append(i)

        ml = 0
        for i in range(len(nums1)):
            if nums1[i] not in idx:
                continue
            js = idx[nums1[i]]
            for j in js:
                k = 0
                l = 0
                while i+k < n and j+k < m and nums1[i+k] == nums2[j+k]:
                    l += 1
                    k += 1
                if l > ml:
                    ml = l
            if ml == min(m,n):
                break

        return ml
