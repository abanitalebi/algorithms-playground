
"""
  Decode Ways

A message containing letters from A-Z can be encoded into numbers using the following mapping:

'A' -> "1"
'B' -> "2"
...
'Z' -> "26"
To decode an encoded message, all the digits must be grouped then mapped back into letters using the reverse of the mapping above (there may be multiple ways). For example, "11106" can be mapped into:

"AAJF" with the grouping (1 1 10 6)
"KJF" with the grouping (11 10 6)
Note that the grouping (1 11 06) is invalid because "06" cannot be mapped into 'F' since "6" is different from "06".

Given a string s containing only digits, return the number of ways to decode it.

The test cases are generated so that the answer fits in a 32-bit integer.

https://leetcode.com/explore/learn/card/dynamic-programming/633/common-patterns-continued/4139/
"""

"""
    Idea: dynamic programming: recursion: n = len(s), then f(n) can be written in terms of f(n-1) & f(n-2) depending on the last 1 or 2 letters.
"""


class Solution:
    def numDecodings(self, s: str) -> int:

        memo = {0: 0, 1: 0 if s == "0" else 1}
        def f(n):
            if n not in memo:
                if int(s[0]) == 0:
                    memo[n] = 0
                elif n == 2:
                    if int(s[1]) == 0:
                        if s[0:2] == "10" or s[0:2] == "20":
                            memo[2] = 1
                        else:
                            memo[2] = 0
                    else:
                        if int(s[0]) == 0:
                            memo[2] = 0
                        else:
                            if 0 < int(s[0:2]) <= 26:
                                memo[2] = 2
                            else:
                                memo[2] = 1
                else:
                    if int(s[n-1]) == 0:
                        if s[n-2] == "1" or s[n-2] == "2":
                            memo[n] = f(n-2)
                        else:
                            memo[n] = 0
                    else:
                        if 0 < int(s[n-2:n]) < 27 and s[n-2] != "0":
                            memo[n] = f(n-1) + f(n-2)
                        else:
                            memo[n] = f(n-1)

            return memo[n]

        xx = f(len(s))
        print("memo: {}".format(memo))
        return xx
