
"""
  Domino and Tromino Tiling

You have two types of tiles: a 2 x 1 domino shape and a tromino shape. You may rotate these shapes.


Given an integer n, return the number of ways to tile an 2 x n board. Since the answer may be very large, return it modulo 109 + 7.

In a tiling, every square must be covered by a tile. Two tilings are different if and only if there are two 4-directionally adjacent cells on the board such that exactly one of the tilings has both squares occupied by a tile.

https://leetcode.com/explore/learn/card/dynamic-programming/647/more-practice-problems/4080/
"""

class Solution:
    def numTilings(self, n: int) -> int:

        memo = {0: 0, 1: 1, 2: 2, 3: 5}
        def f(i):
            if i < 0:
                memo[i] = 0
            if i not in memo:
                memo[i] = f(i-1)  +  f(i-2)
                for j in range(3,i+1):
                    memo[i] += max(1,f(i-j)) * 2

            return memo[i]

        out = f(n)
        print("memo: {}".format(memo))
        return out % (10**9 + 7)
