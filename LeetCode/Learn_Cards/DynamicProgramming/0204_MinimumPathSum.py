
"""
  Minimum Path Sum

Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right, which minimizes the sum of all numbers along its path.

Note: You can only move either down or right at any point in time.

https://leetcode.com/explore/learn/card/dynamic-programming/634/matrix-path-based-dp/4132/
"""

class Solution:
    def minPathSum(self, grid: List[List[int]]) -> int:

        m = len(grid) - 1
        n = len(grid[0]) - 1

        if m == 0:
            return sum(grid[0])
        if n == 0:
            return sum([grid[i][0] for i in range(m+1)])

        memo = {(0,0): grid[0][0], (0,1): grid[0][0]+grid[0][1], (1,0): grid[0][0]+grid[1][0]}
        memo[(1,1)] = grid[0][0] + min(grid[0][1], grid[1][0]) + grid[1][1]

        def f(i,j):
            if i < 0 or j < 0:
                memo[(i,j)] = sys.maxsize
            else:
                if (i,j) not in memo:
                    memo[(i,j)] = min(f(i-1,j), f(i,j-1)) + grid[i][j]

            return memo[(i,j)]

        out = f(m,n)
        print("memo: {}".format(memo))
        return out
