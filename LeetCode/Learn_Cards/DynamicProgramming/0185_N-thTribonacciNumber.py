
"""
  N-th Tribonacci Number

The Tribonacci sequence Tn is defined as follows:

T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.

Given n, return the value of Tn.

https://leetcode.com/explore/learn/card/dynamic-programming/631/strategy-for-solving-dp-problems/4041/
"""


class Solution:
    def tribonacci(self, n: int) -> int:
        # T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.

        if n == 0:
            return 0
        if n == 1:
            return 1
        if n == 2:
            return 1

        memo = {0: 0, 1: 1, 2: 1}
        def f(i):
            if i not in memo:
                memo[i] = f(i-1) + f(i-2) + f(i-3)
            return memo[i]

        return f(n)
