
"""
  House Robber

You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security systems connected and it will automatically contact the police if two adjacent houses were broken into on the same night.

Given an integer array nums representing the amount of money of each house, return the maximum amount of money you can rob tonight without alerting the police.

https://leetcode.com/explore/featured/card/dynamic-programming/631/strategy-for-solving-dp-problems/4148/
"""

"""
    Idea: DP: recursion + memoization:  f(n) = max(f(n-1), f(n-2)+nums[n])
"""


class Solution:
    def rob(self, nums: List[int]) -> int:
        # f(n) = max(f(n-1), f(n-2)+nums[n])
        n = len(nums)

        if n == 1:
            return nums[0]

        memo = {0: nums[0], 1: max(nums[0], nums[1])}
        def f(i):
            # if i == 0:
            #     return nums[0]
            # if i == 1:
            #     return max(nums[0], nums[1])

            if i not in memo:
                memo[i] = max(f(i-1), f(i-2) + nums[i])

            return memo[i]

        return f(n-1)
