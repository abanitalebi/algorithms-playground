
"""
  Number of Dice Rolls With Target Sum

You have n dice and each die has k faces numbered from 1 to k.

Given three integers n, k, and target, return the number of possible ways (out of the kn total ways) to roll the dice so the sum of the face-up numbers equals target. Since the answer may be too large, return it modulo 109 + 7.

https://leetcode.com/explore/learn/card/dynamic-programming/647/more-practice-problems/4079/
"""

"""
    Idea: DP with recursion: Use f(target, i) where target is the remaining amount and i is number of dice rolls left
"""

class Solution:
    def numRollsToTarget(self, n: int, k: int, target: int) -> int:


        memo = {}
        for j in range(1,k+1):
            memo[(j,1)] = 1

        def f(t, i):
            if i < 1:
                memo[(t,i)] = 0
            if t <= 0:
                memo[(t,i)] = 0
            if i == 1 and 1 <= t <= k:
                memo[(t,i)] = 1
            if (t,i) not in memo:
                memo[(t,i)] = 0
                for j in range(1,k+1):
                    memo[(t,i)] += f(t-j, i-1)

            return memo[(t,i)]

        out = f(target, n)
        print("memo: {}".format(out))
        return out % (10**9 + 7)
