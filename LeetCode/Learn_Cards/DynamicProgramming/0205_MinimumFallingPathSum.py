
"""
  Minimum Falling Path Sum

Given an n x n array of integers matrix, return the minimum sum of any falling path through matrix.

A falling path starts at any element in the first row and chooses the element in the next row that is either directly below or diagonally left/right. Specifically, the next element from position (row, col) will be (row + 1, col - 1), (row + 1, col), or (row + 1, col + 1).

https://leetcode.com/explore/learn/card/dynamic-programming/634/matrix-path-based-dp/4133/
"""

class Solution:
    def minFallingPathSum(self, matrix: List[List[int]]) -> int:

        n = len(matrix)

        # R = Row and C = Column
        # We begin from second last row and keep
        # adding maximum sum.
        for R in range(n - 2, -1, -1) :
            for C in range(n) :

                # best = min(A[R+1][C-1], A[R+1][C],
                # A[R+1][C+1])
                best = matrix[R + 1][C]
                if C > 0 :
                    best = min(best, matrix[R + 1][C - 1])
                if C + 1 < n :
                    best = min(best, matrix[R + 1][C + 1])

                matrix[R][C] = matrix[R][C] + best

        ans = sys.maxsize

        for i in range(n) :
            ans = min(ans, matrix[0][i])

        return ans

