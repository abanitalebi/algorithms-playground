
"""
  Unique Paths

There is a robot on an m x n grid. The robot is initially located at the top-left corner (i.e., grid[0][0]). The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]). The robot can only move either down or right at any point in time.

Given the two integers m and n, return the number of possible unique paths that the robot can take to reach the bottom-right corner.

The test cases are generated so that the answer will be less than or equal to 2 * 109.

https://leetcode.com/explore/learn/card/dynamic-programming/634/matrix-path-based-dp/4130/
"""


class Solution:
    def uniquePaths(self, m: int, n: int) -> int:

        memo = {(1,1): 2}
        def f(i,j):
            if i <= 0 or j <= 0:
                memo[(i,j)] = 0
            elif (i,j) not in memo:
                memo[(i,j)] = f(i-1, j) + f(i, j-1)

            return memo[(i,j)]

        return int(f(m,n)/2)
