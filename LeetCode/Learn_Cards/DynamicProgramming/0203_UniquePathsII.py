
"""
  Unique Paths II

You are given an m x n integer array grid. There is a robot initially located at the top-left corner (i.e., grid[0][0]). The robot tries to move to the bottom-right corner (i.e., grid[m-1][n-1]). The robot can only move either down or right at any point in time.

An obstacle and space are marked as 1 or 0 respectively in grid. A path that the robot takes cannot include any square that is an obstacle.

Return the number of possible unique paths that the robot can take to reach the bottom-right corner.

The testcases are generated so that the answer will be less than or equal to 2 * 109.

https://leetcode.com/explore/learn/card/dynamic-programming/634/matrix-path-based-dp/4131/
"""

class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid: List[List[int]]) -> int:
        m = len(obstacleGrid) - 1
        n = len(obstacleGrid[0]) - 1

        if obstacleGrid[0][0] == 1:
            return 0

        if m == 0 and n == 0:
            return 1 if obstacleGrid[0][0] == 0 else 0

        if m == 0:
            if sum(obstacleGrid[0]) == 0:
                return 1
            else:
                return 0
        if n == 0:
            for i in range(len(obstacleGrid)):
                if obstacleGrid[i][0] == 1:
                    return 0
            return 1


        memo = {}
        if obstacleGrid[0][1] == 0 and obstacleGrid[1][0] == 0:
            memo[(1,1)] = 2
        elif obstacleGrid[0][1] == 1 and obstacleGrid[1][0] == 0:
            memo[(1,1)] = 1
        elif obstacleGrid[0][1] == 0 and obstacleGrid[1][0] == 1:
            memo[(1,1)] = 1
        else:
            memo[(1,1)] = 0

        if obstacleGrid[0][1] == 0:
            memo[(0,1)] = 1
        if obstacleGrid[1][0] == 0:
            memo[(1,0)] = 1

        def f(i,j):
            if i < 0 or j < 0:
                memo[(i,j)] = 0
            elif obstacleGrid[i][j] == 1:
                memo[(i,j)] = 0
            else:
                if (i,j) not in memo:
                    memo[(i,j)] = f(i-1,j) + f(i, j-1)

            return memo[(i,j)]

        out = f(m,n)
        print("memo: {}".format(memo))
        return out
