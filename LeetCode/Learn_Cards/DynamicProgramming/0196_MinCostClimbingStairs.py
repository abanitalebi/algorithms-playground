
"""
  Min Cost Climbing Stairs

You are given an integer array cost where cost[i] is the cost of ith step on a staircase. Once you pay the cost, you can either climb one or two steps.

You can either start from the step with index 0, or the step with index 1.

Return the minimum cost to reach the top of the floor.

https://leetcode.com/explore/learn/card/dynamic-programming/633/common-patterns-continued/4135/
"""

class Solution:
    def minCostClimbingStairs(self, cost: List[int]) -> int:
        if len(cost) == 0:
            return 0
        if len(cost) == 1:
            return cost[0]
        if len(cost) == 2:
            return min(cost[0], cost[1])
        if len(cost) == 3:
            return min(cost[0]+cost[2], cost[1], cost[1]+cost[2])

        memo = {0: 0, 1: cost[0], 2: min(cost[0], cost[1]), 3: min(cost[0]+cost[2], cost[1], cost[1]+cost[2])}
        def f(i):
            if i not in memo:
                memo[i] = min(f(i-1)+cost[i-1], f(i-2)+cost[i-2])
            return memo[i]

        print("memo: {}".format(memo))
        return f(len(cost))
