
"""
  Coin Change

You are given an integer array coins representing coins of different denominations and an integer amount representing a total amount of money.

Return the fewest number of coins that you need to make up that amount. If that amount of money cannot be made up by any combination of the coins, return -1.

You may assume that you have an infinite number of each kind of coin.

https://leetcode.com/explore/learn/card/dynamic-programming/632/common-patterns-in-dp-problems/4111/
"""

"""
    IDea: dp(amount) = min( dp(amount-coins[0], dp(amount-coins[1], ..., dp(amount-coins[n-1] ) + 1
        Therefore, the recursion includes a FOR loop.
"""


class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:

        if amount == 0:
            return 0
        min_coin = min(coins)
        if amount < min_coin:
            return -1

        memo = {}
        @lru_cache
        def f(amount):
            if amount < 0:
                memo[amount] = -1
            elif amount == 0:
                memo[amount] = 0
            elif amount in coins:
                memo[amount] = 1
            elif amount < min_coin:
                memo[amount] = -1
            else:
                next_amount = []
                for i in range(len(coins)):
                    na = f(amount - coins[i])
                    if na > 0:
                        next_amount.append(na)
                if len(next_amount) > 0:
                    min_amount = min(next_amount)
                    memo[amount] = min_amount + 1
                    if min_amount == -1:
                        memo[amount] = -1
                else:
                    memo[amount] = -1
            # print("amount: {}, coins: {}, memo: {}".format(amount, coins, memo))

            return memo[amount]

        # print("memo: {}".format(memo))
        return f(amount)




"""
public class Solution {

  public int coinChange(int[] coins, int amount) {
    if (amount < 1) return 0;
    return coinChange(coins, amount, new int[amount]);
  }

  private int coinChange(int[] coins, int rem, int[] count) {
    if (rem < 0) return -1;
    if (rem == 0) return 0;
    if (count[rem - 1] != 0) return count[rem - 1];
    int min = Integer.MAX_VALUE;
    for (int coin : coins) {
      int res = coinChange(coins, rem - coin, count);
      if (res >= 0 && res < min)
        min = 1 + res;
    }
    count[rem - 1] = (min == Integer.MAX_VALUE) ? -1 : min;
    return count[rem - 1];
  }
}"""