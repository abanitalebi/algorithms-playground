
"""
  Paint Fence

You are painting a fence of n posts with k different colors. You must paint the posts following these rules:

Every post must be painted exactly one color.
There cannot be three or more consecutive posts with the same color.
Given the two integers n and k, return the number of ways you can paint the fence.

https://leetcode.com/explore/learn/card/dynamic-programming/633/common-patterns-continued/4137/
"""


class Solution:
    def numWays(self, n: int, k: int) -> int:

        if n == 1:
            return k
        if n == 2:
            return k**2
        if n == 3:
            return k*k*k - k

        memo = {1: k, 2: k*k, 3: k*k*k-k}
        def f(i):
            if i not in memo:
                memo[i] = f(i-1)*k - f(i-3)*(k-1)

            return memo[i]

        return f(n)
