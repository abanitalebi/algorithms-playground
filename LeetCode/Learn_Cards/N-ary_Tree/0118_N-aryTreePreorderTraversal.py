
"""
  N-ary Tree Preorder Traversal

Given the root of an n-ary tree, return the preorder traversal of its nodes' values.

Nary-Tree input serialization is represented in their level order traversal. Each group of children is separated by the null value (See examples)

https://leetcode.com/explore/learn/card/n-ary-tree/130/traversal/925/
"""

"""
# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children
"""

class Solution:
    def preorder(self, root: 'Node') -> List[int]:

        if root is None:
            return []
        if root.children is None:
            return [root.val]

        pord = []
        for x in root.children:
            pord += self.preorder(x)

        return [root.val] + pord
