
"""
  Maximum Depth of N-ary Tree

Given a n-ary tree, find its maximum depth.

The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value (See examples).

https://leetcode.com/explore/learn/card/n-ary-tree/131/recursion/919/
"""

"""
# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children
"""

class Solution:
    def maxDepth(self, root: 'Node') -> int:

        if root is None:
            return 0
        if root.children is None:
            return 1

        depth_values = [self.maxDepth(x) for x in root.children if root.children is not None]

        if len(depth_values) == 0:
            return 1
        else:
            return 1 + max(depth_values)
