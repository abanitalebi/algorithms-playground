
"""
  Calculate Entropy
Given a group of values, the entropy of the group is defined as the formula as following:

where P(x) is the probability of appearance for the value x.

The exercise is to calculate the entropy of a group. Here is one example.

the input group:  [1, 1, 2, 2]

the probability of value 1 is  2/4 = 1/2
the probability of value 2 is  2/4 = 1/2

As a result, its entropy can be obtained by:  - (1/2) * log2(1/2) - (1/2) * log2(1/2) = 1/2 + 1/2 = 1

Note: the precision of result would remain within 1e-6.

https://leetcode.com/explore/learn/card/decision-tree/285/implementation/2650/
"""

class Solution:
    def calculateEntropy(self, input: List[int]) -> float:

        if len(input) == 0:
            return 0

        def get_probs(arr):
            probs = {}
            N = len(arr)
            for i in arr:
                if probs.get(i) is not None:
                    probs[i] += 1 / N
                else:
                    probs[i] = 1 / N

            # print("arr: {}, probs: {}".format(arr, probs))
            return probs

        en = 0
        ps = get_probs(input)
        for p in ps.values():
            en -= p * log2(p)
        return abs(en)
