
"""
  Linked List Cycle II

Given the head of a linked list, return the node where the cycle begins. If there is no cycle, return null.

There is a cycle in a linked list if there is some node in the list that can be reached again by continuously following the next pointer. Internally, pos is used to denote the index of the node that tail's next pointer is connected to (0-indexed). It is -1 if there is no cycle. Note that pos is not passed as a parameter.

Do not modify the linked list.

https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1214/
"""

"""
    Idea1: Find the meeting point using slow-fast pointers. Then start slow from head and continue fast from the meeting point.
    Idea2: Simpler but modifies the list: Add a temp node as next of every node that is traversed. If a new node viewed has a temp next it's a loop.

    https://www.geeksforgeeks.org/find-first-node-of-loop-in-a-linked-list/
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def detectCycle(self, head: ListNode) -> ListNode:
        # If list is empty or has only one node without loop
        if (head == None or head.next == None):
            return None

        slow = head
        fast = head

        # Move slow and fast 1 and 2 steps ahead respectively.
        slow = slow.next
        fast = fast.next.next

        # Search for loop using slow and fast pointers
        while (fast and fast.next):
            if (slow == fast):
                break
            slow = slow.next
            fast = fast.next.next

        # If loop does not exist
        if (slow != fast):
            return None

        # If loop exists. Start slow from head and fast from meeting point.
        slow = head

        while (slow != fast):
            slow = slow.next
            fast = fast.next

        return slow










class Solution2:
    def detectCycle(self, head: ListNode) -> ListNode:
        # Create a temporary node
        temp = Node(-1)

        while (head != None):

            # This condition is for the case
            # when there is no loop
            if (head.next == None):
                return None

            # Check if next is already
            # pointing to temp
            if (head.next == temp):
                break

            # Store the pointer to the next node
            # in order to get to it in the next step
            nex = head.next

            # Make next point to temp
            head.next = temp

            # Get to the next node in the list
            head = nex

        return head