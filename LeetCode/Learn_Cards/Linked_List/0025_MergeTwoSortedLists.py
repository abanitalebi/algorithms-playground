
"""
  Merge Two Sorted Lists

Merge two sorted linked lists and return it as a sorted list. The list should be made by splicing together the nodes of the first two lists.

https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1227/
"""

"""
    Idea: Use 2 pointers, one on each list. Start comparing and add the smaller of the two as a next node of a new node.
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def mergeTwoLists(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        if l1 is None:
            return l2
        if l2 is None:
            return l1

        head = ListNode()
        cur = head
        while (l1 is not None) and (l2 is not None):
            if l1.val >= l2.val:
                cur.next = l2
                l2 = l2.next
            else:
                cur.next = l1
                l1 = l1.next
            cur = cur.next

        if l1 is None:
            while l2 is not None:
                cur.next = l2
                cur = cur.next
                l2 = l2.next
        if l2 is None:
            while l1 is not None:
                cur.next = l1
                cur = cur.next
                l1 = l1.next

        return head.next

