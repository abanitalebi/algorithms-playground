
"""
  Rotate List

Given the head of a linked list, rotate the list to the right by k places.

https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1295/
"""

"""
    Idea: Find the (N-k)th node. Start traversing from there and proceed for N nodes.
"""



# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def pr(x, N):
    mylist = []
    for i in range(N):
        if x is None:
            mylist.append("None")
        else:
            mylist.append(x.val)
            x = x.next
    return mylist

class Solution:
    def rotateRight(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:

        if (head is None) or (head.next is None):
            return head

        cur = head
        N = 0
        while cur is not None:
            cur = cur.next
            N += 1

        k = k % N

        if (k == N) or (k == 0):
            return head

        start = head
        for i in range(N-k):
            start = start.next

        cur = start
        for i in range(N-1):
            if cur.next is None:
                cur.next = head
            cur = cur.next

        cur.next = None

        return start

