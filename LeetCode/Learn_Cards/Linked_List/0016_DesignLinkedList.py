
"""
  Design Linked List

Design your implementation of the linked list. You can choose to use a singly or doubly linked list.
A node in a singly linked list should have two attributes: val and next. val is the value of the current node, and next is a pointer/reference to the next node.
If you want to use the doubly linked list, you will need one more attribute prev to indicate the previous node in the linked list. Assume all nodes in the linked list are 0-indexed.

Implement the MyLinkedList class:

MyLinkedList() Initializes the MyLinkedList object.
int get(int index) Get the value of the indexth node in the linked list. If the index is invalid, return -1.
void addAtHead(int val) Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list.
void addAtTail(int val) Append a node of value val as the last element of the linked list.
void addAtIndex(int index, int val) Add a node of value val before the indexth node in the linked list. If index equals the length of the linked list, the node will be appended to the end of the linked list. If index is greater than the length, the node will not be inserted.
void deleteAtIndex(int index) Delete the indexth node in the linked list, if the index is valid.

https://leetcode.com/explore/learn/card/linked-list/209/singly-linked-list/1290/
"""


class Node:
    def __init__(self, val):
        self.val = val
        self.next = None


class MyLinkedList:
    def __init__(self):
        self.head = None

    def get(self, index):
        cur = self.head

        while index >= 0:
            if index == 0 and cur:
                return cur.val
            index -= 1
            if cur:  # if cur then go cur.next
                cur = cur.next
            else:  # cur is None then the index is invalid
                return -1

    def addAtHead(self, val):
        new_head = Node(val)
        new_head.next = self.head
        self.head = new_head
        # print(self.__repr__())

    def addAtTail(self, val):
        cur = self.head
        if cur:  # non-empty linked list
            while cur.next:
                cur = cur.next
            cur.next = Node(val)
        else:  # empty linked list
            self.head = Node(val)

    def addAtIndex(self, index, val):
        if index == 0:
            self.addAtHead(val)
            return

        cur = self.head
        prev = None
        while index and cur:
            prev = cur
            cur = cur.next
            index -= 1
        if prev:
            if cur:
                new = Node(val)
                new.next = cur
                prev.next = new
            else:
                prev.next = Node(val)

    def deleteAtIndex(self, index):
        # print(self.__repr__())

        if index == 0:
            self.head = self.head.next
        cur = self.head
        prev = None
        while index > 0 and cur:
            prev = cur
            cur = cur.next
            index -= 1
        if prev and cur:
            prev.next = cur.next
        # print(self.__repr__())

    def __repr__(self):
        s = ''
        cur = self.head
        while cur:
            s += str(cur.val) + ' -> '
            cur = cur.next
        return 'linked_list:  ' + str(s if self.head else 'None')



# Your MyLinkedList object will be instantiated and called as such:
# obj = MyLinkedList()
# param_1 = obj.get(index)
# obj.addAtHead(val)
# obj.addAtTail(val)
# obj.addAtIndex(index,val)
# obj.deleteAtIndex(index)