
"""
  Flatten a Multilevel Doubly Linked List

You are given a doubly linked list, which contains nodes that have a next pointer, a previous pointer, and an additional child pointer. This child pointer may or may not point to a separate doubly linked list, also containing these special nodes. These child lists may have one or more children of their own, and so on, to produce a multilevel data structure as shown in the example below.

Given the head of the first level of the list, flatten the list so that all the nodes appear in a single-level, doubly linked list. Let curr be a node with a child list. The nodes in the child list should appear after curr and before curr.next in the flattened list.

Return the head of the flattened list. The nodes in the list must have all of their child pointers set to null.

https://leetcode.com/explore/learn/card/linked-list/213/conclusion/1225/
"""

"""
    Idea: Use recursion.
"""


"""
# Definition for a Node.
class Node:
    def __init__(self, val, prev, next, child):
        self.val = val
        self.prev = prev
        self.next = next
        self.child = child
"""

class Solution:
    def flatten(self, head: 'Node') -> 'Node':
        if head is None:
            return None

        cur = head
        while cur is not None:
            orig_next = cur.next
            if cur.child is not None:
                nex = self.flatten(cur.child)
                while nex is not None:
                    cur.next = nex
                    nex.prev = cur
                    nex = nex.next
                    prev_n = cur.next
                    cur = cur.next
                cur = orig_next
                cur.prev = prev_n.next
                prev_n.next = cur
            else:
                cur = cur.next

        return head