
"""
  Remove Nth Node From End of List

Given the head of a linked list, remove the nth node from the end of the list and return its head.

https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1296/
"""

"""
    Idea: Two pointers, one is n ahead of the other. When the front one reaches the end, the behind one is what we want.
    Similar to: given a file, read the line n from the end of the file, with O(n).
"""

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def pr(n):
    mylist = []
    while n is not None:
        mylist.append(n.val)
        n = n.next
    print(mylist)

class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:

        if head.next is None:
            if n == 0:
                return head
            else:
                return

        pr(head)

        # Two pointers. One is ahead by n nodes.
        cur = head
        back = head
        for i in range(n):
            if cur.next is None:
                if i == n-1:
                    head = head.next
                    return head
                else:
                    return
            cur = cur.next

        pr(head)

        prev = back
        if cur is None:
            head = head.next
        else:
            while cur is not None:
                prev = back
                back = back.next
                cur = cur.next
            prev.next = back.next

        pr(head)

        return head
