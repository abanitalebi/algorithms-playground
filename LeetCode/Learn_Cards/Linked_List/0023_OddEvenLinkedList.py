
"""
  Odd Even Linked List

Given the head of a singly linked list, group all the nodes with odd indices together followed by the nodes with even indices, and return the reordered list.

The first node is considered odd, and the second node is even, and so on.

Note that the relative order inside both the even and odd groups should remain as it was in the input.

You must solve the problem in O(1) extra space complexity and O(n) time complexity.

https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1208/
"""

"""
    Idea: Traverse the list and start with two even/odd pointers, and connect even indices to even and odd indices to odd.
    At the end, connect the end of even list to the beginning of the odd list.
"""


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def pr(node):
    mylist = []
    while node is not None:
        mylist.append(node.val)
        node = node.next
    return mylist



class Solution:

    def oddEvenList(self, head: Optional[ListNode]) -> Optional[ListNode]:

        if head is None:
            return

        if head.next is None:
            return head

        cur = head
        nex = head.next

        even = cur
        odd = nex

        c = 2
        cur = nex.next
        while cur is not None:
            if c % 2 == 0:
                even.next = cur
                even = even.next
                if even.next is None:
                    odd.next = None
            else:
                odd.next = cur
                odd = odd.next
                if odd.next is None:
                    even.next = None

            cur = cur.next
            c += 1

        even.next = nex

        return head



