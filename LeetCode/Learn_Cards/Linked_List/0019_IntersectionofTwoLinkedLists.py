
"""
  Intersection of Two Linked Lists

Given the heads of two singly linked-lists headA and headB, return the node at which the two lists intersect. If the two linked lists have no intersection at all, return null.

For example, the following two linked lists begin to intersect at node c1:


The test cases are generated such that there are no cycles anywhere in the entire linked structure.

Note that the linked lists must retain their original structure after the function returns.
Follow up: Could you write a solution that runs in O(n) time and use only O(1) memory?

Note: The two linkedlists form a Y shape structure.

https://leetcode.com/explore/learn/card/linked-list/214/two-pointer-technique/1215/
"""

"""
    Idea 1: Traverse to find their lengths. Then traverse the longer one until A & remaining of B have a same length. Then check one-by-one in a loop. 
    Idea 2: Use a hash-set. Store nodes of the first one in the hash. Then check the nodes of the second if they are in the hash.
    Idea 3: Connect the end of A to its head to create a loop. Then find the loop from the view-point of B.
"""



# defining a node for LinkedList
class Node:
    def __init__(self,data):
        self.data=data
        self.next=None



def getIntersectionNode(head1,head2):

    #finding the total number of elements in head1 LinkedList
    c1=getCount(head1)

    #finding the total number of elements in head2 LinkedList
    c2=getCount(head2)

    #Traverse the bigger node by 'd' so that from that node onwards, both LinkedList
    #would be having same number of nodes and we can traverse them together.
    if c1 > c2:
        d=c1-c2
        return _getIntersectionNode(d,head1,head2)
    else:
        d=c2-c1
        return _getIntersectionNode(d,head2,head1)


def _getIntersectionNode(d,head1,head2):


    current1=head1
    current2=head2


    for i in range(d):
        if current1 is None:
            return -1
        current1=current1.next

    while current1 is not None and current2 is not None:

        # Instead of values, we need to check if there addresses are same
        # because there can be a case where value is same but that value is
        #not an intersecting point.
        if current1 is current2:
            return current1.data # or current2.data ( the value would be same)

        current1=current1.next
        current2=current2.next

    # Incase, we are not able to find our intersecting point.
    return -1

#Function to get the count of a LinkedList
def getCount(node):
    cur=node
    count=0
    while cur is not None:
        count+=1
        cur=cur.next
    return count


if __name__ == '__main__':
    # Creating two LinkedList
    # 1st one: 3->6->9->15->30
    # 2nd one: 10->15->30
    # We can see that 15 would be our intersection point

    # Defining the common node

    common=Node(15)

    #Defining first LinkedList

    head1=Node(3)
    head1.next=Node(6)
    head1.next.next=Node(9)
    head1.next.next.next=common
    head1.next.next.next.next=Node(30)

    # Defining second LinkedList

    head2=Node(10)
    head2.next=common
    head2.next.next=Node(30)

    print("The node of intersection is ",getIntersectionNode(head1,head2))



















# Python program to get intersection
# point of two linked list
class Node :
    def __init__(self, d):
        self.data = d
        self.next = None

# Function to print the list
def Print(n):
    cur = n
    while (cur != None) :
        print(cur.data, end=" ")
        cur = cur.next
    print("")

# Function to find the intersection of two node
def MegeNode(n1, n2):

    # Define hashset
    hs = set()

    while (n1 != None):
        hs.add(n1)
        n1 = n1.next
    while (n2 != None):
        if (n2 in hs):
            return n2
        n2 = n2.next

    return None


# Driver code

# list 1
n1 = Node(1)
n1.next = Node(2)
n1.next.next = Node(3)
n1.next.next.next = Node(4)
n1.next.next.next.next = Node(5)
n1.next.next.next.next.next = Node(6)
n1.next.next.next.next.next.next = Node(7)

# list 2
n2 = Node(10)
n2.next = Node(9)
n2.next.next = Node(8)
n2.next.next.next = n1.next.next.next

Print(n1)
Print(n2)

print(MegeNode(n1, n2).data)

