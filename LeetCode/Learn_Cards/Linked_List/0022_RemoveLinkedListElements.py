
"""
  Remove Linked List Elements

Given the head of a linked list and an integer val, remove all the nodes of the linked list that has Node.val == val, and return the new head.

https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1207/
"""

"""
    Idea: Start traversing and every time you see val remove it (prev.next = cur.next). 
    Use recursion to ensure head is not val: if head.val == val: return self.removeElements(head.next, val)
"""



# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeElements(self, head: Optional[ListNode], val: int) -> Optional[ListNode]:
        if head is None:
            return
        if head.next is None:
            if head.val == val:
                return
            else:
                return head

        if head.val == val:
            return self.removeElements(head.next, val)

        prev = head
        cur = head.next

        while cur is not None:
            if cur.val == val:
                prev.next = cur.next
                cur = prev.next
            else:
                prev = prev.next
                cur = prev.next

        return head
