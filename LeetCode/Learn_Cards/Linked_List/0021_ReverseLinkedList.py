
"""
  Reverse Linked List

Given the head of a singly linked list, reverse the list, and return the reversed list.

Follow up: A linked list can be reversed either iteratively or recursively. Could you implement both?

https://leetcode.com/explore/learn/card/linked-list/219/classic-problems/1205/
"""

"""
    Idea: One solution is to iterate the nodes in original order and move them to the head of the list one by one.
    Another solution is by recursion: 
       1) Divide the list in two parts - first node and rest of the linked list.
       2) Call reverse for the rest of the linked list.
       3) Link rest to first.
       4) Fix head pointer
"""


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def pr(head):
    mylist = []
    while head is not None:
        mylist.append(head.val)
        head = head.next
    print(mylist)


class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:

        if head is None or head.next is None:
            return head

        cur = head
        prev = cur
        cur = cur.next
        # pr(head)
        while prev.next is not None:
            nex = cur.next

            # Remove current
            prev.next = nex

            # Put it before head
            cur.next = head
            head = cur

            # Iterate
            cur = nex

            # pr(head)

        return head

