
"""
  Array Partition I

Given an integer array nums of 2n integers, group these integers into n pairs (a1, b1), (a2, b2), ..., (an, bn) such that the sum of min(ai, bi) for all i is maximized. Return the maximized sum.

https://leetcode.com/explore/learn/card/array-and-string/205/array-two-pointer-technique/1154/
"""

class Solution:
    def arrayPairSum(self, nums: List[int]) -> int:
        N = len(nums) // 2
        arr = sorted(nums)
        out = 0
        for i in range(N):
            out += min(arr[2*i], arr[2*i+1])
        return out
