
"""
Implement strStr().

Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

Clarification:

What should we return when needle is an empty string? This is a great question to ask during an interview.

For the purpose of this problem, we will return 0 when needle is an empty string. This is consistent to C's strstr() and Java's indexOf().

https://leetcode.com/explore/learn/card/array-and-string/203/introduction-to-string/1161/
"""

"""
    Idea: Start matching after the first letter is found. To speed up, you can start matching after both first and last letters are found.
"""


class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        if len(needle) == 0:
            return 0
        if len(needle) > len(haystack):
            return -1

        N = len(haystack)
        M = len(needle)
        for i in range(N-M+1):
            if haystack[i] == needle[0] and haystack[i+M-1] == needle[M-1]:
                # check if needle exists starting from here
                count = 0
                for j in range(len(needle)):
                    if i+j >= N:
                        break
                    if haystack[i+j] == needle[j]:
                        count += 1
                    else:
                        break
                if count == len(needle):
                    return i
        return -1
