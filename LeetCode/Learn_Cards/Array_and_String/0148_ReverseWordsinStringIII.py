
"""
  Reverse Words in a String III

Given a string s, reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.

https://leetcode.com/explore/learn/card/array-and-string/204/conclusion/1165/
"""

class Solution:
    def reverseWords(self, s: str) -> str:
        words = [i[::-1] for i in s.split(" ")]
        return " ".join(words)
