
"""
  Find Pivot Index

Given an array of integers nums, calculate the pivot index of this array.

The pivot index is the index where the sum of all the numbers strictly to the left of the index is equal to the sum of all the numbers strictly to the index's right.

If the index is on the left edge of the array, then the left sum is 0 because there are no elements to the left. This also applies to the right edge of the array.

Return the leftmost pivot index. If no such index exists, return -1.

https://leetcode.com/explore/learn/card/array-and-string/201/introduction-to-array/1144/
"""

class Solution:
    def pivotIndex(self, nums: List[int]) -> int:
        N = len(nums)
        left = [0] * N
        s = 0
        for i in range(1, N):
            s += nums[i-1]
            left[i] = s

        sum_all = left[-1] + nums[-1]
        for i in range(N):
            l = left[i]
            r = sum_all - nums[i] - l
            if l == r:
                return i

        return -1
