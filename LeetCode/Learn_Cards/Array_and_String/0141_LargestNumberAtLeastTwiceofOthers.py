
"""
  Largest Number At Least Twice of Others

You are given an integer array nums where the largest integer is unique.

Determine whether the largest element in the array is at least twice as much as every other number in the array. If it is, return the index of the largest element, or return -1 otherwise.

https://leetcode.com/explore/learn/card/array-and-string/201/introduction-to-array/1147/
"""

class Solution:
    def dominantIndex(self, nums: List[int]) -> int:
        m = max(nums)
        for i in range(len(nums)):
            if nums[i] == m:
                idx = i
            elif nums[i] * 2 > m:
                return -1
        return idx

