
"""
  Longest Common Prefix

Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

https://leetcode.com/explore/learn/card/array-and-string/203/introduction-to-string/1162/
"""

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:

        if len(strs) == 1:
            return strs[0]

        out = ""
        N = len(strs)
        for i in range(sys.maxsize):
            if i >= len(strs[0]):
                break
            letter = strs[0][i]
            count = 0
            for st in strs:
                if i >= len(st):
                    break
                # print("st: {}, letter: {}, i: {}".format(st, letter, i))
                if st[i] == letter:
                    count += 1
                else:
                    break
            if count == N:
                out += letter
            else:
                break

        return out
