
"""
  Rotate Array

Given an array, rotate the array to the right by k steps, where k is non-negative.

https://leetcode.com/explore/learn/card/array-and-string/204/conclusion/1182/
"""

class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        n = len(nums)
        k %= n

        start = count = 0
        while count < n:
            current, prev = start, nums[start]
            while True:
                next_idx = (current + k) % n
                nums[next_idx], prev = prev, nums[next_idx]
                current = next_idx
                count += 1

                if start == current:
                    break
            start += 1
