
"""
  Evaluate Reverse Polish Notation

Evaluate the value of an arithmetic expression in Reverse Polish Notation.

Valid operators are +, -, *, and /. Each operand may be an integer or another expression.

Note that division between two integers should truncate toward zero.

It is guaranteed that the given RPN expression is always valid. That means the expression would always evaluate to a result, and there will not be any division by zero operation.

https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1394/
"""

"""
    Idea: Every time you see an operation, apply the last two values with it, and pop the from the stack.
"""

class Solution:
    def evalRPN(self, tokens: List[str]) -> int:

        if len(tokens) == 0:
            return 0
        if len(tokens) == 1:
            return tokens[0]

        ops = ['+', '-', '*', '/']
        i = 2
        stack = [tokens[0], tokens[1]]
        while (i < len(tokens)) and (len(stack) > 0):
            new_item = tokens[i]
            # print("stack: {}, new_item: {}".format(stack, new_item))
            if (new_item in ops) and (len(stack) > 1):
                y = int(stack.pop())
                x = int(stack.pop())
                if new_item == '+':
                    z = x + y
                elif new_item == '-':
                    z = x - y
                elif new_item == '*':
                    z = x * y
                elif new_item == '/':
                    z = int(x / y)
                stack.append(z)
                print(x,y,z)
            else:
                stack.append(new_item)
            i += 1

        return stack[0]


