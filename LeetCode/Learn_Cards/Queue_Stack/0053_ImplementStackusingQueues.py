
"""
  Implement Stack using Queues

Implement a last-in-first-out (LIFO) stack using only two queues. The implemented stack should support all the functions of a normal stack (push, top, pop, and empty).

Implement the MyStack class:

void push(int x) Pushes element x to the top of the stack.
int pop() Removes the element on the top of the stack and returns it.
int top() Returns the element on the top of the stack.
boolean empty() Returns true if the stack is empty, false otherwise.
Notes:

You must use only standard operations of a queue, which means that only push to back, peek/pop from front, size and is empty operations are valid.
Depending on your language, the queue may not be supported natively. You may simulate a queue using a list or deque (double-ended queue) as long as you use only a queue's standard operations.

https://leetcode.com/explore/learn/card/queue-stack/239/conclusion/1387/
"""


"""
    Idea: Can use two queues or only one queue. Push: just append. Pop: move from q1 to q2 (or circularly if one q).
"""


class MyStack:

    def __init__(self):
        self.q1 = []
        self.q2 = []
        self.empty_ = True
        self.top_ = None


    def push(self, x: int) -> None:
        self.q1.append(x)
        self.empty_ = False
        self.top_ = x


    def pop(self) -> int:
        if len(self.q1) == 0:
            return None
        if len(self.q1) == 1:
            self.empty_ = True
            self.top_ = None
            return self.q1.pop(0)
        if len(self.q1) == 2:
            self.empty_ = False
            self.top_ = self.q1.pop(0)
            x = self.q1.pop(0)
            self.q1 = [self.top_]
            return x
        while len(self.q1) > 2:
            self.q2.append(self.q1.pop(0))
        self.empty_ = False
        self.top_ = self.q1.pop(0)
        x = self.q1.pop(0)
        self.q2.append(self.top_)

        self.q1 = self.q2
        self.q2 = []

        return x


    def top(self) -> int:
        return self.top_


    def empty(self) -> bool:
        return self.empty_



# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()

