
"""
  Valid Parentheses

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.

https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1361/
"""


"""
    Idea: What if whenever we encounter a matching pair of parenthesis in the expression, 
    we simply remove it from the expression? This would keep on shortening the expression. 
    So when we see [] or {} or (), we just remove them, the remaining should still be valid. We can do that with stack.
"""


class Solution:
    def isValid(self, s: str) -> bool:

        if len(s) == 0:
            return True

        mapping = {'(': ')', '[': ']', '{': '}'}

        slist = list(s)
        st = [slist[0]]
        i = 0
        while i < len(slist)-1:
            i += 1
            if (len(st) > 0) and (slist[i] == mapping.get(st[-1])):
                st.pop()
            else:
                st.append(slist[i])

        return len(st) == 0
