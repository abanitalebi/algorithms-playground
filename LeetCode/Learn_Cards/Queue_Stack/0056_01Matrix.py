
"""
  01 Matrix

Given an m x n binary matrix mat, return the distance of the nearest 0 for each cell.

The distance between two adjacent cells is 1.

https://leetcode.com/explore/learn/card/queue-stack/239/conclusion/1388/
"""

"""
    Idea: Use BFS at every coordinate.
"""


def updateMatrix(self, matrix: List[List[int]]) -> List[List[int]]:
    q = collections.deque()
    row = len(matrix)
    col = len(matrix[0])
    dirs = [(-1, 0),(0, -1),(1,0),(0,1)]
    for x in range(row):
        for y in range(col):
            if matrix[x][y] == 0:
                q.append((x,y))
            else:
                matrix[x][y] = float("inf")
    while q:
        x,y = q.popleft()
        for dx, dy in dirs:
            new_x, new_y = x+dx, y+dy
            if 0 <= new_x < row and 0 <= new_y < col and matrix[new_x][new_y] > matrix[x][y]+1:
                q.append((new_x,new_y))
                matrix[new_x][new_y] = matrix[x][y]+1
    return matrix