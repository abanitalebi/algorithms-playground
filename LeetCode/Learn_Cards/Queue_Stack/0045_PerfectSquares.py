
"""
  Perfect Squares

Given an integer n, return the least number of perfect square numbers that sum to n.

A perfect square is an integer that is the square of an integer; in other words, it is the product of some integer with itself. For example, 1, 4, 9, and 16 are perfect squares while 3 and 11 are not.

https://leetcode.com/explore/learn/card/queue-stack/231/practical-application-queue/1371/
"""

"""
    Idea: Use BFS. Start with root=collections.deque([n]). Start deducting 1^2, 2^2, ..., k^2 until difference > 0;
        Number of levels = shortest path = least number of perfect squares. 
"""



class Solution:
    def numSquares(self, n: int) -> int:

        if (n == 1) or (n == 0):
            return 1

        count = 0
        q = collections.deque([n])
        while q:
            count += 1
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()
                if cur == 0:
                    return count
                else:
                    k = 1
                    while (cur - k*k >= 0):
                        if cur - k*k == 0:
                            return count
                        q.append(cur - k*k)
                        k += 1
        return -1

