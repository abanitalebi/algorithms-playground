
"""
  Number of Islands

Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return the number of islands.

An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.

https://leetcode.com/explore/learn/card/queue-stack/231/practical-application-queue/1374/
"""


"""
    Idea: Number of Islands (Connected Components). 
        BFS/DFS per each node (cell) and keep updating a visited mask. 4 or 8 neighborhood.
"""



class Solution:

    def apply_bfs(self, grid, visited, m, n, i, j):

        rows = [-1,1,0,0]
        cols = [0,0,-1,1]

        root = [i,j]
        q = [root]    # q = collections.deque([root])
        while q:
            q_size = len(q)
            for idx in range(q_size):
                removed = q.pop(0)    # q.popleft()
                iq = removed[0]
                jq = removed[1]
                for h,w in zip(rows,cols):
                    if (iq+h >= 0) and (iq+h < m) and (jq+w >= 0) and (jq+w < n):
                        if (grid[iq+h][jq+w] == "1") and (visited[iq+h][jq+w] == 0):
                            q.append([iq+h,jq+w])
                            visited[iq+h][jq+w] = 1


    def numIslands(self, grid: List[List[str]]) -> int:

        if len(grid) == 0:
            return 0
        if len(grid[0]) == 0:
            return 0

        visited = []
        m = 0
        for i in range(len(grid)):
            row = []
            n = 0
            for j in range(len(grid[i])):
                row.append(0)
                n += 1
            visited.append(row)
            m += 1

        num_islands = 0
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                if (visited[i][j] == 0) and (grid[i][j] == "1"):
                    self.apply_bfs(grid, visited, m, n, i, j)
                    num_islands += 1

        return num_islands



