
"""
  Decode String

Given an encoded string, return its decoded string.

The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times. Note that k is guaranteed to be a positive integer.

You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.

Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, k. For example, there won't be input like 3a or 2[4].

https://leetcode.com/explore/learn/card/queue-stack/239/conclusion/1379/
"""

"""
    Idea: Use a stack. push until you see ]. Then pop until seeing [ to gather the string that is repeated. 
        Keep poping to find the k times to repeat. Append to stack and continue.
"""


class Solution:
    def decodeString(self, s: str) -> str:

        inp = list(s)
        stack = []
        i = 0
        while (i < len(inp)):
            cur = inp[i]
            if cur == "]":
                x = []
                while cur != "[":
                    cur = stack.pop()
                    x.append(cur)
                x.pop()
                x = x[::-1]
                cont = True
                k = []
                while stack:
                    if stack[-1] not in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
                        break
                    k.append(stack.pop())
                k = k[::-1]
                k = int(''.join(k))
                y = x * k
                stack += y
            else:
                stack.append(cur)
            i += 1

        out = ''.join(stack)
        return out


