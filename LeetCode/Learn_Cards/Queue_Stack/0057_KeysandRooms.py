
"""
  Keys and Rooms

There are n rooms labeled from 0 to n - 1 and all the rooms are locked except for room 0. Your goal is to visit all the rooms. However, you cannot enter a locked room without having its key.

When you visit a room, you may find a set of distinct keys in it. Each key has a number on it, denoting which room it unlocks, and you can take all of them with you to unlock the other rooms.

Given an array rooms where rooms[i] is the set of keys that you can obtain if you visited room i, return true if you can visit all the rooms, or false otherwise.

https://leetcode.com/explore/learn/card/queue-stack/239/conclusion/1391/
"""

"""
    Idea: Use BFS. Add 0 to the queue, then add un-visited neighbors on each level traversal.
"""


class Solution:
    def canVisitAllRooms(self, rooms: List[List[int]]) -> bool:

        # Gt number of unique elements
        all_rooms = []
        for r in rooms:
            all_rooms += r
        all_rooms = list(set(all_rooms))
        n = len(all_rooms)

        visited = [0] * n
        q = collections.deque([0])
        while q:
            q_size = len(q)
            for i in range(q_size):
                cur = q.popleft()
                options = rooms[cur]
                for j in options:
                    if visited[j-1] != 1:
                        q.append(j)
                        visited[j-1] = 1
        s = sum(visited)
        return s == n
