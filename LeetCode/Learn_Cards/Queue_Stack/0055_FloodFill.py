
"""
  Flood Fill

An image is represented by an m x n integer grid image where image[i][j] represents the pixel value of the image.

You are also given three integers sr, sc, and newColor. You should perform a flood fill on the image starting from the pixel image[sr][sc].

To perform a flood fill, consider the starting pixel, plus any pixels connected 4-directionally to the starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels (also with the same color), and so on. Replace the color of all of the aforementioned pixels with newColor.

Return the modified image after performing the flood fill.

https://leetcode.com/explore/learn/card/queue-stack/239/conclusion/1393/
"""

"""
    Idea: Use BFS. Similar to counting islands but just one BFS on the given point. 
"""


class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, newColor: int) -> List[List[int]]:

        def BFS(arr, row, col, color, visited):
            h = [-1, 1, 0, 0]
            w = [0, 0, -1, 1]
            q = [(row, col)]
            val = arr[row][col]
            # print("arr: {}".format(arr))
            while q:
                # print("----------------------------------")
                # print("q: {}, visited: {}".format(q, visited))
                q_size = len(q)
                for i in range(q_size):
                    cur = q.pop(0)
                    # print("cur: {}".format(cur))
                    row = cur[0]
                    col = cur[1]
                    arr[row][col] = color
                    visited[row][col] = 1
                    for (r,c) in zip(h,w):
                        if (row+r >= 0) and (row+r < len(arr)) and (col+c >=0) and (col+c < len(arr[0])):
                            # print("this one", visited[row+r][col+c], row+r, col+c)
                            if (arr[row+r][col+c] == val) and (visited[row+r][col+c] != 1):
                                q.append((row+r, col+c))
                                # print("Appended: {}".format((row+r, col+c)))
            return arr

        # visited = [[[0] * len(image[0])] * len(image)]    # Wrong! It copies inner lists by reference!!
        visited = []
        for i in range(len(image)):
            x = []
            for j in range(len(image[0])):
                x.append(0)
            visited.append(x)

        out = BFS(image, sr, sc, newColor, visited)

        # out = image
        # for i in range(len(image)):
        #     for j in range(len(image[0])):
        #         if visited[i][j] == 1:
        #             out[i][j] = newColor

        return out

