
"""
  Design Circular Queue

Design your implementation of the circular queue. The circular queue is a linear data structure in which the operations are performed based on FIFO (First In First Out) principle and the last position is connected back to the first position to make a circle. It is also called "Ring Buffer".

One of the benefits of the circular queue is that we can make use of the spaces in front of the queue. In a normal queue, once the queue becomes full, we cannot insert the next element even if there is a space in front of the queue. But using the circular queue, we can use the space to store new values.

Implementation the MyCircularQueue class:

MyCircularQueue(k) Initializes the object with the size of the queue to be k.
int Front() Gets the front item from the queue. If the queue is empty, return -1.
int Rear() Gets the last item from the queue. If the queue is empty, return -1.
boolean enQueue(int value) Inserts an element into the circular queue. Return true if the operation is successful.
boolean deQueue() Deletes an element from the circular queue. Return true if the operation is successful.
boolean isEmpty() Checks whether the circular queue is empty or not.
boolean isFull() Checks whether the circular queue is full or not.
You must solve the problem without using the built-in queue data structure in your programming language.

https://leetcode.com/explore/learn/card/queue-stack/228/first-in-first-out-data-structure/1337/
"""


"""
    Idea: Circular queue (ring buffer) is used instead of regular queue, to efficiently use the space.
        It has two pointers, head & tail. Starts enqueue from right, and dequeue from left (FIFO).
"""



class MyCircularQueue:

    def __init__(self, k: int):
        self.max_len = k
        self.head = -1
        self.tail = -1
        self.q = [0 for i in range(k)]
        self.isEmpty_ = True
        self.isFull_ = False


    def enQueue(self, value: int) -> bool:
        new_tail = (self.tail + 1) % self.max_len
        if new_tail != self.head:
            self.tail = new_tail
            self.q[self.tail] = value
            self.isEmpty_ = False
            if (self.tail + 1) % self.max_len == self.head:
                self.isFull_ = True
            if self.head == -1:
                self.head = 0
            # print(self.q, self.head, self.tail)
            return True
        else:
            self.isFull_ = True
            self.isEmpty_ = False
            # print(self.q, self.head, self.tail)
            return False


    def deQueue(self) -> bool:
        new_head = (self.head + 1) % self.max_len
        if self.head >= 0:
            if self.head == self.tail:
                self.head = -1
                self.tail = -1
                self.isEmpty_ = True
                self.isFull_ = False
                # print(self.q, self.head, self.tail)
                return True
            else:
                self.isEmpty_ = False
                self.isFull_ = False
                self.head = new_head
                return True
        else:
            return False


    def Front(self) -> int:
        # print(self.q[self.head], self.q, self.head, self.tail)
        if self.isEmpty_:
            return -1
        else:
            return self.q[self.head]


    def Rear(self) -> int:
        # print(self.q[self.tail], self.q, self.head, self.tail)
        if self.isEmpty_:
            return -1
        else:
            return self.q[self.tail]


    def isEmpty(self) -> bool:
        return self.isEmpty_


    def isFull(self) -> bool:
        return self.isFull_



# Your MyCircularQueue object will be instantiated and called as such:
# obj = MyCircularQueue(k)
# param_1 = obj.enQueue(value)
# param_2 = obj.deQueue()
# param_3 = obj.Front()
# param_4 = obj.Rear()
# param_5 = obj.isEmpty()
# param_6 = obj.isFull()

