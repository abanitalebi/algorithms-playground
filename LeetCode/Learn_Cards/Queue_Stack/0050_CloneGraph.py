
"""
  Clone Graph

Given a reference of a node in a connected undirected graph.

Return a deep copy (clone) of the graph.

Each node in the graph contains a value (int) and a list (List[Node]) of its neighbors.

class Node {
    public int val;
    public List<Node> neighbors;
}

https://leetcode.com/explore/learn/card/queue-stack/232/practical-application-stack/1392/
"""

"""
    Idea: Use recursion + hash map (map each old node to its new corresponding node). If already visited,
        append to neighbors the corresponding old node, otherwise append a new cope (recursed) of the neighbors.
        
    Another idea: like divide and conquer. If is was a tree, we could just use recursion over the children of the root.
        Graph is similar, except we have to check if a node is already visited (i.e. created) or not.
"""




"""
# Definition for a Node.
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []
"""

class Solution:
    def cloneGraph(self, node: 'Node') -> 'Node':

        if node is None:
            return None

        hmap = {}

        def copy_node(cur, visited):
            # print(cur.val)
            cur2 = Node(cur.val, [])
            visited.append(cur)
            hmap[cur] = cur2
            if len(cur.neighbors) == 0:
                return cur2
            for i in range(len(cur.neighbors)):
                if cur.neighbors[i] in visited:
                    cur2.neighbors.append(hmap[cur.neighbors[i]])
                else:
                    visited.append(cur.neighbors[i])
                    cur2.neighbors.append(copy_node(cur.neighbors[i], visited))
            return cur2

        v = []
        node2 = copy_node(node, v)

        return node2

