
"""
  Implement Queue using Stacks

Implement a first in first out (FIFO) queue using only two stacks. The implemented queue should support all the functions of a normal queue (push, peek, pop, and empty).

Implement the MyQueue class:

void push(int x) Pushes element x to the back of the queue.
int pop() Removes the element from the front of the queue and returns it.
int peek() Returns the element at the front of the queue.
boolean empty() Returns true if the queue is empty, false otherwise.
Notes:

You must use only standard operations of a stack, which means only push to top, peek/pop from top, size, and is empty operations are valid.
Depending on your language, the stack may not be supported natively. You may simulate a stack using a list or deque (double-ended queue) as long as you use only a stack's standard operations.

https://leetcode.com/explore/learn/card/queue-stack/239/conclusion/1386/
"""

"""
    Idea: Each push is O(1) to S1. For pop(), transfer all elements one-by-one to S2 with O(n), then pop from it.
        So each operation is amortized O(1). 
"""




class MyQueue:

    def __init__(self):
        self.s1 = []
        self.s2 = []

    def push(self, x: int) -> None:
        if self.s2:
            while self.s2:
                y = self.s2.pop()
                self.s1.append(y)
        self.s1.append(x)
        # print(self.s1, self.s2)
        return True

    def pop(self) -> int:
        if self.s1:
            while self.s1:
                x = self.s1.pop()
                self.s2.append(x)
        y = self.s2.pop()
        # print(self.s1, self.s2)
        return y

    def peek(self) -> int:
        if self.s1:
            while self.s1:
                x = self.s1.pop()
                self.s2.append(x)
        # print(self.s1, self.s2)
        return self.s2[-1]

    def empty(self) -> bool:
        if len(self.s1) == 0 and len(self.s2) == 0:
            return True
        else:
            return False


# Your MyQueue object will be instantiated and called as such:
# obj = MyQueue()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.peek()
# param_4 = obj.empty()