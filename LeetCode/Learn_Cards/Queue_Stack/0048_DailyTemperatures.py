
"""
  Daily Temperatures

Given an array of integers temperatures represents the daily temperatures, return an array answer such that answer[i] is the number of days you have to wait after the ith day to get a warmer temperature. If there is no future day for which this is possible, keep answer[i] == 0 instead.

https://leetcode.com/explore/learn/card/queue-stack/230/usage-stack/1363/
"""

"""
    Idea: Build a stack with elements of form (i, tem[i]). Each iteration compare a new element of the array with the 
        top of the stack. If higher, pop from the stack and report index difference, if lower add to stack.
"""


class Solution:
    def dailyTemperatures(self, temperatures: List[int]) -> List[int]:

        if len(temperatures) == 0:
            return []

        i = 0
        stack = [(0, temperatures[0])]
        out = [0] * len(temperatures)
        while (len(stack) > 0) and (i < len(temperatures)-1):
            # print("temperatures: {},  stack: {}".format(temperatures, stack))
            i += 1
            new_item = (i, temperatures[i])
            while (len(stack) > 0) and (new_item[1] > stack[-1][1]):
                out[stack[-1][0]] = new_item[0] - stack[-1][0]
                stack.pop()
            if (len(stack) > 0) and (new_item[1] <= stack[-1][1]):
                stack.append(new_item)
            if (len(stack) == 0) and (i < len(temperatures)-1):
                stack.append(new_item)

        return out
