
# Python Syntax
x = [1,2,3,4,5] <br>
a = x.pop() <br>
x = [1,2,3,4], a = 5 <br>
x.insert(0, 10) <br>
x = [10,1,2,3,4] <br>
x = [1,2,3,4,3] <br>
x.remove(3) <br>
x = [1,2,4,3] <br> <br>

x = [(1,10), (0,20)]
sorted(x) -> [(0,20), (1,10)]
max(x) -> (1,10)

---------------------

2 << 1 <br>
4 <br>
2 << 2 <br>
8 <br>
7 >> 2 <br>
1 <br> <br>
---------------------

x = [1,2,4,3] <br>
sorted(x) -> [1,2,3,4] <br>
x.sort() <br>
x now is [1,2,3,4], changed in-place <br>
---------------------

Strings:
'a' + 'b' -> 'ab'. <br>
.islower(), .isupper(), .lower(), .upper()

---------------------
import heapq  <br>
outlist = heapq.nlargest(n, mylist)  <br>
heapq.heapify(mylist)  <br>
m = heapq.heappop(mylist)  <br>
heapq.heappush(heap, item)  <br>
---------------------

How to sort tuples based on one of their entries: Use heap:
Also an implementation of a priority queue with minHeap:
```
class Edge:
    def __init__(self, point1, point2, cost):
        self.point1 = point1
        self.point2 = point2
        self.cost = cost
    
    def __lt__(self, other):
        return self.cost < other.cost

pq = []
for i in range(size):
    x1, y1 = points[i]
    for j in range(i + 1, size):
        x2, y2 = points[j]
        # Calculate the distance between two coordinates.
        cost = abs(x1 - x2) + abs(y1 - y2)
        edge = Edge(i, j, cost)
        pq.append(edge)

heapq.heapify(pq)
```



---------------------

visited = [[0] * 3] * 4 <br>
This is dangerous, as it copies the inner list by reference, so assignment will be wrong. <br>



---------------------


Find path from root of a binary tree to a given node (unique values) (DFS/BFS): <br>
Give path as input to the recursive function and modify it within the function (instead of being an output) <br>
```
def find_path(cur, path, node):

    if cur is None:
        return False

    path.append(cur)

    if cur is node:
        return True
    if cur.left is not None:
        if find_path(cur.left, path, node):
            return True
    if cur.right is not None:
        if find_path(cur.right, path, node):
            return True

    path.pop()
    return False
    # Also possible with BFS. Collect paths in the queue, instead of nodes.
```

All Paths From Source to Target in a graph/tree: we can do BFS or DFS. With BFS: Collect paths in the queue, instead of nodes.
See learn_card -> Graph -> 0066, 0068. <br> 
```
def allPathsSourceTarget(graph: List[List[int]]) -> List[List[int]]:
    if not graph or len(graph) == 0:
        return []

    def find_path(node, N):
        path.append(node)
        if node == N-1:
            paths.append(path.copy())
            return

        for neighbor in graph[node]:
            find_path(neighbor, N)
            path.pop()

    paths = []
    path = []
    N = len(graph)

    find_path(0, N)

    return paths
```
```
def allPathsSourceTarget(self, graph: List[List[int]]) -> List[List[int]]:
    if not graph or len(graph) == 0:
        return []

    paths = []
    N = len(graph)

    q = collections.deque([[0]])
    while q:
        q_size = len(q)
        for i in range(q_size):
            cur_path = q.popleft()
            cur_node = cur_path[-1]

            for neighbor in graph[cur_node]:
                if neighbor == N-1:
                    paths.append(cur_path + [neighbor])
                else:
                    q.append(cur_path + [neighbor])
    return paths
```



Shortest path (between two nodes) in an unweighted graph: <br>
https://leetcode.com/explore/learn/card/queue-stack/231/practical-application-queue/1376/ <br>
Use a queue and perform a BFS traversal. The first time you visit the target node gives you the shortest path. <br>
Path: You can collect the parent nodes in a dictionary, or as an attribute of the node class. <br>
Hash set: In a graph (unlike tree) you may have loops. If you need to avoid multiple node visits, create a hash set, and store previously visited nodes in it. In the BFS traversal, you can do if node.neighbor is not None and not in hash set then ... <br>
Therefore, one dictionary can be used both for carrying parent nodes and checking for loops. <br>

Shortest Path Faster Algorithm (improved Bellman-Ford):
For weighted graphs (even with negative edges but no negative cycle).
Compute the shortest path to all nodes: start with a node and use a queue. Initialize path_costs with all sys.maxsize.
Then pop from the queue. For the neighbors of the current node, compute the path to the root, and update their path_costs
if it's less than the current stored value. If updated, add the neighbor to the queue if not already there.
See 0071,0072 under Graph learn card. <br>



---------------------

Find depth/height of a binary tree: <br>
```
def maxDepth(self, root: Optional[TreeNode]) -> int:
    if root is None:
        return 0

    left = self.maxDepth(root.left)
    right = self.maxDepth(root.right)

    h = 1 + max(left, right)

    return h
```

---------------------

BFS in a binary tree, tree, or graph: <br>
Use a queue & FOR (for each row/level) inside a While: <br>
(If you change the queue to a stack it becomes a DFS) <br>
(Sometimes you also carry a 'visited' array) <br>
```
    q = [root]    # or: q = collections.deque([root])
    while q:
        q_size = len(q)
        for i in range(q_size):
            removed = q.pop(0)    # or: q.popleft()
            if removed.left is not None:
                q.append(removed.left)
            if removed.right is not None:
                q.append(removed.right)
            
            % Do something here, e.g. connect nodes horizontally to next nodes
            if i < q_size-1:
                removed.next = q[0]
```
---------------------

Memoization: is an optimization technique used primarily to speed up computer programs by storing the results of expensive function calls and returning the cached result when the same inputs occur again.
```
def fib(self, N):
    """
    :type N: int
    :rtype: int
    """
    cache = {}
    def recur_fib(N):
        if N in cache:
            return cache[N]

        if N < 2:
            result = N
        else:
            result = recur_fib(N-1) + recur_fib(N-2)

        # put result in cache for later reference.
        cache[N] = result
        return result

    return recur_fib(N)
```

---------------------
Template for divide and conquer (e.g. merge sort, Validate Binary Search Tree):
```
def divide_and_conquer( S ):
    # (1). Divide the problem into a set of subproblems.
    [S1, S2, ... Sn] = divide(S)

    # (2). Solve the subproblem recursively,
    #   obtain the results of subproblems as [R1, R2... Rn].
    rets = [divide_and_conquer(Si) for Si in [S1, S2, ... Sn]]
    [R1, R2,... Rn] = rets

    # (3). combine the results from the subproblems.
    #   and return the combined result.
    return combine([R1, R2,... Rn])
```
---------------------

BST:
A binary tree in which a node's value is greater than all its left children, and less than all its right children.
To find a predecessor, go to the left once and then as many times to the right as you could.
To find a successor, go to the right once and then as many times to the left as you could.
```
def successor(root):
    root = root.right
    while root.left:
        root = root.left
    return root
```







---------------------
# OOP
Here are the different components of a class:
- Public - The class members can be accessed from everywhere.
- Private - The class members can only be accessed by the defining class
- Protected - the class members can only be accessed by parent and inherited classes <br>

ABSTRACTION: Consider your method as the coffee machine and your input parameter as the buttons on the machine. 
    Abstraction allows you to create seamless programs by just knowing what method to call and what parameters to input. <br>
ENCAPSULATION: It is a group of properties and members under a single class or Object. With this principle, you can 
    prevent the repetition of code and also shorten the length of your code. <br>
INHERITANCE: It is the ability to acquire the properties of existing classes and create new ones. <br>
POLYMORPHISM: Polymorphism refers to one name with many forms. It is the ability of one function to perform in different 
    ways (different types, parameters, etc.) (overloading & overriding). <br>






---------------------
# Tricks and Tips
Merging two non-decreasing arrays: (A has m+n elements, n right-side zeros, B has n elements) <br>
Use a pointer on each array, and perform in-place replacement. 
Trick is to start from the biggest elements on the right side (i.e. start filling the right-side of the larger array). 
See question 0004. <br> <br>

Read -nth line of a file: <br>
Given a file, read the line n from the end of the file, with O(length). <br>
Similar to: Remove Nth Node From End of a LinkedList. <br>
Idea: Two pointers, one is n ahead of the other. When the front one reaches the end, the behind one is what we want. <br> <br>

Reverse Linked List: <br>
One solution is to iterate the nodes in original order and move them to the head of the list one by one. <br>
Another solution is by recursion. <br> <br>

If you need to add or delete a node frequently, a linked list could be a good choice. <br>
If you need to access an element by index often, an array might be a better choice than a linked list. <br> <br>

Binary tree traversal: <br>
PreOrder, InOrder, PostOrder - Use recursion <br>
LevelOrder (BFS) - Use a queue; In a while loop add a node.left, node.right, then pop(0). 
Use a for loop for each level within a while q. Each for loop exhausts elements of a level but populates it back again with another level. <br> <br>

Height of a binary tree: <br>
Recursive. In each iteration, choose the larger height of children + 1. <br> <br>

Circular queue (ring buffer) is used instead of regular queue, to efficiently use the space. <br>
It has two pointers, head & tail. Starts enqueue from right, and dequeue from left (FIFO). <br> <br>

BFS examples: Number of Islands, Open the circular lock, perfect squares summed to a given number <br>
FloodFill: Use BFS. Similar to counting islands but just one BFS on the given point. <br> <br>
Maze: Can be solved with BFS. However, instead of adding 4-neighbors to q, we let the ball roll (loop over dw,dw) and 
    only add the nodes when it hits the wall to q. <br>

MinStack: Retrieve the minimum of a stack in constant time (value, not element). Consider each node in the stack having a minimum value. 
Can implement by having each element as a tuple of (val, current_min). <br> <br>

Valid Paranthesis: What if whenever we encounter a matching pair of parenthesis in the expression,
we simply remove it from the expression? This would keep on shortening the expression. 
So when we see [] or {} or (), we just remove them, the remaining should still be valid. We can do that with stack. <br> <br>

Daily Temperatures: (How many days need to pass for a warmer temperature): 
Build a stack with elements of form (i, tem[i]). Each iteration compare a new element of the array with the top of the stack. 
If higher, pop from the stack and report index difference, if lower add to stack. <br> <br>

Clone Graph: Use recursion + hash map (map each old node to its new corresponding node). 
If already visited, append to neighbors of the corresponding old node, otherwise append a new node (recursed) of the neighbors.
Another idea: like divide and conquer. If is was a tree, we could just use recursion over the children of the root.
Graph is similar, except we have to check if a node is already visited (i.e. created) or not. <br> <br>

Implement Queue Using Stack: Each push is O(1) to S1. For pop(), transfer all elements one-by-one to S2 with O(n), then pop from it. So each operation is amortized O(1). <br> <br>

RangeSum: Implement sum(x[a:b]) but with O(1) time and O(n) memory.
Dynamic Programming: Pre-compute all ranges of x[0:i], then sum(x[a:b]) = s_0:b - s_0:a. Amortized O(1) as precomputation takes O(n) to do. <br> <br>

Disjoint Sets (See the template code: `\Graph\Disjoint_set_template.py'):
Quick find: store roots as array values. O(1) find and O(n) union.
Quick Union: store parents as array values. Connect roots of two given nodes for the union operation. Requires one traverse for find() and two for union, hence O(n) worst case, so better than quick find. 
Union by Rank (height): for union operation, we'll choose the root node of the taller tree as the new root node. This doesn't increase the height, so more efficient (making the tree balanced).
Path compression: optimizes the find function. Directly connect elements to the root node, using recursion.  
Optimized “disjoint set” with Path Compression and Union by Rank -> O(1) on average both for Find and Union. <br> <br>

Loop in a Graph: There is a loop, if a node has two parents. Run a BFS, and carry parents in a dict. 
In each iteration, exclude the parent of the current node, and check if the current neighbors already have a parent. 
If they ask for the path of a loop, can recurse on the nodes over the parents dictionary. <br> <br>

Spanning tree: A spanning tree is a connected subgraph in an undirected graph where all vertices are connected with the 
minimum number of edges. An “undirected graph” can have multiple spanning trees. What is a minimum spanning tree? 
A minimum spanning tree is a spanning tree with the minimum possible total edge weight in a “weighted undirected graph”.
Cut property: For any cut C of the graph, if the weight of an edge E in the cut-set of C is strictly smaller than the 
weights of all other edges of the cut-set of C, then this edge belongs to all MSTs of the graph. <br> <br>

Kruskal’s Algorithm: an algorithm to construct a “minimum spanning tree” of a “weighted undirected graph”.
1- Ascending sort all edges by their weight, 2- Add edges in that order into the minimum spanning tree. Skip the edges 
that would produce cycles in the minimum spanning tree. 3- Repeat step 2 until N-1 edges are added.
Time Complexity: O(E logE) <br> <br>

Prim's Algorithm: Each iteration add the edge with minimum weight that goes outside of the current visited set of nodes. 
Then add a vertex to the visited set and repeat until non-visited set is empty.
Time Complexity: O(E logV) for Binary heap, and O(E+V⋅logV) for Fibonacci heap. <br> <br>

“Kruskal’s algorithm” expands the “minimum spanning tree” by adding edges. Whereas “Prim’s algorithm” expands the “minimum spanning tree” by adding vertices. <br> <br>

Time Complexity of DFS: O(V + E) <br>
Time Complexity of BFS: O(V + E) <br> <br>

All Paths From Source to Target in a graph/tree: if we are asked to output True/False if a path exists, we can do BFS or DFS. 
But if we are asked to return all such paths, or one such path, one way is to use recursion (DFS) with a find_path function.
See learn_card -> Graph -> 0066, 0068. <br>
Another way is with BFS, similar to regular BFS pattern but instead of appending nodes to the queue, we append paths.
Can also use a hash_map and carry the parents of all nodes (useful for finding loops too)
See 0068. <br> <br>

Shortest path: <br>
Unweighted graph: BFS <br>
Weighted with positive weights: Dijkstra (our default BFS template but instead of carrying nodes or paths, we also record parents/summed-weights) <br>
Weighted: Bellman-Ford <br>
In a “graph with no negative-weight cycles” with N vertices, the shortest path between any two vertices has at most N-1 edges. <br>
In a “graph with negative weight cycles”, there is no shortest path. <br>

Bellman-Ford: Using Dynamic Programming to Find the Shortest Path: <br>
Shortest Path Faster Algorithm (SPFA):
It is an improved version of B-F algorithm: use a queue, add 0 to it. Then iterate. Each iteration, remove one item from
queue, look at edges going out of it; if those nodes can get updated with a smaller value and if they are not already in 
the queue, then add them to queue. Repeat until queue is empty.
O(V.E) (same as B-F in worst case) but faster overall. O(V) memory. <br> <br>

Topological sorting: (Kahn's algorithm) - O(E+V): e.g. for course scheduling
For DAGs (no cycle: There must be at least one vertex in the “graph” with an “in-degree” of 0) - Use a queue, add nodes 
with in-degree 0. Then do level-order-traversal, and for each removed node of queue, subtract one from each of its
neighbors' in-degrees, and if any becomes 0 we add it to the queue. Continue until queue is empty. <br>

Minimum Height Trees (find their root nodes) is similar: 
In starting all leaf nodes are pushed into the queue, then they are removed from the tree, next new leaf node is pushed 
in the queue, this procedure keeps on going until we have only 1 or 2 node in our tree, which represent the result. 
Similar to Kahn's algorithm but instead of starting from 0 in-degree nodes, we start from leaf nodes. <br>
There are at max 1 or 2 MHTs. <br>

To compute the runtime of a recursive algorithm, we can use the execution tree. e.g. for fib(n) it'll be O(2^n) and O(n) with memoization. <br>

BST:
The inorder traversal in BST will be in ascending order. <br>
To find a successor, go to the right once and then as many times to the left as you could. <br>
Search, Insert, and Delete are all O(h) in a BST. Search & Insert more straighforward, but for Delete we need to look 
into children (if no children, one children, or two children) <br>

Validate if a tree is a BST: 
Recursion, divide and conquer. Doesn't need to be symmetric. 
On each node, check to ensure its left and right are valid BSTs.
Then, compare root.val with max value of left side & min value of right side (have to carry/update them). <br>


Quick sort:
First, selects a pivot value to divide the list into two sublists. One sublist contains all the values that are less than
the pivot value, while the other sublist contains the values that are greater than or equal to the pivot value. 
This process is also called partitioning. Typically, one can choose the last element in the list as the pivot. <br>
After the partitioning process, the original list is then reduced into two smaller sublists. We then recursively sort the two sublists. <br>
Now we are sure that all elements in one sublist are less or equal than any element in another sublist. 
Therefore, we can simply concatenate the two sorted sublists that we obtain in step 2 to obtain the final sorted list. <br>
2-3 times faster than others.
Complexity: O(NlogN) best case and O(N^2) in the worst case (if sorted already). <br>

Quick select:
To find the k-th smallest element or the first k small elements.
Similar to quick-sort, but each iteration we remove one partion that doesn't include the k-th small element.
O(n) time complexity.


Backtracking:
is a general algorithm for finding all (or some) solutions to some computational problems (notably Constraint satisfaction problems or CSPs), 
which incrementally builds candidates to the solution and abandons a candidate ("backtracks") as soon as it determines that the candidate cannot lead to a valid solution. <br>
For recursion this means pruning the recursion tree because we don't take unnecessary paths. <br>

Print all subsets of length k from 1....n:
Recursion: The n-th element either appears in the subset (in which case we append it to the subset), or does not. <br>

Binary search:
See the template in Learn_Card -> Binary_Search -> Template.
A while loop is enough. No need for recursive calls. <br>

Intersection of two arrays:
Convert them to two sets. Then loop over one and search in the other. Each check is O(1) so overall O(m+n).
Can use set1 & set2 as well. <br>

Trie (Prefix Tree):
A Trie is a special form of a Nary tree. Typically, a trie is used to store strings. Each Trie node represents a string (a prefix). 
Each node might have several children nodes while the paths to different children nodes represent different characters. 
And the strings the child nodes represent will be the origin string represented by the node itself plus the character on the path.
Trie is widely used in various applications, such as autocomplete, spell checker, etc. 
See LearnCards -> Trie -> 0122 Trie implementation. <br>

```
Decision Tree:
A special form of binary tree. Naturally interpretable (using feature importance). (RF is not interpretable though).
For numerical attributes, the condition takes the form of less-than-or-equal comparison, i.e. "object.attribute ≤ C".
For categorical attributes, the condition is expressed as membership to a list of categorical values, i.e. object.attribute∈{C1,C2,C3}. For example, object.color∈{red, green, yellow}.
Stop conditions (on each split it selects the BEST feature to split on):
- (Ideal) All the examples that fall into the current node belong to the same category, i.e. no further classification is needed.
- (regularization to stop over-growing) The tree reaches its predefined max_depth.
- (regularization) The number of examples that fall into the current node is less than the predefined minimal_number_of_examples <br>
So two factors are important: splitting condition and metric for split quality assessment.
For splitting, we want to reduce the uncertainty of guessing the value in each split.
A good split, is when: before splitting features are not uniform, but after splitting features within each split are uniform.
- Gini gain: Gini_gain(L,L1,L2) = G(L) − G(L1).size(L1)/size(L) - G(L2).size(L2)/size(L)
  Gini impurity: 1 - sum(pi^2): Intuition: the probability of finding two samples with different labels in a game of random
   selection with replacement. So higher value means it's better to split. Equal feature values gives zero Gini, so no split. Gini gain: f(G,G1,G2).
- information_gain(L,L1,L2) = H(L) − H(L1).size(L1)/size(L) - H(L2).size(L2)/size(L). 
  The overall entropy of the splitted subgroups L1,L2, is the sum of the entropy for each subgroup weighted by its proportion with regards to the original group.
Entropy({1,1,2,2}) = 1 <br>
- Precision VS. Recall (e.g. in a dog-cat classification): 
  Precision: "How many selected items are relevant?" 
  Recall: "How many relevant items are selected?" <br>
- Feature importance:
In linear regression it'll be the weights of features.
In decision tree:
Since the best feature gets selected on each split, higher nodes should be more important with higher gini gain (but we normalize to sample count): 
  Importance = Sum( #samples / total_samples * node_gini_gain ). This can be further normalized to sum importances to 1. <br>
```


Zigzag scan in a 2D array:
Read diagonals (start with 0,i then 1,i-1, ..., i,0). Reverse every other diagonal. <br>



```
Heap:
Is a complete binary tree, where the value of each node must be no greater than (or no less than) the value of its child nodes.
Insertion and deletion: O(log N). Min/Max O(1).

Max heap: root is max
min heap: root is min

minHeap = [1,2,3]
Construct a Heap (only minHeap in python): heapq.heapify(minHeap)
Inserting an Element: heapq.heappush(minHeap, 5)
Getting the top element: minHeap[0]
Deleting the top element: heapq.heappop(minHeap)
Getting the Length of a Heap: len(minHeap)

0154 -> minimum number of meeting rooms. Requires two heaps. (there may be a reursive solution as well).
0158 -> find median in a stream. Reqiuires two heaps (a max and a min)
``` 
<br>


Hash Table: <br>
Each bucket contains an array to store all the values in the same bucket initially.
If there are too many values in the same bucket, these values will be maintained in a height-balanced binary search tree instead.
```
0162 - Given a non-empty array of integers nums, every element appears twice except for one. Find that single one - O(n) & constant memory. [Use summation of list/set]
We can delete a key in a dict: del hashmap[2]
We can check if a dictionary has a certain key like: dic = dict(zip(nums, list(range(len(nums)))))   ->   if diff in dic
If a={1:10, 2:20}, b={2:20, 1:10}, then a == b is True. Also 1 in a == True.
0175 - Find duplicate subtrees in a BT -> Convert traversals to strings, and store them in a dict with counts. 
    This is serialization of subtrees. Usually when we need to use nodes as keys, it's better to use their serializations instead.
0179 - 4Sum: -(a+b) = (c+d). Instead of doing O(n**4), we first construct a dict at O(n2) of the first pair, and similarly for the second pair. 
    Then if -(a+b) in sum-dict of the other pair (with O(1)) we add the counts. Overall O(n2).
```


Dynamic programming: <br>
Divide and conquer algorithms break a problem into subproblems, but these subproblems are not overlapping. <br>
DP requires both subproblems & overlapping. There are two ways to implement a DP algorithm: 
    Bottom-up, also known as tabulation. e.g. in Fib(n): f(0)=0, f(1)=1, for i in [2...n]: f(i) = f(i-1) + f(i-2)
    Top-down, also known as memoization. memo = hashmap; if i doesn't exist in memo: memo[i] = F(i - 1) + F(i - 2)
Top-down uses recursion (easier to write), and bottom-up uses iteration (faster).
If a problem is asking for the maximum/minimum/longest/shortest of something, the number of ways to do something, or if it is possible to reach a certain point, it is probably greedy or DP.
If the problem has constraints that cause decisions to affect other decisions, such as using one element prevents the usage of other elements, then we should consider using dynamic programming to solve the problem.
Generally speaking, divide and conquer approaches can be parallelized while dynamic programming approaches cannot be (easily) parallelized. 
    This is because the subproblems in divide an conquer approaches are independent of one another (they do not overlap) while in dynamic programming, the subproblems do overlap.
General framework: Try to identify. Then write recursion. Then add memoization.
0183 - House Robber: f(n) = max(f(n-1), f(n-2)+nums[n])
0191 - Coin Change: dp(amount) = min( dp(amount-coins[0]), dp(amount-coins[1]), ..., dp(amount-coins[n-1]) ) + 1. Therefore, the recursion includes a FOR loop.
    In some cases, the recursion occurs on an unknown (dynamic) number of previous iterations, which needs a loop (new state variable).
In some matrix/grid problems that we need to go from top-left corner to bottom-right corner (only right/down moves) we may use DP.
0212 - Number of dice rolls to reach target sum: DP with recursion: Use f(target, i) where target is the remaining amount and i is number of dice rolls left.
Useful to add: from functools import lru_cache. Then: @lru_cache  ->  def dp(i):...






---------------------
# Other useful websites
https://www.geeksforgeeks.org/   <br>
https://leetcode.com/discuss/general-discussion/460599/blind-75-leetcode-questions <br>
https://walkccc.me/LeetCode/ <br>
https://goodtecher.com/category/leetcode/ <br>


---------------------
# Questions to be solved again - Numbering from learn cards
0004 <br>
0014 <br>
0015 <br>
0016 - LinkedList <br>
0017, 0018 - LinkedList Cycle/Loop (Slow-Fast pointers or temp next node) <br>
0028 - LinkedList deep copy (2 passes with hash-map) <br>
0035 - Symmetric binary tree (recurse in a new is_mirror function that takes two input nodes) <br>
0036 - PathSum (A recursively called function, that takes a node & a value as input.) <br>
0037 - BuildTree from InOrder & PostOrder traversals (recurse: root is at the end of post-order. search for its value in inorder) <br>
0039 - BFS (Use a queue for BFS. But a for loop for each level within a while q.) <br>
0043, 0055 - Number of Islands (Connected Components). BFS/DFS per each node (cell) and keep updating a visited mask. 4 or 8 neighborhood. <br>
0044 - OpenTheLock: BFS using a queue. Root node is 0000; in each level each digit can be increased or decreased by 1, hence 8 children. Continue until seeing (shortest path) the target code. Use a 'seen' array-mask of length 10^4 to include deadends and already visited nodes. <br>
0045 - Perfect Squares: Use BFS. Start with root=collections.deque([n]). Start deducting 1^2, 2^2, ..., k^2 until difference > 0; Number of levels = shortest path = least number of perfect squares.  <br>
0058 - Number of provinces - BFS, similar to number of islands, but BFS is 1-D, it's on rows of the adjacency matrix. <br>
0059 - Graph Valid Tree: Search for loops. There is a loop, if a node has two parents. Run a BFS, and carry parents in a dict. In each iteration, exclude the parent of the current node, and check if the current neighbors already have a parent. <br>
0063, 0064 - Minimum Spanning Tree with Prim's algorithm. Memorize the template. <br>
0066, 0068 - All paths from a source node to a target node in a graph. <br>
0071, 0072 - Shortest path faster algorithm <br>
0075, 0073 - Minimum Height Trees, Course Schedule (prerequisites) <br>
0021 - Reverse a linked-list: iterate, and put each node as head. Easy code. <br>
0083 - return all the structurally unique BST's (binary search trees), which have exactly n nodes of unique values from 1 to n <br>
0084 - Merge sort: recursion <br>
0089, 0091, 0095, 0096 - Print all subsets of length k from 1...n. Recursion: The n-th element either appears in the subset (in which case we append it to the subset), or does not. 0091 is similar. <br>
0107 - BST, search for closest distance to target: Idea: Binary search with O(Log N). Each time compare with current root, and discard half of the tree. Update the closest distance every iteration if a better one is found. <br>
0138 - Convert Sorted Array to Binary Search Tree: Recursion. TreeNode(mid) , f(nums[:mid]), f([mid+1:]). Then connect node to its left and right. <br>
0143 - Search str within str - Idea: Start matching after the first letter is found. To speed up, you can start matching after both first and last letters are found. <br>
AMZN-003 - Container With Most Water (n vertical bars find max area): two pointers starting from the two sides, O(n). Each iteration reduce the width, by moving the shorter bar by one.
AMZN-008 - 2Sum, 3Sum, and 4Sum: can use hash maps for O(n), O(n2), O(n2). (sum = target).
AMZN-008 - For 3Sum-closest (sum closest to target) we can use 2-pointers like the previous WaterContainer problem for O(n2). Iterate through the array. For the current position i, set lo to i + 1, and hi to the last index: While the lo pointer is smaller than hi: Set sum to nums[i] + nums[lo] + nums[hi]. If the absolute difference between sum and target is smaller than the absolute value of diff: Set diff to target - sum. If sum is less than target, increment lo. Else, decrement hi.
AMZN-052 - Longest Palindromic Substring - Expand Around Center, O(n2): for i in range(length): max(get_max_len(s, i, i + 1), get_max_len(s, i, i))
