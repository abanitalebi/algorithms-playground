
"""
  Implement Trie (Prefix Tree)

A trie (pronounced as "try") or prefix tree is a tree data structure used to efficiently store and retrieve keys in a dataset of strings. There are various applications of this data structure, such as autocomplete and spellchecker.

Implement the Trie class:

Trie() Initializes the trie object.
void insert(String word) Inserts the string word into the trie.
boolean search(String word) Returns true if the string word is in the trie (i.e., was inserted before), and false otherwise.
boolean startsWith(String prefix) Returns true if there is a previously inserted string word that has the prefix prefix, and false otherwise.

https://leetcode.com/explore/learn/card/trie/147/basic-operations/1047/
"""

"""
    Idea: Define a Node class and a Trie class. For insert, search, startswith operations, search strating from root.
"""


class Node:
    def __init__(self, val='', is_word=False, children={}):
        self.val = val
        self.is_word = False
        self.children = {}


class Trie:
    def __init__(self, val='', is_word=False, children={}):
        self.root = Node(val=val, is_word=is_word, children=children)

    def insert(self, word: str) -> None:
        cur = self.root
        for ch in word:
            if ch not in cur.children:
                cur.children[ch] = Node(ch)
            cur = cur.children[ch]
        cur.is_word = True
        # print("insert: cur.val: {}".format(cur.val))

    def search(self, word: str) -> bool:
        cur = self.root
        for ch in word:
            if ch in cur.children:
                cur = cur.children[ch]
            else:
                return False
        if cur.is_word:
            return True

    def startsWith(self, prefix: str) -> bool:
        cur = self.root
        for ch in prefix:
            if ch in cur.children:
                cur = cur.children[ch]
            else:
                return False
        return True


# Your Trie object will be instantiated and called as such:
# obj = Trie()
# obj.insert(word)
# param_2 = obj.search(word)
# param_3 = obj.startsWith(prefix)

