
"""
  Replace Words

In English, we have a concept called root, which can be followed by some other word to form another longer word - let's call this word successor. For example, when the root "an" is followed by the successor word "other", we can form a new word "another".

Given a dictionary consisting of many roots and a sentence consisting of words separated by spaces, replace all the successors in the sentence with the root forming it. If a successor can be replaced by more than one root, replace it with the root that has the shortest length.

Return the sentence after the replacement.

https://leetcode.com/explore/learn/card/trie/148/practical-application-i/1053/
"""

class Solution:
    def replaceWords(self, dictionary: List[str], sentence: str) -> str:

        dic = {}
        for i in dictionary:
            dic[i] = 1

        words = sentence.split(" ")
        out = []
        for word in words:
            is_in = False
            for i in range(len(word)):
                if dic.get(word[:i]) is not None:
                    out.append(word[:i])
                    is_in = True
                    break
            if not is_in:
                out.append(word)

        return " ".join(out)
