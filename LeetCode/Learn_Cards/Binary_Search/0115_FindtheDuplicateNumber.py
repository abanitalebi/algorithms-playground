
"""
  Find the Duplicate Number

Given an array of integers nums containing n + 1 integers where each integer is in the range [1, n] inclusive.

There is only one repeated number in nums, return this repeated number.

You must solve the problem without modifying the array nums and uses only constant extra space.

https://leetcode.com/explore/learn/card/binary-search/146/more-practices-ii/1039/
"""

"""
    Idea: Flip the sign of each element upon passing over it. If an element is already negative that is a duplicate.
"""


class Solution:
    def findDuplicate(self, nums: List[int]) -> int:

        def fix_nums(arr):
            for j in range(len(arr)):
                if arr[j] < 0:
                    arr[j] *= -1
            return arr

        for i in range(len(nums)):
            if nums[abs(nums[i])] < 0:
                nums = fix_nums(nums)
                return abs(nums[i])
            nums[abs(nums[i])] *= -1
            # print(nums)
