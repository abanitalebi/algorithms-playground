
"""
  Find Minimum in Rotated Sorted Array

Suppose an array of length n sorted in ascending order is rotated between 1 and n times. For example, the array nums = [0,1,2,4,5,6,7] might become:

[4,5,6,7,0,1,2] if it was rotated 4 times.
[0,1,2,4,5,6,7] if it was rotated 7 times.
Notice that rotating an array [a[0], a[1], a[2], ..., a[n-1]] 1 time results in the array [a[n-1], a[0], a[1], a[2], ..., a[n-2]].

Given the sorted rotated array nums of unique elements, return the minimum element of this array.

You must write an algorithm that runs in O(log n) time.

https://leetcode.com/explore/learn/card/binary-search/126/template-ii/949/
"""


class Solution:
    def findMin(self, nums: List[int]) -> int:

        n = len(nums)
        left = 0
        right = n-1
        while left <= right:
            # print(nums[left:right+1])
            mid = (left + right) // 2
            if mid > 0 and nums[mid] < nums[mid-1]:
                return nums[mid]
            if mid < n-1 and nums[mid] > nums[mid+1]:
                return nums[mid+1]
            if nums[mid] > nums[left]:
                left = mid
            else:
                right = mid
            if len(nums[left:right+1]) == 1:
                break
        return nums[0]
