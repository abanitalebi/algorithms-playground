
"""
  Sqrt(x)

Given a non-negative integer x, compute and return the square root of x.

Since the return type is an integer, the decimal digits are truncated, and only the integer part of the result is returned.

Note: You are not allowed to use any built-in exponent function or operator, such as pow(x, 0.5) or x ** 0.5.

https://leetcode.com/explore/learn/card/binary-search/125/template-i/950/
"""

class Solution:
    def mySqrt(self, x: int) -> int:

        if x == 0 or x == 1:
            return x

        left = 0
        right = x
        while True:
            mid = (left + right) // 2
            if (mid*mid <= x) and ((mid+1)*(mid+1) > x):
                return mid
            if mid * mid > x:
                right = mid - 1
            if mid * mid < x:
                left = mid + 1


