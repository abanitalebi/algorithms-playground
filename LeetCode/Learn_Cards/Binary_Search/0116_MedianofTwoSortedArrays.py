
"""
  Median of Two Sorted Arrays
Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.

The overall run time complexity should be O(log (m+n)).

https://leetcode.com/explore/learn/card/binary-search/146/more-practices-ii/1040/
"""

"""
    Idea: https://www.youtube.com/watch?v=LPFhl65R7ww
    https://www.geeksforgeeks.org/median-two-sorted-arrays-different-sizes-ologminn-m/
    
    Goal: To partition x & y arrays such that left sides are less than right sides. Then median is found using edge elements.
    Suppose arrays are x & y, and x is shorter. Binary search on x to find where to partition.
    For each iteration of an x partition, a unique partition on y is found as the length of left sides = length of right sides
    O(min(m,n))
"""


class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        median = 0
        i = 0
        j = 0

        # def to find max
        def maximum(a, b) :
            return a if a > b else b

        # def to find minimum
        def minimum(a, b) :
            return a if a < b else b

        # def to find median of two sorted arrays
        def findMedianSortedArrays0(a, n, b, m) :

            global median, i, j
            min_index = 0
            max_index = n

            while (min_index <= max_index) :

                i = int((min_index + max_index) / 2)
                j = int(((n + m + 1) / 2) - i)

                # if i = n, it means that Elements from a[] in the second half is an empty
                # set. and if j = 0, it means that Elements from b[] in the first half is
                # an empty set. so it is necessary to check that, because we compare elements
                # from these two groups. Searching on right
                if (i < n and j > 0 and b[j - 1] > a[i]) :
                    min_index = i + 1

                # if i = 0, it means that Elements from a[] in the first half is an empty
                # set and if j = m, it means that Elements from b[] in the second half is an empty
                # set. so it is necessary to check that, because we compare elements from these two groups.
                # searching on left
                elif (i > 0 and j < m and b[j] < a[i - 1]) :
                    max_index = i - 1

                # we have found the desired halves.
                else :

                    # this condition happens when we don't have any elements in the first half from a[]
                    # so we returning the last element in b[] from the first half.
                    if (i == 0) :
                        median = b[j - 1]

                    # and this condition happens when we don't have any elements in the first half
                    # from b[] so we returning the last element in a[] from the first half.
                    elif (j == 0) :
                        median = a[i - 1]
                    else :
                        median = maximum(a[i - 1], b[j - 1])
                    break



            # calculating the median. If number of elements is odd there is one middle element.

            if ((n + m) % 2 == 1) :
                return median

            # Elements from a[] in the second half is an empty set.
            if (i == n) :
                return ((median + b[j]) / 2.0)

            # Elements from b[] in the second half is an empty set.
            if (j == m) :
                return ((median + a[i]) / 2.0)

            return ((median + minimum(a[i], b[j])) / 2.0)


        # Driver code
        a = nums1
        b = nums2
        n = len(nums1)
        m = len(nums2)

        # we need to define the smaller array as the first parameter to make
        # sure that the time complexity will be O(log(min(n,m)))
        if (n < m) :
            return findMedianSortedArrays0(a, n, b, m)
        else:
            return findMedianSortedArrays0(b, m, a, n)

