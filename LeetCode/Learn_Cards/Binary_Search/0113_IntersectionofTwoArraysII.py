
"""
  Intersection of Two Arrays II

Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must appear as many times as it shows in both arrays and you may return the result in any order.

https://leetcode.com/explore/learn/card/binary-search/144/more-practices/1029/
"""

"""
    Idea: Convert them to two sets and with O(m+n) find the intersection. Count the frequency of each common item ion both and repeat as many times as min of their frequencies.
"""


class Solution:
    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:

        def get_count(arr, val):
            count = 0
            for j in arr:
                if j == val:
                    count += 1
            return count

        if len(nums1) == 0 or len(nums2) == 0:
            return []
        set1 = set(nums1)
        set2 = set(nums2)
        res = []
        for i in set1:
            if i in set2:
                res += [i] * min(get_count(nums1, i), get_count(nums2, i))

        return res
