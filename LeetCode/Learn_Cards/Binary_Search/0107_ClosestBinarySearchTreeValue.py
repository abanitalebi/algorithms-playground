
"""
  Closest Binary Search Tree Value

Given the root of a binary search tree and a target value, return the value in the BST that is closest to the target.

https://leetcode.com/explore/learn/card/binary-search/136/template-analysis/1028/
"""

"""
    Idea: Binary search with O(Log N). Each time compare with current root, and discard half of the tree. 
    Update the closest distance every iteration if a better one is found.
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def closestValue(self, root: Optional[TreeNode], target: float) -> int:

        if root.left is None and root.right is None:
            return root.val

        cur_close = root.val
        while root is not None:
            diff = abs(root.val - target)
            if abs(root.val - target) < abs(cur_close - target):
                cur_close = root.val
            if diff == 0:
                return root.val
            if root is not None and root.val > target:
                root = root.left
            elif root is not None and root.val < target:
                root = root.right

        return cur_close
