
"""
  Valid Perfect Square

Given a positive integer num, write a function which returns True if num is a perfect square else False.

Follow up: Do not use any built-in library function such as sqrt.

https://leetcode.com/explore/learn/card/binary-search/137/conclusion/978/
"""


class Solution:
    def isPerfectSquare(self, num: int) -> bool:

        if num == 0 or num == 1:
            return True
        if num < 0:
            return False

        left = 0
        right = num
        while left <= right:
            mid = (left + right) // 2
            if mid * mid == num:
                return True
            elif mid * mid < num:
                left = mid + 1
            elif mid * mid > num:
                right = mid - 1
        return False
