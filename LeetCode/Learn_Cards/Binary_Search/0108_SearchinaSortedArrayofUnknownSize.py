
"""
  Search in a Sorted Array of Unknown Size

This is an interactive problem.

You have a sorted array of unique elements and an unknown size. You do not have an access to the array but you can use the ArrayReader interface to access it. You can call ArrayReader.get(i) that:

returns the value at the ith index (0-indexed) of the secret array (i.e., secret[i]), or
returns 231 - 1 if the i is out of the boundary of the array.
You are also given an integer target.

Return the index k of the hidden array where secret[k] == target or return -1 otherwise.

You must write an algorithm with O(log n) runtime complexity.

https://leetcode.com/explore/learn/card/binary-search/136/template-analysis/1061/
"""

"""
    Idea: First find n, by using a step size. Each time multiple step by 2 or 1/2 until n is found. Then binary search to find target.
"""


# """
# This is ArrayReader's API interface.
# You should not implement it, or speculate about its implementation
# """
#class ArrayReader:
#    def get(self, index: int) -> int:

class Solution:
    def search(self, reader: 'ArrayReader', target: int) -> int:

        # Base case, if len == 0 or 1
        large_value = pow(2,31) - 1
        if reader.get(0) == large_value:
            return -1
        if reader.get(1) == large_value:
            if reader.get(0) == target:
                return 0
            else:
                return -1


        # Find the length
        step = 2
        right = 2
        while step > 0:
            # print("right: {}, step: {}".format(right, step))
            if reader.get(right) != large_value and reader.get(right+1) == large_value:
                break
            if reader.get(right) != large_value:
                step *= 2
                right += step
            else:
                step = step // 2
                right -= step
        # print("len(arr): {}".format(right+1))


        # Binary search to find target
        left = 0
        while left <= right:
            mid = (left + right) // 2
            if reader.get(mid) == target:
                return mid
            if reader.get(mid) > target:
                right = mid - 1
            elif reader.get(mid) < target:
                left = mid + 1
        return -1



