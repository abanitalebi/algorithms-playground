
"""
  First Bad Version

You are a product manager and currently leading a team to develop a new product. Unfortunately, the latest version of your product fails the quality check. Since each version is developed based on the previous version, all the versions after a bad version are also bad.

Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one, which causes all the following ones to be bad.

You are given an API bool isBadVersion(version) which returns whether version is bad. Implement a function to find the first bad version. You should minimize the number of calls to the API.

https://leetcode.com/explore/learn/card/binary-search/126/template-ii/947/
"""


# The isBadVersion API is already defined for you.
# def isBadVersion(version: int) -> bool:

class Solution:
    def firstBadVersion(self, n: int) -> int:

        def find_bad(left, right):
            if left == 1 and isBadVersion(left):
                return 1
            if right == n and isBadVersion(right) and right > 0 and not isBadVersion(right-1):
                return n

            while left <= right:
                mid = (left + right) // 2
                if (mid > 0) and (isBadVersion(mid)) and (not isBadVersion(mid-1)):
                    return mid
                if isBadVersion(mid):
                    if mid > 0 and not isBadVersion(mid-1):
                        return mid-1
                    right = mid-1
                if not isBadVersion(mid):
                    if mid < n-1 and isBadVersion(mid+1):
                        return mid+1
                    left = mid+1

        return find_bad(1, n)
