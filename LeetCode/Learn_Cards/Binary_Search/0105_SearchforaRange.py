
"""
  Search for a Range

Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.

If target is not found in the array, return [-1, -1].

You must write an algorithm with O(log n) runtime complexity.

https://leetcode.com/explore/learn/card/binary-search/135/template-iii/944/
"""

"""
    Idea: One binary search to find "first" and one to find "last".
"""


class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:

        n = len(nums)
        if n == 0:
            return [-1,-1]

        # obtain the first target, and if not available return [-1,-1]
        first = -1
        last = -1
        left = 0
        right = n-1
        if nums[0] == target:
            first = 0
        else:
            while left <= right:
                mid = (left + right) // 2
                if nums[mid] < target and mid < n-1 and nums[mid+1] == target:
                    first = mid+1
                    break
                if nums[mid] < target:
                    left = mid+1
                if nums[mid] >= target:
                    right = mid-1

        if first == -1:
            return [-1,-1]
        print("first: {}".format(first))

        # obtain the second target
        left = 0
        right = n-1
        if nums[-1] == target:
            last = n-1
        else:
            while left <= right:
                mid = (left + right) // 2
                if nums[mid] == target and mid < n-1 and nums[mid+1] > target:
                    last = mid
                    break
                if nums[mid] <= target:
                    left = mid+1
                if nums[mid] > target:
                    right = mid-1

        return [first, last]
