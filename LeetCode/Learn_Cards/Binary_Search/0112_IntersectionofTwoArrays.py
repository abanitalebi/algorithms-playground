
"""
  Intersection of Two Arrays

Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must be unique and you may return the result in any order.

https://leetcode.com/explore/learn/card/binary-search/144/more-practices/1034/
"""

"""
    Idea: Convert them to two sets. Then loop over one and search in the other. Each check is O(1) so overall O(m+n).
    Can use set1 & set2 as well.
"""


class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:

        if len(nums1) == 0 or len(nums2) == 0:
            return []
        set1 = set(nums1)
        set2 = set(nums2)

        res = []
        for i in set1:
            if i in set2:
                res.append(i)
        return res
