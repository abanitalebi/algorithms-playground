
"""
  Binary Search

Given an array of integers nums which is sorted in ascending order, and an integer target, write a function to search target in nums. If target exists, then return its index. Otherwise, return -1.

You must write an algorithm with O(log n) runtime complexity.

https://leetcode.com/explore/learn/card/binary-search/138/background/1038/
"""

"""
    Idea: binary search: use 2 pointers to refer to the beginning and end of the search array.
"""

class Solution:
    def search(self, nums: List[int], target: int) -> int:

        def binary_search(arr, p1, p2, target):
            if len(arr) == 0:
                return -1
            if len(arr) == 1:
                if arr[0] == target:
                    return 0
                else:
                    return -1
            if len(arr) == 2:
                if arr[0] == target:
                    return 0
                elif arr[1] == target:
                    return 1
                else:
                    return -1

            mid = int((p1+p2)/2)
            # print("mid: {}, len(arr): {}, p1: {}, p2: {}".format(mid, len(arr), p1, p2))
            if arr[mid] == target:
                return mid
            if abs(mid-p1) == 1 and abs(mid-p2) == 1:
                if arr[p1] == target:
                    return p1
                elif arr[p2] == target:
                    return p2
                else:
                    return -1

            elif arr[mid] > target:
                return binary_search(arr, p1, mid, target)
            else:
                return binary_search(arr, mid, p2, target)

        return binary_search(nums, 0, len(nums)-1, target)
