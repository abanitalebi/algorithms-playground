
"""
  Two Sum II - Input array is sorted

Given a 1-indexed array of integers numbers that is already sorted in non-decreasing order, find two numbers such that they add up to a specific target number. Let these two numbers be numbers[index1] and numbers[index2] where 1 <= index1 < index2 <= numbers.length.

Return the indices of the two numbers, index1 and index2, added by one as an integer array [index1, index2] of length 2.

The tests are generated such that there is exactly one solution. You may not use the same element twice.

https://leetcode.com/explore/learn/card/binary-search/144/more-practices/1035/
"""

"""
    Idea: Two pointers. Can leverage a set for finding as it is O(1).
"""


class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:

        p1 = 0
        nums = set(numbers)
        while numbers[p1] <= target:
            if numbers[p1] == target:
                for p2 in range(len(numbers)):
                    if numbers[p2] == 0 and p2 != p1:
                        return [min(p1,p2)+1, max(p1,p2)+1]
            p2 = 1
            if target - numbers[p1] in nums:
                while numbers[p1] + numbers[p2] < target:
                    p2 += 1
            if target == numbers[p1] + numbers[p2] and p1 != p2:
                return [min(p1,p2)+1, max(p1,p2)+1]
            else:
                p1 += 1


        return []
