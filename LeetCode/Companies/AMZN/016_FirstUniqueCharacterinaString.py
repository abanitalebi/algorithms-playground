
"""
  First Unique Character in a String

Given a string s, find the first non-repeating character in it and return its index. If it does not exist, return -1.

https://leetcode.com/explore/interview/card/amazon/76/array-and-strings/480/
"""

class Solution:
    def firstUniqChar(self, s: str) -> int:
        dic = {}
        for letter in s:
            if dic.get(letter) is not None:
                dic[letter] += 1
            else:
                dic[letter] = 1

        for idx, letter in enumerate(s):
            if dic[letter] == 1:
                return idx

        return -1
