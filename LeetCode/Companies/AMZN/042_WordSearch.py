
"""
  Word Search

Given an m x n grid of characters board and a string word, return true if word exists in the grid.

The word can be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or vertically neighboring. The same letter cell may not be used more than once.

https://leetcode.com/explore/interview/card/amazon/84/recursion/2989/
"""


class Solution(object):
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        self.ROWS = len(board)
        self.COLS = len(board[0])
        self.board = board

        for row in range(self.ROWS):
            for col in range(self.COLS):
                if self.backtrack(row, col, word):
                    return True

        # no match found after all exploration
        return False


    def backtrack(self, row, col, suffix):
        # bottom case: we find match for each letter in the word
        if len(suffix) == 0:
            return True

        # Check the current status, before jumping into backtracking
        if row < 0 or row == self.ROWS or col < 0 or col == self.COLS \
                or self.board[row][col] != suffix[0]:
            return False

        ret = False
        # mark the choice before exploring further.
        self.board[row][col] = '#'
        # explore the 4 neighbor directions
        for rowOffset, colOffset in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
            ret = self.backtrack(row + rowOffset, col + colOffset, suffix[1:])
            # break instead of return directly to do some cleanup afterwards
            if ret: break

        # revert the change, a clean slate and no side-effect
        self.board[row][col] = suffix[0]

        # Tried all directions, and did not find any match
        return ret









import copy

class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        m = len(board)
        n = len(board[0])

        if m == 1 and n == 1:
            return board[0][0] == word

        def bfs(board, word, seen, i, j):
            dr = [-1,1,0,0]
            dc = [0,0,-1,1]
            q = collections.deque([(i,j,word[0])])
            # seen[i][j] = 1
            while q:
                q_len = len(q)
                for _ in range(q_len):
                    cur_node = q.popleft()
                    row, col, w = cur_node[0], cur_node[1], cur_node[2]
                    if len(w) >= len(word):
                        continue
                    if w == word[:len(w)]:
                        seen[row][col] = 1
                    for ddr,ddc in zip(dr,dc):
                        r = row + ddr
                        c = col + ddc
                        # if w == "ABCCE":
                        print("w: {}, row: {}, col: {}, r: {}, c: {}, seen: {}".format(w,row,col,r,c,seen))
                        if 0 <= r < m and 0 <= c < n and seen[r][c] == 0:
                            if w + board[r][c] == word:
                                return True
                            else:
                                if w == word[:len(w)]:
                                    q.append((r,c,w + board[r][c]))
            return False


        visited = []
        for i in range(m):
            t = []
            for j in range(n):
                t.append(0)
            visited.append(t)

        for i in range(m):
            for j in range(n):
                if board[i][j] == word[0]:
                    if len(word) == 1:
                        return True
                    print("i: {}, j: {}, visited: {}".format(i,j,visited))
                    found = bfs(board, word, copy.deepcopy(visited), i, j)
                    if found:
                        return True

        return False
