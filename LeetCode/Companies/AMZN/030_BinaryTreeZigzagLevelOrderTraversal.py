
"""
  Binary Tree Zigzag Level Order Traversal

Given the root of a binary tree, return the zigzag level order traversal of its nodes' values. (i.e., from left to right, then right to left for the next level and alternate between).

https://leetcode.com/explore/interview/card/amazon/78/trees-and-graphs/2980/
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def zigzagLevelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        if root is None:
            return []
        q = collections.deque([root])
        level = -1
        t = []
        while q:
            level += 1
            l = []
            q_len = len(q)
            for i in range(q_len):
                cur = q.popleft()
                l.append(cur.val)
                if cur.left is not None:
                    q.append(cur.left)
                if cur.right is not None:
                    q.append(cur.right)
            print(l)
            if level % 2 == 0:
                t.append(l)
            else:
                t.append(l[::-1])
        return t
