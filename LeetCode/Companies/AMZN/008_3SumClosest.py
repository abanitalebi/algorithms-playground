
"""
  3Sum Closest

Given an integer array nums of length n and an integer target, find three integers in nums such that the sum is closest to target.

Return the sum of the three integers.

You may assume that each input would have exactly one solution.

https://leetcode.com/explore/interview/card/amazon/76/array-and-strings/2967/
"""

"""
2Sum, 3Sum, and 4Sum: can use hash maps for O(n), O(n2), O(n2). (sum = target).
For 3Sum-close (sum closest to target) we can use 2-pointers like the previous WaterContainer problem for O(n2). 

Algorithm:

Initialize the minimum difference diff with a large value.
Sort the input array nums.
Iterate through the array:
    For the current position i, set lo to i + 1, and hi to the last index.
    While the lo pointer is smaller than hi:
        Set sum to nums[i] + nums[lo] + nums[hi].
        If the absolute difference between sum and target is smaller than the absolute value of diff:
            Set diff to target - sum.
        If sum is less than target, increment lo.
        Else, decrement hi.
    If diff is zero, break from the loop.
Return the value of the closest triplet, which is target - diff.
"""


class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:
        diff = float('inf')
        nums.sort()
        for i in range(len(nums)):
            lo, hi = i + 1, len(nums) - 1
            while (lo < hi):
                sum = nums[i] + nums[lo] + nums[hi]
                if abs(target - sum) < abs(diff):
                    diff = target - sum
                if sum < target:
                    lo += 1
                else:
                    hi -= 1
            if diff == 0:
                break
        return target - diff
