
"""
  Reverse Linked List

Given the head of a singly linked list, reverse the list, and return the reversed list.

https://leetcode.com/explore/interview/card/amazon/77/linked-list/2979/
"""


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

def pr(head):
    mylist = []
    while head is not None:
        mylist.append(head.val)
        head = head.next
    print(mylist)


class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:

        if head is None or head.next is None:
            return head

        cur = head
        prev = cur
        cur = cur.next
        # pr(head)
        while prev.next is not None:
            nex = cur.next

            # Remove current
            prev.next = nex

            # Put it before head
            cur.next = head
            head = cur

            # Iterate
            cur = nex

            # pr(head)

        return head

