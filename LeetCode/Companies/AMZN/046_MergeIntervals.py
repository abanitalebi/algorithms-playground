
"""
  Merge Intervals

Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals, and return an array of the non-overlapping intervals that cover all the intervals in the input.

https://leetcode.com/explore/interview/card/amazon/79/sorting-and-searching/2993/
"""

class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:

        starts = [a[0] for a in intervals]
        ends = [a[1] for a in intervals]

        heapq.heapify(starts)
        heapq.heapify(ends)

        intv = []
        cur_min = heapq.heappop(starts)
        cur_max = heapq.heappop(ends)
        while starts and ends:
            next_min = heapq.heappop(starts)
            next_max = heapq.heappop(ends)

            if next_min <= cur_max:
                cur_min = cur_min
                cur_max = next_max
            else:
                intv.append([cur_min, cur_max])
                cur_min = next_min
                cur_max = next_max
        intv.append([cur_min, cur_max])
        return intv

