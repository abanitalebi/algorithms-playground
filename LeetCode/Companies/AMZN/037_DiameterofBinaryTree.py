
"""
  Diameter of Binary Tree

Given the root of a binary tree, return the length of the diameter of the tree.

The diameter of a binary tree is the length of the longest path between any two nodes in a tree. This path may or may not pass through the root.

The length of a path between two nodes is represented by the number of edges between them.

https://leetcode.com/explore/interview/card/amazon/78/trees-and-graphs/2985/
"""

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def diameterOfBinaryTree(self, root: Optional[TreeNode]) -> int:

        def get_height(node):
            if node is None:
                return 0
            left = get_height(node.left)
            right = get_height(node.right)
            return max(left,right) + 1

        if root is None:
            return 0
        if root.left is None and root.right is None:
            return 0

        left_height = get_height(root.left)
        right_height = get_height(root.right)

        return max(self.diameterOfBinaryTree(root.left), self.diameterOfBinaryTree(root.right), left_height + right_height)
