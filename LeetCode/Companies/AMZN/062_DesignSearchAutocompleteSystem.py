
"""
  Design Search Autocomplete System

Design a search autocomplete system for a search engine. Users may input a sentence (at least one word and end with a special character '#').

You are given a string array sentences and an integer array times both of length n where sentences[i] is a previously typed sentence and times[i] is the corresponding number of times the sentence was typed. For each input character except '#', return the top 3 historical hot sentences that have the same prefix as the part of the sentence already typed.

Here are the specific rules:

The hot degree for a sentence is defined as the number of times a user typed the exactly same sentence before.
The returned top 3 hot sentences should be sorted by hot degree (The first is the hottest one). If several sentences have the same hot degree, use ASCII-code order (smaller one appears first).
If less than 3 hot sentences exist, return as many as you can.
When the input is a special character, it means the sentence ends, and in this case, you need to return an empty list.
Implement the AutocompleteSystem class:

AutocompleteSystem(String[] sentences, int[] times) Initializes the object with the sentences and times arrays.
List<String> input(char c) This indicates that the user typed the character c.
Returns an empty array [] if c == '#' and stores the inputted sentence in the system.
Returns the top 3 historical hot sentences that have the same prefix as the part of the sentence already typed. If there are fewer than 3 matches, return them all.

https://leetcode.com/explore/interview/card/amazon/81/design/3000/
"""



# https://leetcode.com/problems/design-search-autocomplete-system/


class TrieNode:
    def __init__(self):
        self.children = {}
        self.frequency = 0
        self.end = False


class Trie:
    def __init__(self):
        self.root = TrieNode()

    def update_sentence(self, sentence, frequency):
        root = self.root
        for c in sentence:
            if c not in root.children:
                root.children[c] = TrieNode()
            root = root.children[c]

        root.frequency += frequency
        root.end = True

    def get_frequent_sentences(self, prefix, cur_node):
        def find_sentences(prefix, node):
            if node.end:
                sentences.append((-node.frequency, ''.join(prefix)))

            for c in node.children:
                prefix.append(c)
                find_sentences(prefix, node.children[c])
                prefix.pop()

        sentences = []
        find_sentences(prefix, cur_node)
        sentences.sort()

        return [s[1] for s in sentences[:3]]


class AutocompleteSystem(object):
    def __init__(self, sentences, times):
        """
        :type sentences: List[str]
        :type times: List[int]
        """
        self.trie = Trie()
        for sentence, time in zip(sentences, times):
            self.trie.update_sentence(sentence, time)

        self.cur_trie_node = self.trie.root
        self.prefix = []

    def input(self, c):
        """
        :type c: str
        :rtype: List[str]
        """
        if c == '#':
            self.trie.update_sentence(self.prefix, 1)
            self.cur_trie_node = self.trie.root
            self.prefix = []
            return []
        else:
            self.prefix.append(c)
            if self.cur_trie_node is None or c not in self.cur_trie_node.children:
                self.cur_trie_node = None
                return []
            self.cur_trie_node = self.cur_trie_node.children[c]
            return self.trie.get_frequent_sentences(self.prefix, self.cur_trie_node)

# Your AutocompleteSystem object will be instantiated and called as such:
# obj = AutocompleteSystem(sentences, times)
# param_1 = obj.input(c)
