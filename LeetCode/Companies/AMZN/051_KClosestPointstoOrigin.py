
"""
  K Closest Points to Origin

Given an array of points where points[i] = [xi, yi] represents a point on the X-Y plane and an integer k, return the k closest points to the origin (0, 0).

The distance between two points on the X-Y plane is the Euclidean distance (i.e., √(x1 - x2)2 + (y1 - y2)2).

You may return the answer in any order. The answer is guaranteed to be unique (except for the order that it is in).

https://leetcode.com/explore/interview/card/amazon/79/sorting-and-searching/2996/
"""


class Solution:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:

        arr = []
        for i, point in enumerate(points):
            d = math.sqrt(point[0]**2 + point[1]**2)
            arr.append([d, point[0], point[1]])
        heapq.heapify(arr)

        out = []
        for i in range(k):
            val = heapq.heappop(arr)
            out.append([val[1], val[2]])

        return out
