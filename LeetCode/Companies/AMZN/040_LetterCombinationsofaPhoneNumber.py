
"""
  Letter Combinations of a Phone Number

Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent. Return the answer in any order.

A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.

https://leetcode.com/explore/interview/card/amazon/84/recursion/521/
"""


class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        dic = {'2': ['a','b','c'], '3': ['d','e','f'], '4': ['g','h','i'], '5': ['j','k','l'], '6': ['m','n','o'], '7': ['p','q','r','s'], '8': ['t','u','v'], '9': ['w','x','y','z']}

        dig = list(digits)
        if len(dig) == 1:
            return dic[dig[0]]
        if len(dig) == 0:
            return []

        n = len(dig)
        prev = self.letterCombinations(digits[:-1])
        cur = []
        letters = dic[digits[-1]]
        for word in prev:
            for let in letters:
                cur.append(''.join(list(word) + [let]))
        return cur
