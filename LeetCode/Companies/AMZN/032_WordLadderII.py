
"""
  Word Ladder II

A transformation sequence from word beginWord to word endWord using a dictionary wordList is a sequence of words beginWord -> s1 -> s2 -> ... -> sk such that:

Every adjacent pair of words differs by a single letter.
Every si for 1 <= i <= k is in wordList. Note that beginWord does not need to be in wordList.
sk == endWord
Given two words, beginWord and endWord, and a dictionary wordList, return all the shortest transformation sequences from beginWord to endWord, or an empty list if no such sequence exists. Each sequence should be returned as a list of the words [beginWord, s1, s2, ..., sk].

https://leetcode.com/explore/interview/card/amazon/78/trees-and-graphs/483/
"""



class Solution:
    def findLadders(self, beginWord: str, endWord: str, wordList: List[str]) -> int:

        if endWord not in wordList:
            return []

        word_queue = deque([[beginWord, [beginWord]]])
        visited = set([beginWord])

        patterns = defaultdict(list)
        paths = []
        word_len = len(beginWord)

        for word in wordList:
            for index in range(word_len):
                patterns[word[:index] + '*' + word[index + 1:]].append(word)

        while word_queue:
            current_level_length = len(word_queue)
            current_level_visited = set()

            for _ in range(current_level_length):
                word, word_sequence = word_queue.popleft()
                if word == endWord:
                    paths.append(word_sequence)
                    continue
                for index in range(word_len):
                    pattern  = word[ : index] + '*' + word[index + 1: ]
                    for adjacent_word in patterns[pattern]:
                        if adjacent_word not in visited:
                            word_queue.append([adjacent_word, word_sequence[:] + [adjacent_word]])
                            current_level_visited.add(adjacent_word)
            visited.update(current_level_visited)

        return paths






class Solution:

    def findNeighbors(self, cur_word, word_list):
        neighbors = set()
        for i in range(len(cur_word)):
            for char in 'qwertyuiopasdfghjklzxcvbnm':
                next_word = cur_word[:i]+char+cur_word[i+1:]
                if next_word in word_list:
                    neighbors.add(next_word)
        return neighbors

    def findLadders(self, beginWord: str, endWord: str, wordList: List[str]) -> List[List[str]]:

        # Get a dict of neighbors
        d = {}
        all_nodes = wordList + [beginWord]
        lw = len(wordList[0])
        # for word1 in all_nodes:
        #     neighbors = []
        #     for word in wordList:
        #         com = 0
        #         for j in range(lw):
        #             if word[j] == word1[j]:
        #                 com += 1
        #         if com == lw - 1:
        #             neighbors.append(word)
        #     d[word1] = neighbors
        # print("Dict created...")


        n = len(beginWord)
        out = []
        root = beginWord
        paths = [root]
        q = collections.deque([paths])
        found_some = False
        while q:
            q_len = len(q)
            for i in range(q_len):
                cur_path = q.popleft()
                cur_node = cur_path[-1]

                # add adjacent lists to q - strs neighbor to cur_node that are not in cur_path
                # neighbors = []
                # lw = len(wordList[0])
                # for word in wordList:
                #     com = 0
                #     for j in range(lw):
                #         if word[j] == cur_node[j]:
                #             com += 1
                #     if com == lw - 1:
                #         if word not in cur_path:
                #             neighbors.append(word)

                # neighbors = []
                # for n in d[cur_node]:
                #     if n not in cur_path:
                #         neighbors.append(n)

                neighbors = self.findNeighbors(cur_node, wordList)

                # check if endWord is seen
                for new_node in neighbors:
                    if new_node == endWord:
                        out.append(cur_path + [new_node])
                        found_some = True
                    else:
                        q.append(cur_path + [new_node])

            if found_some:
                break
        return out
