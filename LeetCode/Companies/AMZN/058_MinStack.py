
"""
  Min Stack

Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

Implement the MinStack class:

MinStack() initializes the stack object.
void push(int val) pushes the element val onto the stack.
void pop() removes the element on the top of the stack.
int top() gets the top element of the stack.
int getMin() retrieves the minimum element in the stack.

https://leetcode.com/explore/interview/card/amazon/81/design/503/
"""


"""
    Idea: MinStack: Retrieve the minimum of a stack in constant time. Consider each node in the stack having a minimum value.
        Can implement by having each element as a tuple of (val, current_min).
"""


class MinStack:

    def __init__(self):
        self._top = None
        self._len = 0
        self._list = []
        self._min = None

    def push(self, val: int) -> None:
        self._len += 1
        self._top = val
        if self._len == 1:
            self._min = val
        else:
            if val <= self._min:
                self._min = val
        self._list += [(val, self._min)]


    def pop(self) -> None:
        if self._len > 0:
            self._len -= 1
            if self._len > 0:
                self._list = self._list[:-1]
                self._top = self._list[-1][0]
                self._min = self._list[-1][1]
            else:
                self._list = []
                self._top = None
                self._min = None
        else:
            return -1


    def top(self) -> int:
        return self._top


    def getMin(self) -> int:
        return self._min



# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(val)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()

