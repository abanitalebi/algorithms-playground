
"""
  Container With Most Water

You are given an integer array height of length n. There are n vertical lines drawn such that the two endpoints of the ith line are (i, 0) and (i, height[i]).

Find two lines that together with the x-axis form a container, such that the container contains the most water.

Return the maximum amount of water a container can store.

Notice that you may not slant the container.

https://leetcode.com/explore/interview/card/amazon/76/array-and-strings/2963/
"""


"""
    Idea: two pointers starting from the two sides. Each iteration reduce the width, by moving the shorter bar by one.
"""


class Solution:
    def maxArea(self, height: List[int]) -> int:
        n = len(height)
        if n == 0:
            return 0

        left = 0
        right = n - 1
        m = (right - left) * min(height[left], height[right])

        while left < right:
            if height[left] < height[right]:
                left += 1
            else:
                right -= 1
            area = (right - left) * min(height[left], height[right])
            if area > m:
                m = area

        return m





# Simple looping solution will give a time limit exceeded error
class Solution:
    def maxArea(self, height: List[int]) -> int:
        n = len(height)
        if n == 0:
            return 0

        m = 0
        for i in range(n):
            for j in range(i+1,n):
                area = (j-i) * min(height[j], height[i])
                if area > m:
                    m = area
        return m