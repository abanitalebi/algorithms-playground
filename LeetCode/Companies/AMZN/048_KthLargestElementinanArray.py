
"""
  Kth Largest Element in an Array

Given an integer array nums and an integer k, return the kth largest element in the array.

Note that it is the kth largest element in the sorted order, not the kth distinct element.

https://leetcode.com/explore/interview/card/amazon/79/sorting-and-searching/482/
"""


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        arr = [-i for i in nums]
        heapq.heapify(arr)
        for i in range(k):
            out = heapq.heappop(arr)
        return -out
