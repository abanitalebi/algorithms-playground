
"""
  Maximum Subarray

Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

A subarray is a contiguous part of an array.

https://leetcode.com/explore/interview/card/amazon/80/dynamic-programming/899/
"""

class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        # Initialize our variables using the first element.
        current_subarray = max_subarray = nums[0]

        # Start with the 2nd element since we already used the first one.
        for num in nums[1:]:
            # If current_subarray is negative, throw it away. Otherwise, keep adding to it.
            current_subarray = max(num, current_subarray + num)
            max_subarray = max(max_subarray, current_subarray)

        return max_subarray





class Solution:
    def maxSubArray(self, nums: List[int]) -> int:

        n = len(nums)
        print(n)
        if n == 1:
            return nums[0]
        memo = {1: nums[0], 2: max(nums[0], nums[1], nums[0]+nums[1])}

        @lru_cache
        def f(i):
            if i not in memo:
                m = -sys.maxsize
                acc = 0
                for j in range(i):
                    acc = acc + nums[i-j-1]
                    m = max(m, acc)
                memo[i] = max(f(i-1), m)

            return memo[i]

        return f(n)
