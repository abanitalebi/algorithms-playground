
"""
  Copy List with Random Pointer

A linked list of length n is given such that each node contains an additional random pointer, which could point to any node in the list, or null.

Construct a deep copy of the list. The deep copy should consist of exactly n brand new nodes, where each new node has its value set to the value of its corresponding original node. Both the next and random pointer of the new nodes should point to new nodes in the copied list such that the pointers in the original list and copied list represent the same list state. None of the pointers in the new list should point to nodes in the original list.

For example, if there are two nodes X and Y in the original list, where X.random --> Y, then for the corresponding two nodes x and y in the copied list, x.random --> y.

Return the head of the copied linked list.

The linked list is represented in the input/output as a list of n nodes. Each node is represented as a pair of [val, random_index] where:

val: an integer representing Node.val
random_index: the index of the node (range from 0 to n-1) that the random pointer points to, or null if it does not point to any node.
Your code will only be given the head of the original linked list.

https://leetcode.com/explore/interview/card/amazon/77/linked-list/2978/
"""

"""
# Definition for a Node.
class Node:
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        self.val = int(x)
        self.next = next
        self.random = random
"""

class Solution:
    def copyRandomList(self, head: 'Node') -> 'Node':

        if head is None:
            return None

        cur1 = head
        # N = -1
        head2 = Node(0)
        cur2 = head2
        hash_map = {}
        while cur1 is not None:
            # N += 1
            cur2.next = Node(cur1.val)
            cur2 = cur2.next
            hash_map[cur1] = cur2
            cur1 = cur1.next

        cur1 = head
        cur2 = head2.next
        while cur1 is not None:
            if cur1.random is None:
                cur2.random = None
            else:
                cur2.random = hash_map[cur1.random]
            cur2 = cur2.next
            cur1 = cur1.next

        return head2.next


