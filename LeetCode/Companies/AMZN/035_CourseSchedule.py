
"""
  Course Schedule

There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
Return true if you can finish all courses. Otherwise, return false.

https://leetcode.com/explore/interview/card/amazon/78/trees-and-graphs/2983/
"""

"""
    Idea: Kahn's algorithm for topological sorting: BFS, add to q nodes with in-degree of zero.
"""

class Solution:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        n = numCourses
        if n <= 1:
            return True

        d = {}
        degrees = [0] * n
        req = {}
        for cs in prerequisites:
            if cs[0] not in d:
                d[cs[0]] = [cs[1]]
                degrees[cs[0]] = 1
            else:
                d[cs[0]].append(cs[1])
                degrees[cs[0]] += 1
            if cs[1] not in req:
                req[cs[1]] = [cs[0]]
            else:
                req[cs[1]].append(cs[0])

        # print("degrees: {}, requirements: {}".format(degrees, req))

        z = []
        for i in range(n):
            if i not in d:
                z.append(i)
        if len(z) == 0:
            return False

        visited = z
        q = collections.deque(z)
        while q:
            q_len = len(q)
            for i in range(q_len):
                cur = q.popleft()
                visited.append(cur)
                # print("cur: {}".format(cur))
                neighbors = req.get(cur)
                if not neighbors:
                    continue
                for node in neighbors:
                    degrees[node] -= 1
                    if degrees[node] == 0:
                        q.append(node)
                # print("degrees: {}".format(degrees))
        visited = list(set(visited))
        # print("visited: {}".format(visited))
        if len(visited) == n:
            return True
        else:
            return False
