
"""
  Missing Number

Given an array nums containing n distinct numbers in the range [0, n], return the only number in the range that is missing from the array.

https://leetcode.com/explore/interview/card/amazon/76/array-and-strings/2971/
"""

"""
    Idea: Negate nums[i] if it's positive. Then the only element that is positive shows what's not seen. O(n).
"""

class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        n = len(nums)
        saw_n = False
        saw_0 = False
        for i in range(n):
            if abs(nums[i]) == n:
                saw_n = True
            elif abs(nums[i]) == 0:
                nums[0] *= -1
                saw_0 = True
                z = i
            else:
                nums[abs(nums[i])] = -nums[abs(nums[i])]

        print(nums)

        if saw_n == False:
            return n
        if saw_0 == False:
            return 0

        for i in range(n):
            if nums[i] > 0:
                return i

        return z
