
"""
  Add Two Numbers

You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

https://leetcode.com/explore/interview/card/amazon/77/linked-list/513/
"""


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        if l1 is None:
            return l2
        if l2 is None:
            return l1

        head = ListNode()
        cur = head
        carry = 0
        val = 0
        while (l1 is not None) and (l2 is not None):
            val = l1.val + l2.val + carry
            if val < 10:
                val = l1.val + l2.val + carry
                carry = 0
            else:
                val = l1.val + l2.val + carry - 10
                carry = 1
            nex = ListNode(val)
            cur.next = nex
            cur = cur.next
            l1 = l1.next
            l2 = l2.next
            # print(carry)

        if l2 is None:
            while l1 is not None:
                val = l1.val + carry
                if val < 10:
                    val = l1.val + carry
                    carry = 0
                else:
                    val = l1.val + carry - 10
                    carry = 1
                nex = ListNode(val)
                cur.next = nex
                cur = cur.next
                l1 = l1.next

        if l1 is None:
            while l2 is not None:
                val = l2.val + carry
                if val < 10:
                    val = l2.val + carry
                    carry = 0
                else:
                    val = l2.val + carry - 10
                    carry = 1
                nex = ListNode(val)
                cur.next = nex
                cur = cur.next
                l2 = l2.next

        if carry == 1:
            cur.next = ListNode(1)

        return head.next

