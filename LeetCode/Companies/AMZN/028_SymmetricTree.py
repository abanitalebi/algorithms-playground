
"""
  Symmetric Tree

Given the root of a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).

https://leetcode.com/explore/interview/card/amazon/78/trees-and-graphs/507/
"""




# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:

    def isMirror(self, root1, root2):

        if (root1 is None) and (root2 is None):
            return True
        if (root1 is not None) and (root2 is None):
            return False
        if (root1 is None) and (root2 is not None):
            return False

        if root1.val != root2.val:
            return False
        else:
            return self.isMirror(root1.left, root2.right) and self.isMirror(root1.right, root2.left)



    def isSymmetric(self, root: Optional[TreeNode]) -> bool:

        return self.isMirror(root.left, root.right)

