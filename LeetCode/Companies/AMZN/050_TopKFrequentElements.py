
"""
  Top K Frequent Elements

Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any order.

https://leetcode.com/explore/interview/card/amazon/79/sorting-and-searching/2995/
"""



class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:

        counts = {}
        for i in range(len(nums)):
            if counts.get(nums[i]) is not None:
                counts[nums[i]] += 1
            else:
                counts[nums[i]] = 1

        freqs = zip(counts.values(), counts.keys())
        freqs = sorted(freqs)

        out = []
        for item in freqs[-k:]:
            out.append(item[1])

        return out
