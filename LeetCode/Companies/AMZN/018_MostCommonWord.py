
"""
  Most Common Word

Given a string paragraph and a string array of the banned words banned, return the most frequent word that is not banned. It is guaranteed there is at least one word that is not banned, and that the answer is unique.

The words in paragraph are case-insensitive and the answer should be returned in lowercase.

https://leetcode.com/explore/interview/card/amazon/76/array-and-strings/2973/
"""

class Solution:
    def mostCommonWord(self, paragraph: str, banned: List[str]) -> str:

        d = {}
        p0 = paragraph.split(",")
        p = []
        for i in range(len(p0)):
            p += p0[i].split(" ")
        print(p)
        b = set(banned)
        punc = set(["!", "?", "'", ",", ";", ".", " "])
        for i in range(len(p)):
            if len(p[i]) == 0:
                continue
            p[i] = p[i].lower()
            while p[i][-1] in punc:
                p[i] = p[i][:-1]
            while p[i][0] in punc:
                p[i] = p[i][1:]
            if p[i] not in b:
                if p[i] not in d:
                    d[p[i]] = 1
                else:
                    d[p[i]] += 1
        print(d)
        return sorted(zip(d.values(), d.keys()))[-1][1]
