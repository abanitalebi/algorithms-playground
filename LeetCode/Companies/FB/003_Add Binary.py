
"""
  Add Binary

Given two binary strings a and b, return their sum as a binary string.

https://leetcode.com/explore/interview/card/facebook/5/array-and-strings/263/
"""

class Solution:
    def addBinary(self, a, b) -> str:
        x, y = int(a, 2), int(b, 2)
        while y:
            answer = x ^ y
            carry = (x & y) << 1
            x, y = answer, carry
        return bin(x)[2:]
