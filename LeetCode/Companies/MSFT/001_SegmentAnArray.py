
"""
Given an integer array and two integers T1 < T2, write code to in-place segment the array to 3 segments:
(<T1  ,  >=T1 & < T2  ,  >= T2), with one scan.
"""


def segment_array(nums, T1, T2):
    N = len(nums)

    p1 = 0    # position to place the next small element
    p2 = N-1  # position to place the next large element
    i = 0     # iterator over the array
    print("nums: {}, T1: {}, T2: {}".format(nums, T1, T2))
    while i < N and p1 < p2:
        if nums[i] < T1:
            # swap nums[i] with nums[p1]
            nums[i], nums[p1] = nums[p1], nums[i]
            i += 1
            p1 += 1
        elif nums[i] > T2:
            # swap nums[i] with nums[p2]
            nums[i], nums[p2] = nums[p2], nums[i]
            p2 -= 1
            if i == N-1:
                break
        else:
            i += 1
        # print("i: {}, p1: {}, p2: {}".format(i, p1, p2))
    print("nums: {}".format(nums))
    return nums


if __name__ == "__main__":
    # nums = [1, 2, 3, 4, 5, 6]
    nums = [1, 2, 3, 4, 5, 1]
    T1 = 3
    T2 = 5
    nums = segment_array(nums, T1, T2)
