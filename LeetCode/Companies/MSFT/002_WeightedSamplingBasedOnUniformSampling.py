
"""
    Weighted sampling based on uniform sampling

Implement weighted sampling using uniform sampling:
Given an array of n weights, randomly print 0 to n-1 based on their wieghts using the random.uniform(0,1).
"""

"""
    Idea: This is like projecting from a vertical axis of 0-1 (uniform sampling) to horizontal axis (0-1) of normalized weights.
        We start from zero and draw a step-like graph to accumulate the weights from (0,0) to (1,1).
        If a particular element has a high weight, most of uniformly selected values on the vertical axis will correspond to it.
        Therefore, we create a new array of accumulated weights, and for each uniform sample, we start from zero and dynamically 
            add weights to reach the selected weight. A better is to do a binary search since accumulated array is sorted.
"""


# From stackoverflow:
def weighted_choice(choices):
    total = sum(w for c, w in choices)
    r = random.uniform(0, total)
    upto = 0
    for c, w in choices:
        if upto + w >= r:
            return c
        upto += w
    assert False, "Shouldn't get here"

