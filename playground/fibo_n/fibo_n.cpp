//
// Created by Amin on 7/30/2016.
//

#include "fibo_n.h"

int main () {
    int n = 5;
    long long fibo_n = 1;
    long long fibo_n_1 = 1;
    long long fibo_n_2 = 0;
    for (int i = 0; i <= n; i++) {
        fibo_n = fibo_n_1 + fibo_n_2;
        fibo_n_2 = fibo_n_1;
        fibo_n_1 = fibo_n;
        std::cout << "Fibonachi Sequence Number " << i << " = " << fibo_n << std::endl;
    }
}