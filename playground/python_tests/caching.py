
import time

import numpy as np


from cachetools import cached, TTLCache
cache = TTLCache(maxsize=200, ttl=30)


# @cached(cache)
def my_func(a):

    for i in range(a):
        x = sorted(range(a))

    print(np.sum(x))


if __name__ == "__main__":

    start_time = time.time()
    my_func(10000)
    end_time = time.time()
    print("execution time: {}".format(end_time - start_time))
