import heapq
from collections import OrderedDict

class Knn:
    def __init__(self, nums, classes, k):
        self.nums = nums
        self.classes = classes
        self.k = k

        # self.pairs = heapq.heapify(zip(nums, classes))
        self.pairs = OrderedDict(sorted(zip(nums, classes)))
        print("pairs: {}".format(self.pairs))

    def run(self, data):
        results = [0] * len(data)
        N = len(self.pairs)
        for x in data:

            # idx = binary_search(self.pairs.items(), x)
            found = False
            for i in self.pairs.items():
                if i >= x:
                    found = True
                    idx = i
                    break
            if found == False:
                idx = N - k




        return results


if __name__ == "__main__":
    nums = [10, 2, 3, 4, 5, 6, 7, 8, 9, 1]
    classes = [0, 1, 1, 0, 0, 0, 1, 1, 1, 1]
    k = 3

    data = [3.5, -1, 12]
    output = Knn(nums, classes, k).run(data)
    print("output: {}".format(output))
