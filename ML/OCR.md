
# OCR
https://github.com/open-mmlab/mmocr


# Text Detection
EAST: Efficient & Accurate Scene Text Detector: https://paperswithcode.com/paper/east-an-efficient-and-accurate-scene-text


# Recognition
Attention-OCR
CTC
Recognition -> a language model trained together for correcting words
